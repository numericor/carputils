#!/usr/bin/env python3

r"""

VARP experiment
===============

This experiments implements the Virtual Arrhythmia Risk Prediction experiment
as described by Arevalo et al in .

"""

EXAMPLE_DESCRIPTIVE_NAME = 'Virtual Arrhythmia Risk Prediction'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>'


# --- import packages --------------------------------------------------------
import argparse
import os, sys
from datetime import date
from json import load, dump
from shutil import copy
import numpy as np

from carputils import ep
from carputils.resources import petsc_options
from carputils import testing
from carputils import settings
from carputils import tools
from carputils import model
from carputils import mesh
from carputils.cep4cms import j2carp as j2c


def parser():
    parser = tools.standard_parser()
    exp = parser.add_argument_group('Experiment definition and general options')

    exp.add_argument('--plan', type=str, default=None,
                       help='Describe simulation plan in json format including configuration, experimental settings, ' +
                            'electrodes and protocols. Other settings provided through --config, --experiment, ' +
                            '--electrode* and --protocol* are ignored.' )

    exp.add_argument('--config', type=str, required=True, default='conf.json',
                       help='Describe tissue model in terms of labels and function association.')

    exp.add_argument('--experiment', type=str, required=True, default='exp.json',
                       help='Macro parameter experiment description in .json format.')

    exp.add_argument('--stage',
            default='S1',
            choices=['S1', 'S2', 'MT', 'PR'],
            help='Pick protocol stage, possible choices are '
                +'S1 - pre-pacing with given number and cycle length of S1 stimuli '
                +'S2 - premature stimulus at short cycle length '
                +'MT - test VT induction and, if successfull, maintenance of VT'
                +'PR - proloing MT protocl to test longer term sustainance of VT' )


    exp.add_argument('--cohort', type=str, default=None,
                     help='Directory storing cohort of all cases belonging to a given study')

    exp.add_argument('--caseID', type=str, default=None,
                     help='Pick a specific case from the cohort directory.')

    exp.add_argument('--mesh', type=str, default=None,
                       help='Path and basename of mesh to be prepaced (/path_to_mesh_dir/mesh_base_name).')
    
    exp.add_argument('--sourceModel', default='monodomain',
                     choices=ep.MODEL_TYPES,
                     help='pick electrical model type (default: monodomain)')

    exp.add_argument('--surf2vol',
                     nargs=2,
                     default=[0.14,0.14],
                     help='Pick bidomain surface to volume ratio for normal and border zone tissue default: [ 0.14, 0.14 ]).'
                          +'This scales velocity independently of direction. '
                          +'Allows independent scaling of velocity in normal and border zone tisse. '
                          +'This is approximated as 2/(cell radius) / fi '
                          +'where fi is the intracellular volume fraction.')

    exp.add_argument('--electrodes', type=str,
                       default=None,
                       help='List of electrode descriptions in json format. ' +
                            'This is optional, electrodes may be listed also ' +
                            'in the \"electrodes\" section of the experiment description.')

    exp.add_argument('--electrode', type=str, required=True,
                      help='Pick a specific electrode from list, descriptions must be given ' +
                           'provided through --electrodes, or within an \"electrodes\" section' +
                           'in --experiment.')

    exp.add_argument('--phie',
                     type=bool,
                     default=False,
                     help='Recover extracellular potentials in entire extracellular domain'
                          +'including heart and surrounding media (torso, blood pool, etc)' )

    # induction stage - specific options
    stage_S1  = parser.add_argument_group('Stage S1 options')

    # restart from pre-paced setting

    # define S1 pacing cycle length
    stage_S1.add_argument('--pcl',
            default=600,
            type=float,
            help='Pacing cycle length of S1 stimulus train.'
                +'The default pacing cycle is 600 ms.')

    stage_S1.add_argument('--S1-cycles',
            default=1,
            type=int,
            choices=range(1,10),
            help='Number of S1 pacing stimuli to deliver.')

    stage_S1.add_argument('--CI_min',
            default=280,
            type=float,
            help='Minimal coupling interval for premature stimulus delivery (S2, S3 or S4).'
                +'The default is set to 280 ms.'
                +'Using a pacing cycle length below this requires re-running the S1 stage' ) 

    stage_S1.add_argument('--limit-cycle', 
            type=str, 
            default=None,
            help='Checkpoint file of tissue state after pre-pacing.')

    # define S2 pacing stage
    stage_S234  = parser.add_argument_group('Stage S2 - S4 options')
    stage_S234.add_argument('--CI',
            default=330,
            type=float,
            help='Coupling interval for premature stimulus delivery (S2, S3 or S4).'
                +'The default is set to 330 ms.' )

    stage_S234.add_argument('--S2-cycles',
            default=1,
            type=int,
            help='Number of S2,3,4 pacing stimuli to deliver.')

    # maintenance checking
    stage_MT = parser.add_argument_group('Stage MT and PR options for testing maintenance')

    stage_MT.add_argument('--maintain',
                          default=2000,
                          type=float,
                          help='Time window for maintenance checking.'
                               +'The default window is set to 2000 ms.')

    stage_MT.add_argument('--activity',
                          default=150,
                          type=float,
                          help='Time window for activity checking after which simulation terminates'
                                 +' if not activity is detected. The default is set to 150 ms.' )

    stage_MT.add_argument('--prolong',
            default=1000, type=float,
            help='Prolong maintenance observation period (default: 1000 ms).' )

    # post-processing options
    pproc = parser.add_argument_group('Post-processing and visualization options')
    pproc.add_argument('--visual-mesh',
            default='N',
            choices=['N', 'X', 'XE'],
            help='Extract meshes for visualization, choices are '
                +'N  - do (N)ot extract meshes (default) '
                +'X  - e(X)tract meshes '
                +'XE - e(X)tract mesh and (E)xit, do not run simulation')

    pproc.add_argument('--pngs',
                       choices=['off', 'stage', 'all'],
                       default='off',
                       help='Create png files from data using meshalyzer.' +
                            'Options are: \"off\" do not generate pngs ' +
                             '\"stage\" render pngs only for current stage ' +
                             '\"all\" render pngs for all stages.' )

    pproc.add_argument('--movie', default=argparse.SUPPRESS,
                       action='store_true',
                       help='Flag for turning pngs into mpeg movies.')

    return parser


# create separated intra and extra tag list for physics grid separation
def load_tag_map(tag_dict, extra_labels):

    # collect intra cellular regions
    intra = list()
    extra = list()
    for key in tag_dict:
        # skip bath
        if key in extra_labels:
            domain = extra
        else:
            domain = intra

        # get tags for key
        values = tag_dict.get(key)
        tags   = values.get('tags')
        if isinstance(tags, list):
            domain += tags
        elif isinstance(tags, int):
            domain.append(tags)
        else:
            print('Not supported type "{}" for tag "{}"!'.format(type(tags), key))

    tag_map=dict()
    tag_map['INTRA'] = intra
    tag_map['EXTRA'] = intra + extra

    # return tag_map
    return argparse.Namespace(**tag_map)


def get_electrodes(args):
    # read in all electrodes
    if 'electrodes' in args:
        # read electrode list from separate json description
        elec_file = args.electrodes
    else:
        elec_file = args.experiment

    # get dictionary
    j_dict = j2c.load_json(elec_file)

    # read "electrodes" section from dictionary
    elecs = j_dict.get('electrodes')

    if not elecs:
        print('No electrodes section found in %s'%(elec_file))

    return elecs
    #argparse.Namespace(**elecs)


def stage_IDs(args):

    # predefined stages
    #------------------------
    stages = ['S1', 'S2', 'MT', 'PR']

    # IDs of different stages
    # -----------------------
    S1_ID = simID('S1', args.electrode, args.pcl, args.CI, args.caseID)
    S2_ID = simID('S2', args.electrode, args.pcl, args.CI, args.caseID)
    MT_ID = simID('MT', args.electrode, args.pcl, args.CI, args.caseID)
    PR_ID = simID('PR', args.electrode, args.pcl, args.CI, args.caseID)

    IDs = [S1_ID, S2_ID, MT_ID, PR_ID]

    stageIDs = {}
    for i,sID in enumerate(stages):
        stageIDs[sID] = IDs[i]

    return stages, stageIDs


def setup_stage_protocol(args, stim_map):
    """
    Set up protocol specific stimulus options.

    Args:
        args : command line arguments
               defining the experimental stage

        stim_map : list of available electrodes
"""
    idx = 0

    # check whether stimulus is in list
    if args.electrode not in stim_map:
        print('\n\nElectrode %s not found.\n' % (args.electrode))
        sys.exit()

    cmd = []

    # select electrode
    # ----------------
    e_label = ['-stim[{}].name'.format(idx), args.electrode]

    elec, idx_hd = j2c.elec_sec_to_carp(stim_map, idx, args.electrode)

    if not elec:
        print('No valid electrode definition given for %s' % (args.electrode))
        sys.exit()

    elec = e_label + elec

    # IDs of different stages
    # -----------------------

    stages, stageIDs = stage_IDs(args)

    # Stage 1 - pre-pacing with S1 stimulus at PCL=600 ms
    # ---------------------------------------------------
    s1_start = 0  # start of protocol
    s1_pcl = args.pcl  # pacing cycle length during S1
    s1_num = args.S1_cycles  # number of S1 to deliver (typically 6, but we do less as we have pre-paced initial vectors
    s1_dur = 2  # duration of a single pulse
    s1_strength = 100       # strength of S1 stimulus

    # we stop S1 at the earliest plausible S2 delivery
    # to checkpoint there for restarting without repeating anything
    s1_tend = s1_pcl * (s1_num - 1) + args.CI_min
    s1_stop = s1_pcl * (s1_num - 1)

    # Stage 2 - premature stimulus S2, S3 or S4 at prescribed PCL
    # -----------------------------------------------------------
    s2_start = s1_stop + args.CI  # start of s2 stage
    s2_num = args.S2_cycles  # how many s2 to apply, typically we do 1 only
    s2_tend = s2_start + args.CI * s2_num  # end of s2 stage, observe for another S2 CI
    chk_intv = 10  # checkpoint every 10 ms starting at chk0
    chk0 = s1_tend + chk_intv  # start checkpointing for restarting

    # Stage 3 - test maintenance of VT for at least 4 x PCL of until VT terminates
    # ----------------------------------------------------------------------------
    mt_tend = s2_tend + args.maintain  # maintenance observation period after S2 delivery
    sent = args.activity               # sentinel, check for activity, stop otherwise

    # Stage 4 - test maintenance of VT for prolonged periods or until VT terminates
    # -----------------------------------------------------------------------------
    pr_tend = mt_tend + args.prolong  # maintenance observation period after end of MT

    # number of electrodes used is always 1
    nelecs = ['-num_stim', 1]

    # pulse definition
    pulse = ['-stim[0].pulse.name', "square-monophasic" ]

    if args.stage == 'S1':
        # shorten bcl if we run 1 cycle only
        if s1_num == 1:
            s1_pcl = s1_dur

        s1 = ['-stim[0].ptcl.start', s1_start,
              '-stim[0].ptcl.bcl', s1_pcl,
              '-stim[0].ptcl.npls', s1_num,
              '-stim[0].ptcl.duration', s1_dur]

        s1_chk_pt = ['-num_tsav', 1,
                     '-tsav[0]', s1_tend]

        pulse += ['-stim[0].pulse.strength', s1_strength]
        s1_prtcl = nelecs + elec + s1 + pulse + ['-tend', s1_tend] + s1_chk_pt

        # pre-paced limit cycle provided?
        if args.limit_cycle:
            # ascertain checkpoint exists
            if os.path.exists(args.limit_cycle):
                s1_prtcl += [ '-start_statef', args.limit_cycle ]
            else:
                print('\n\nLimit cycle checkpoint file {} not found.'.format(args.limit_cycle))
                sys.exit()
                print('Starting from default state using {} pacing cycles at S1 stage'.format(args.S1_cycles))

        cmd += s1_prtcl 

    if args.stage == 'S2':
        restart = ['-start_statef', './%s/state.%.1f' % (stageIDs['S1'], s1_tend)]

        s2_chk_pt = ['-chkpt_start', chk0,
                     '-chkpt_intv', chk_intv,
                     '-chkpt_stop', s2_start]

        s2_chk_pt+= ['-num_tsav', 1,
                     '-tsav[0]', s2_tend ]

        s2 = ['-stim[0].ptcl.start', s2_start,
              '-stim[0].ptcl.bcl', args.CI,
              '-stim[0].ptcl.npls', s2_num,
              '-stim[0].ptcl.duration', s1_dur*2]

        pulse += ['-stim[0].pulse.strength', s1_strength*2 ]

        s2_prtcl = nelecs + elec + s2 + pulse + ['-tend', s2_tend] + s2_chk_pt + restart

        cmd += s2_prtcl

    # set up lat detection for sentinel, detect Vm upstrokes
    # by looking for positive slopes crossing the threshold
    # could use j2c.ek_lat_detect(0,-20)
    lats = ['-num_LATs', 1,
            '-lats[0].ID', 'Sentinel',
            '-lats[0].all', 1,
            '-lats[0].measurand', 0,
            '-lats[0].threshold', -20,
            '-lats[0].mode', 0]

    if args.stage == 'MT':
        # we don't stimulate here anymore
        nelecs = ['-num_stim', 0]

        # restart from end of S2 stage
        restart = ['-start_statef', './%s/state.%.1f' % (stageIDs['S2'], s2_tend)]

        # checkpoint for restarting prolongation period
        mt_chk_pt = ['-num_tsav', 1,
                     '-tsav[0]', mt_tend]

        mt_sentinel = ['-sentinel_ID', 0,
                       '-t_sentinel_start', s2_tend,
                       '-t_sentinel', sent]

        mt_prtcl = nelecs + restart + mt_chk_pt + ['-tend', mt_tend]  + lats + mt_sentinel

        cmd += mt_prtcl

    if args.stage == 'PR':
        # we don't stimulate here anymore
        nelecs = ['-num_stim', 0]

        # restart from end of S2 stage
        restart = ['-start_statef', './%s/state.%.1f' % (stageIDs['MT'], mt_tend)]

        # checkpoint for restarting prolongation period
        pr_chk_pt = ['-num_tsav', 1,
                     '-tsav[0]', pr_tend]

        pr_sentinel = ['-sentinel_ID', 0,
                       '-t_sentinel_start', mt_tend,
                       '-t_sentinel', sent]

        pr_prtcl = nelecs + restart + pr_chk_pt + ['-tend', pr_tend] + lats + pr_sentinel

        cmd += pr_prtcl

    return cmd

def jobID(args):
    """
    Generate name of top level output directory.
    """
    if args.stage == 'S1':
        CI = args.pcl
    else:
        CI = args.CI

    if args.pngs != 'off' or 'movie' in args:
        # png rendering only, create dummy simID to avoid overwriting
        # of previously created outputs
        sID = 'make_pngs'
    else:
        if args.caseID:
            case_ID = args.caseID
        else:
            case_ID = os.path.basename(args.mesh)

        sID = simID(args.stage, args.electrode, args.pcl, args.CI, case_ID )

    return sID

def simID(stage, electrode, PCL, CI, caseID):

    PCLdig = '%.1f'%(PCL)
    CIdig  = '%.1f'%(CI)   
    
    if stage == 'S1':
        ID = '{}_{}_PCL_{}_ms_{}'.format(stage, electrode, PCLdig, caseID )
    else:
        ID = '{}_{}_PCL_{}_ms_CI_{}_ms_{}'.format(stage, electrode, PCLdig, CIdig, caseID )

    return ID 

def visual_mesh(job, args, tag_map):

    # assemble carp command line for mesh extraction
    cmd = tools.carp_cmd()
    
    # extract meshes if requested
    cmd += tools.gen_physics_opts(tag_map.EXTRA, tag_map.INTRA)
    cmd += ['-gridout_i', 3,
            '-gridout_e', 3,
            '-experiment', 3 ]

    # prepare output of visualization meshes
    meshname, args.cohort, args.caseID = get_mesh(args.mesh, args.cohort, args.caseID)
    simID = get_visual_mesh(args.mesh, args.cohort, args.caseID)

    # check existence of directory
    if not os.path.isdir(simID):
        os.mkdir(simID)
    
    cmd  += [ '-simID', simID,
              '-meshname', meshname ]
    
    # Run mesh extraction command
    # change jobID
    job.carp(cmd)

    if args.visual_mesh == 'XE':
        sys.exit()


def make_pngs(job, args, meshname):

    if args.pngs == 'off':
        return

    # check if visualization meshes exist in case directory
    visual_mesh_dir = get_visual_mesh(args.mesh, args.cohort, args.caseID)

    # Prepare file paths for transmembrane voltage
    print('meshname={}'.format(meshname))
    print('visual_mesh={}'.format(visual_mesh_dir))
    geom = os.path.join(visual_mesh_dir, os.path.basename(meshname)+'_i')
    view = os.path.join(args.cohort, 'vm.mshz')

    # create IDs for all VARP stages
    stages, stageIDs = stage_IDs(args)
    if args.pngs == 'stage':
        IDs = [ stageIDs[args.stage] ]
    elif args.pngs == 'all':
        IDs = []
        for stage in stages:
            IDs += [ stageIDs[stage] ]
    else: # off
        IDs = []

    val_mov_IDs = []
    for ID in IDs:

        # check on files
        data = os.path.join(ID, 'vm.igb')
        if not os.path.exists(data):
            print('\n\nData file %s not found, skipping png generation.\n'%(data))
            continue

        pngID = '{}.pngs'.format(ID)

        job.meshalyzer(geom, data, view, PNGfile = pngID, numframe = 10000, frame = 0)

    # if we don't want to render a movie we exit here
    if not 'movie' in args:
        sys.exit()


def png2mov( mv_enc, mvID, selector, fps):

    if mv_enc == 'mencoder':
        vcmd = png_mencoder(mvID, selector, fps)

    elif mv_enc == 'ffmpeg':
        vcmd = png_ffmpeg(mvID, selector, fps)
    
    else:
        vcmd = None

    return vcmd

def png_mencoder(mvID, selector, fps):

    vcmd = ['mencoder', 'mf://{}'.format(selector), '-ovc', 'x264', '-fps', fps, '-o', mvID ]

    return vcmd


def png_ffmpeg(mvID, selector, fps):

    vcmd = ['ffmpeg', '-pattern_type', 'glob', '-i', '\'{}\''.format(selector), '-c:v', 'libx264', '-framerate', fps, '-pix_fmt', 'yuv420p', mvID ]

    return vcmd


def make_movies(job, args):

    # ffmpeg encoding not working yet for combining stage movies
    mv_encs = [ 'mencoder', 'ffmpeg']
    mv_enc  = mv_encs[0] 
    print('\n')

    if 'movie' not in args:
        return

    # to be turned into parameters
    keep_pngs = True

    # create IDs for all VARP stages
    stages, stageIDs = stage_IDs(args)
    IDs = []
    for stage in stages:
        IDs += [ stageIDs[stage]]

    valid_stage = []
    val_mov_IDs = []
    for ID in IDs:

        pngID = '{}.pngs'.format(ID)
        mvID = '{}.mpg'.format(ID)

        if os.path.exists('{}/frame00000.png'.format(pngID)):
            valid_stage += [True]
            val_mov_IDs += [mvID]

            fps = 100
            vcmd = png2mov(mv_enc, mvID, '{}/frame*.png'.format(pngID), fps)
            if vcmd:
                job.bash(vcmd)

            if not keep_pngs:
                job.bash(['rm', '-rf', '{}'.format(pngID)])

    # generate combined movie
    vcmd = ['mencoder' ]
    vcmd += val_mov_IDs

    comb_movie_ID = ''
    for i,val in enumerate(valid_stage):
        if val:
            comb_movie_ID += '{}_'.format(stages[i])

    # use S2 ID to create global label
    glID = stageIDs['S2']
    glID = glID.replace('S2_',comb_movie_ID)

    vcmd += [ '-ovc', 'copy', '-o', '{}.mpg'.format(glID) ]
    job.bash(vcmd)

    show_video = True
    if show_video:
        vshowcmd = [ 'mplayer', '{}.mpg'.format(glID)]
        job.bash(vshowcmd)

    sys.exit()


# get mesh
def get_mesh(mesh, cohort, caseID):

    meshname = None

    if cohort and caseID:    
        meshname = '{}/{}/{}'.format(cohort, caseID, caseID)
   
    elif mesh:

        # mesh given, defines meshname 
        meshname = mesh
        # here we define cohort and caseID based on meshname
        cohort = os.path.dirname(mesh)
        caseID = os.path.basename(mesh)
  
    else:
        print('\n\nNo mesh specified, use either mesh or cohort and caseID arguments.\n')
        sys.exit()

    return meshname, cohort, caseID    

# get visualization mesh 
def get_visual_mesh(mesh, cohort, caseID):

    visual_mesh_dir = None

    # check if visualization meshes exist in case directory
    if mesh:
        dir_path = os.path.dirname(mesh)
        visual_mesh_dir = os.path.join(dir_path, 'visual_mesh') 
    elif cohort and caseID:
        visual_mesh_dir = os.path.join(cohort, caseID, 'visual_mesh')
    else:
        print('\n\nNo mesh specified, use either mesh or cohort+case arguments.\n')
        sys.exit()

    return visual_mesh_dir    


# visualize simulation results
def visualize(job, args, meshname):

    visual_mesh_dir = get_visual_mesh(args.mesh, args.cohort, args.caseID)

    # Prepare file paths for transmembrane voltage
    geom = os.path.join(visual_mesh_dir, os.path.basename(meshname)+'_i')
    data = os.path.join(job.ID, 'vm.igb')

    if args.cohort:
        view = os.path.join(args.cohort, 'vm.mshz')
    elif args.mesh:
        view = os.path.join(os.path.dirname(meshname), 'vm.mshz')

    # Call meshalyzer Vm
    job.meshalyzer(geom, data, view)


def recover_phie(args):

    # recover extracellular potential field in a post-processing step
    ppID  = 'pseudo_bidm'
    r_cmd = [ '-experiment', 4,
              '-post_processing_opts', 128,
              '-ppID', ppID,
              '-bidomain', 2,
              '-ellip_use_pt', 1 ]

    args.overwrite_behaviour = 'append'
    return r_cmd, args


@tools.carpexample(parser, jobID, clean_pattern='^(\d{4}-)|^(S1_)|^(S2_)|^(MT_)|^(PR_)')
def varp(args, job):
    #cnf_, exps_, fncs_, elecs_, prtcls_, slv_, plan_ = j2c.load_experiment(args.config, args.experiment, args.plan, args.electrodes, args. protocols)

    cnf, exps, fncs, elecs_, prtcls_, slv_, plan_ = j2c.load_experiment(args.config, args.experiment, args.plan, args.electrodes, None)
    # read tissue/organ configuration
    #cnf = j2c.load_json(args.config)

    # load tag map
    tag_map = load_tag_map(cnf, ['scar'])

    # read experiment description
    #exps = j2c.load_json(args.experiment)

    # retrieve definition of all functional domains
    #fncs = exps.get('functions')

    # check configuration
    if not j2c.check_config(cnf, fncs):
        sys.exit()

    # extract meshes for visualization if requested
    if not args.visual_mesh == 'N':
        visual_mesh(job, args, tag_map)

    # if a mesh-name is provided via CLI, overwrite or
    # insert the mesh in the experiments dictionary
    meshname, args.cohort, args.caseID = get_mesh(args.mesh, args.cohort, args.caseID)
    if meshname is not None:
        exps['mesh'] = meshname

    # png & movie generation
    make_pngs(job, args, meshname )
    make_movies(job, args)

    # set up simulation command
    cmd  = tools.carp_cmd()
    cmd += ['-simID', job.ID ]
    cmd += ['-meshname', meshname]
    cmd += ['-spacedt', 1.]
    cmd += ['-tstart', -1 ]

    # use consistent solver settings
    slv = exps.get('solver')

    # read in all available electrodes
    elecs = j2c.get_dict_sec('electrodes', args.experiment, args.electrodes)
    if not elecs:
        sys.exit()

    # set up experimental protocol for current stage
    prtcls = exps.get('protocols')
    #pS2 = prtcls.get('S2')
    #pS2_cycs = pS2.get('cycles')
    prtcl = setup_stage_protocol(args, elecs)

    # set up ionic and conductive tissue properties
    #propagation = pS2.get('propagation')
    propagation = 'rd'
    cmd += j2c.setup_tissue(cnf, fncs, propagation)
    if not j2c.check_initial_state_vectors(fncs, True):
        # some initial state vectors are missing
        print('\nMissing initial state vectors, exiting!\n\n')
        sys.exit()

    cmd += prtcl + j2c.add_slv_settings(slv)

    # recover phie in a post-processing step?
    if args.phie:
        r_cmd, args = recover_phie(args)
        cmd += r_cmd

    # run
    job.carp(cmd)

    # Do visualization
    if args.visualize and not settings.platform.BATCH:
        visualize(job, args, meshname)

if __name__ == '__main__':
    varp()
