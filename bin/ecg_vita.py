#!/usr/bin/env python3

r"""

VITA experiment
===============

This experiments implements the Virtual Induction and Treatment of Arrhythmias protocol
as described by Campos et al in Med Imag Anal 2021.

"""

EXAMPLE_DESCRIPTIVE_NAME = 'Virtual Induction and Treatment of Arrhyhtmias'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>'


# --- import packages --------------------------------------------------------
import argparse
import subprocess
import os, sys
from datetime import date
from json import load, dump
from shutil import copy
import numpy as np

from carputils import tools
from carputils import model
from carputils import settings
from carputils.cep4cms import j2carp as j2c
from carputils.vita.trfm import read_trfm, apply_trfm
from carputils.vita import genmesh2 as gm
from carputils.vita import map_lf as mlf
from carputils.vita import sr_vita
from carputils.vita.vita2studio import vita2studio as v2s

import carputils.cext.mshread as mshread
import carputils.cext.mshwrite as mshwrite


def parser():
    parser = tools.standard_parser()
    exp  = parser.add_argument_group('ECG-VITA xperiment definition and general options')

    exp.add_argument('--trfm',
                          default=None,
                          type=str,
                          help='Provide affine transformation file to transform torso. '
                              +'By default, transform is located under $caseID/$torso.trfm.')

    exp.add_argument('--stage',
                          default='TORSO',
                          choices=['TORSO', 'LEADFIELD', 'MESH', 'VITA', 'STUDIO', 'ECG'],
                          help='Pick protocol stage, possible choices are '
                              +'TORSO - add a torso from a torso library '
                              +'LEADFIELD - transform leadfield description and compute leadfield vectors '
                              +'MESH - generate mesh combining VITA heart and transformed torso geometry '
                              +'VITA - run VITA '
                              +'STUDIO - test VT induction and, if successfull, maintenance of VT '
                              +'ECG - compute ECGs associated with all detected VT initiation sites.')


    exp.add_argument('--caseID', type=str, default=None,
                          help='Directory of VITA case to be used.')

    exp.add_argument('--vita-heart-mesh', type=str, default=None,
                          help='Mesh name of heart used for VITA, must be stored in caseID directory.')

    # induction stage - specific options
    stage_TORSO  = parser.add_argument_group('Add torso from library options')

    # select torso dir and file
    stage_TORSO.add_argument('--torso-dir',
                          default='torsos',
                          type=str,
                          help='Select directory storing torso library.')

    stage_TORSO.add_argument('--torso',
                          default=None,
                          type=str,
                          help='Select torso surface mesh from library.')

    # transform the leadfield
    stage_LF  = parser.add_argument_group('Stage leadfield transformation')
    stage_LF.add_argument('--leadfield',
                          default=None,
                          type=str,
                          help='Provide leadfield file to be transformed. Must be located under $torso_dir/$torso/. '
                              +'By default, this is assembled as $torso_dir/$torso/$torso.torso.lf.json.')

    stage_LF.add_argument('--lf-compute',
                          action='store_true',
                          help='Compute leadfield vectors.')

    # run VITA to find VT circuits
    stage_VITA = parser.add_argument_group('Run VITA on myocardium')
    stage_VITA.add_argument('--ek-params',
                          default='ek_params.init',
                          type=str,
                          help='Provide eikonal parameters for VITA run.')

    stage_VITA.add_argument('--num-stim',
                          default=1,
                          type=int,
                          help='Number of stimuli used in VITA.')

    stage_VITA.add_argument('--step-size',
                          default=1000, type=float,
                          help='Step size used in VITA to determine isosurfaces.')

    stage_VITA.add_argument('--rtt',
                          default=50, type=float,
                          help='Minimum round trip time in ms for detecting VITA circuits.')

    stage_VITA.add_argument('--no-ecg',
                          action='store_true',
                          help='Use provided heart mesh for VITA, not the torso-derived mesh for computing ECGs.')

    # convert VITA output to studio format for interactive analyses
    stage_STUDIO = parser.add_argument_group('Convert VITA output to studio format')
    stage_STUDIO.add_argument('--plan',
                          type=str,
                          default='plan.vita.tmpl.json',
                          help='studio simulation plan template for VITA.')

    stage_STUDIO.add_argument('--launch',
                          action='store_true',
                          help='Launch studio with VITA model after conversion.')

    return parser


def trfm_torso(torso_dir, torso_ID, torso_cnf, trfmf, caseID):

    print('\n\nTransforming torso model {} using {} to fit with VITA heart.'.format(torso_ID, trfmf))
    print('-'*80)

    # check files first
    mshext  = { 'pts': '.bpts', 'elem':'.belem', 'lon':'.blon' }
    trfm_srfs = list(torso_cnf.keys())

    print('\nChecking input surfaces and transform files:')
    srf_IDs = []
    srffs = []
    for i,srf in enumerate(trfm_srfs):

        srf_ID = '{}.{}'.format(torso_ID, srf)
        srff   = os.path.join(torso_dir, torso_ID, '{}'.format(srf_ID))
        if os.path.isfile(srff+mshext.get('pts')):
            print('Using {} surface mesh {}{} from library {}.'.format(srf, srff, mshext.get('pts'), torso_dir))
            srf_IDs.append(srf_ID)
            srffs.append(srff)
        else:
            print('Surface mesh {} not found in library {}.'.format(srff, torso_dir))
            trfm_srfs.remove(srf)

    if not len(srf_IDs):
        print('No valid torso surface mesh supplied, proceeding without torso.')
        return

    # we have at least a valid torso surface
    trfm = None
    if os.path.isfile(trfmf):
        print('Using affine transformation from {}.'.format(trfmf))
        # read transform
        trfm = read_trfm(trfmf)
    else:
        print('Affine transformation file {} not found.'.format(trfmf))
        sys.exit()

    # transform all valid meshes
    print('\n\nApply affine transform to torso {}'.format(torso_ID))
    print('-'*80)
    for i, srff in enumerate(srffs):
        print('{}:'.format(srff))
        print('Read source points file {}{}.'.format(srff, mshext.get('pts')))
        src_pts = mshread.read_points_bin(srff+mshext.get('pts'))

        # apply transform
        print('Apply transform {} to {}.'.format(trfmf, srff))
        src_pts_trfm = apply_trfm(src_pts, trfm)
    
        # write back transformed pts file
        trfm_ptsf = os.path.join(caseID, '{}.{}.trfm{}'.format(torso_ID, trfm_srfs[i], mshext.get('pts')))
        print('Write transformed points to {}.'.format(trfm_ptsf))
        mshwrite.write_points_bin(trfm_ptsf, src_pts_trfm)
    
        # copy over elem and lon files, no transform needed
        trfm_elemf = os.path.join(caseID, '{}.{}.trfm{}'.format(torso_ID, trfm_srfs[i], mshext.get('elem')))
        trfm_lonf  = os.path.join(caseID, '{}.{}.trfm{}'.format(torso_ID, trfm_srfs[i], mshext.get('lon')))
        print('Copying {} surface element file to {}.'.format(trfm_srfs[i], trfm_elemf))
        copy(srff+mshext.get('elem'), trfm_elemf)
        print('Copying {} surface fiber file to {}.'.format(trfm_srfs[i], trfm_elemf))
        copy(srff+mshext.get('lon'),  trfm_lonf)
        print()

    print('\nSupplied torso transformed, ready for remeshing.')
    print('-'*80)
    print('\n')

    return trfm

def read_torso_cnf(torso_cnf_file):

    # read torso configuration
    #t_cfgf = os.path.join(torso_dir, torso_ID, torso_ID + '.torso' + '.cfg' + '.json')
    t_cfg = j2c.load_json(torso_cnf_file)
    # go over surfaces to create configuration
    torso_cnf = {}
    if t_cfg:
        surfs = t_cfg.get('surfs')
        for srf, tag in surfs.items():
            if not isinstance(tag, list):
                tag = [tag]
            torso_cnf[srf] = {'mesh': '', 'tag': tag}
    else:
        # use default configuration
        torso_cnf = {'heart': {'mesh': '', 'tag': [10]},
                     'lungs': {'mesh': '', 'tag': [200]},
                     'torso': {'mesh': '', 'tag': [0]}}

    return torso_cnf


def add_torso(torso_ID, vita_heart_mesh_file, vita_ecg_torso_mesh_file, cnf):

    caseID = os.path.dirname(vita_heart_mesh_file)
    torso_base = os.path.join(caseID, torso_ID)
    #heart_base = os.path.join(caseID, vita_heart_mesh)

    for key, val in cnf.items():
        val['mesh'] = torso_base +'.' + key + '.trfm'

    keep = False
    lungs = cnf.get('lungs')
    torso = cnf.get('torso')
    gm.mg_torso(torso, lungs, vita_heart_mesh_file, vita_ecg_torso_mesh_file, keep)


def extract_vita_mesh(torso_mesh, x_vita_mesh):

    # extract intracellular grid to run vita on
    cmd = ['meshtool', 'extract', 'myocard',
           '-msh={}'.format(torso_mesh),
           '-ifmt=carp_bin',
           '-ofmt=carp_bin',
           '-submsh={}'.format(x_vita_mesh)]

    HDR = '=' * 80
    print(HDR + '\nCALLING: ' + ' '.join(cmd) + '\n' + HDR)
    #heart_surf = '{}.surfmesh.vtk'.format(heart_base)
    subprocess.check_call(cmd)
    print('\n' * 2)


def extract_intra_mesh_with_tags(torso_mesh, vita_heart_cnf, x_vita_mesh):
    
    intra_tags = []
    for key, tags in vita_heart_cnf.items():
        if key != 'scar':
            intra_tags += tags
    
    intra_tags_str = ','.join(str(tag) for tag in intra_tags)
    
    # extract intracellular grid to run vita on
    cmd = ['meshtool', 'extract', 'mesh',
           '-msh={}'.format(torso_mesh),
           '-tags',    intra_tags_str,
           '-ifmt=carp_bin',
           '-ofmt=carp_bin',
           '-submsh={}'.format(x_vita_mesh)]

    HDR = '=' * 80
    print(HDR + '\nCALLING: ' + ' '.join(cmd) + '\n' + HDR)
    #heart_surf = '{}.surfmesh.vtk'.format(heart_base)
    subprocess.check_call(cmd)
    print('\n' * 2)

    # extract largest connected mesh
    cmd = ['meshtool', 'extract', 'unreachable',
           '-msh={}'.format(x_vita_mesh),
           '-mode',    '1',
           '-ifmt=carp_bin',
           '-ofmt=carp_bin',
           '-submsh={}'.format(x_vita_mesh)]
    
    print(HDR + '\nCALLING: ' + ' '.join(cmd) + '\n' + HDR)
    subprocess.check_call(cmd)
    print('\n' * 2)

    # restore mapping
    cmd = ['meshtool', 'restore', 'mapping',
           '-msh={}'.format(torso_mesh),
           '-ifmt=carp_bin',
           '-submsh={}'.format(x_vita_mesh)]
    
    print(HDR + '\nCALLING: ' + ' '.join(cmd) + '\n' + HDR)
    subprocess.check_call(cmd)
    print('\n' * 2)


def extract_vita_intra_mesh(job, plan_file, out_dir, torso_mesh):

    # extract intracellular grid to run vita on
    cmd = tools.carp_cmd()
    cmd += j2c.plan2carp(job, plan_file, torso_mesh, 'R-D')
    cmd += ['-simID',      out_dir,
            '-bidomain',   0,
            '-gridout_i',  2,
            '-experiment', 3]
    # Start simulation only to output intracellular grid
    job.carp(cmd)


def add_leadfield_params(lf_vtcs, lf_src_vtxf, lf_dir, lf_egmf):

    ref_idx = lf_vtcs['REF']
    carp_lf_cmd = ['-lead_field_egm_file', lf_egmf,
                   '-lead_field_dir', lf_dir,
                   '-lead_field_mode', 0,
                   '-lead_field_meth', 1,
                   '-lead_field_ref', ref_idx,
                   '-lead_field_sources', lf_src_vtxf,
                   '-dump_lead_fields', 1
                   ]

    return carp_lf_cmd

def add_ucb_trigger(stimuli, blk, trg, idx):

    stim_cmd = []
    idx0 = idx
    # activate block and vt trigger
    blk_stim = stimuli.get(blk)
    blk_stim['active'] = 1
    stim_cmd += stimulus_j2c(blk_stim, idx, blk)
    idx += 1

    vt_trigger_stim = stimuli.get(trg)
    vt_trigger_stim['active'] = 1
    stim_cmd += stimulus_j2c(vt_trigger_stim, idx, trg)
    idx += 1

    stims_added = idx - idx0

    return stim_cmd, stims_added


# create now combined block/VT stimulus combinations
def stimuli_to_blk_trg_list(stimuli, blk_tag, vt_tag):

    # get all block sites and their numerical ID
    #blk = 'BLK_'
    blk_lst = []
    for key in stimuli.keys():
        if blk_tag in key:
            blk_lst.append(key.replace(blk_tag, ''))

    # create now combined block/VT stimulus combinations
    #vt = 'VT_'
    vt_init = {}
    for ID in blk_lst:
        vt_cnt = 0
        vt_ID = vt_tag + ID
        vt_init[ID] = {'blk': blk_tag + ID}
        for key in stimuli.keys():
            if vt_ID in key:
                vt_init[ID]['vt_{}'.format(vt_cnt)] = key
                vt_cnt += 1

    for key, val in vt_init.items():
        vt_site = 'VT initiation site {}'.format(key)
        print(vt_site)
        print('-' * len(vt_site))
        for bk, bv in val.items():
            print('%6s: %s' % (bk, bv))
        print('\n')

    return blk_lst, vt_init


def ecg_gen(vita_dir, vita_torso_grid, vita_grid, job):

    # vita runs on intra grid only for now, studio may run on combined grid
    sep = '-' * 80
    print('\n\nGenerate VT ECG signatures from VITA run.')
    print('%s\n' % (sep))
    print('vita directory        - %s' % (vita_dir))
    print('mesh (vita)           - %s' % (vita_grid))
    if vita_torso_grid:
        print('mesh with torso (ECG) - %s'%(vita_torso_grid))

    # find simulation plan and leadfields
    sim_plan = os.path.join(vita_dir, 'plan.vita.' + vita_torso_grid + '.json')
    print('simulation plan       - %s' % (sim_plan))
    lf_dir = os.path.join(vita_dir, 'leadfield')
    lf_setup = os.path.join(lf_dir, 'lf.' + vita_torso_grid + '.json')
    print('leadfield setup       - %s' % (lf_setup))
    lf_src_vtxf = os.path.join(lf_dir, 'lf.' + vita_torso_grid + '.vtx')
    print('leadfield vertex file - %s' % (lf_src_vtxf))
    ecg_dir = os.path.join(vita_dir, 'ecgs')
    print('ECG/EGM output dir - %s' % (ecg_dir))
    print('\n')

    # read VT initiation data from simulation plan
    sp = j2c.load_json(sim_plan)

    sim = sp.get('simulation')
    stimuli = sim.get('stimuli')

    # get block/trigger combinations to induce VT
    blk_lst, vt_init = stimuli_to_blk_trg_list(stimuli, 'BLK_', 'VT_')

    # read in leadfield configuration
    lf_conf, lf_vtcs = mlf.get_leadfield_vtcs(lf_setup)

    # run carp now for each initiation site, for all vt triggers per block site
    # build baseline carp cmdline first
    carp_base_cmd = tools.carp_cmd()
    rmode = 're'
    carp_base_cmd += j2c.plan2carp(job, sim_plan, os.path.join(vita_dir, vita_torso_grid), rmode)
    max_vtcl = 400
    carp_base_cmd += ['-tend', max_vtcl] #sim.get('timesteps') * sim.get('deltatime')]
    # no solver settings, for now we set a coarse time step
    carp_base_cmd += ['-dt', 250]

    # define time step for ecg output
    ecg_dt = 1.
    carp_base_cmd += ['-spacedt', ecg_dt]

    lf_egmf = 'lfEGMs'
    carp_lf_cmd = add_leadfield_params(lf_vtcs, lf_src_vtxf, lf_dir, lf_egmf)
    # store to temporary simulation data
    tmp_sim_dir = os.path.join(vita_dir, 'ecg_compute')
    if not os.path.exists(tmp_sim_dir):
        print('Creating temporary simulation directory {}.'.format(tmp_sim_dir))
        os.makedirs(tmp_sim_dir)

    if not os.path.exists(ecg_dir):
        print('Creating ECG directory {}.'.format(ecg_dir))
        os.makedirs(ecg_dir)

    # iterate over VT initiation sites and start VT for a given chirality
    VT_IDs = []
    for key, val in vt_init.items():

        # separate block and trigger sites
        vt_blk = val['blk']
        vt_trg = {}
        for k in val.keys():
            if 'vt_' in k:
                vt_trg[k] = val[k]

        # iterate over triggers to induce VT
        for k, trg in vt_trg.items():

            VT_IDs.append(trg)
            vt_run = 'Initiating VT using block {} and trigger {}'.format(vt_blk, trg)
            print(vt_run)
            print('-'*len(vt_run))

            simID = os.path.join(tmp_sim_dir, trg)
            carp_vt_cmd = ['-simID', simID]

            # define block/trigger stimulus protocol
            # set up unidirectional block for VT initiation
            idx = 0
            stim_cmd, stims_added = add_ucb_trigger(stimuli, vt_blk, trg, idx)

            carp_stim_cmd = []
            if rmode == 'rd':
                carp_stim_cmd += ['-num_stim', stims_added] + stim_cmd
                carp_vt_cmd += ['-diffusionOn', 1]
            else:
                carp_stim_cmd += ['-num_stim', stims_added + 1] + stim_cmd + ['-stim[2].crct.type', 8]
                carp_vt_cmd += ['-diffusionOn', 0]

            job.carp(carp_base_cmd + carp_lf_cmd + carp_vt_cmd + carp_stim_cmd)

            # copy leadfield EGM results and create ECG signals
            src_lfEGMf = os.path.join(simID, lf_egmf + '.dat')
            trg_lfEGMf = os.path.join(ecg_dir, trg + '_raw_' + lf_egmf + '.dat')
            copy(src_lfEGMf, trg_lfEGMf)

            #print('-'*80)
            #print('\nOnly simulating single VT circuit, exiting ...')
            #sys.exit()

    #sys.exit()
    print('Storing EGM/ECG traces ...')
    print('-'*80)
    for ID in VT_IDs:
        vt_out_dir = os.path.join(tmp_sim_dir, ID)
        raw_egmf = os.path.join(ecg_dir, ID + '_raw_' + lf_egmf + '.dat')
        egms, ecgs = mlf.lf_egms_to_ecg(raw_egmf, ecg_dt, lf_vtcs, lf_src_vtxf)

        # save data to .json
        trg_egmf = os.path.join(ecg_dir, ID + '_' + 'EGMs.json')
        print('Storing {} EGMs to {}'.format(ID, trg_egmf))
        with open(trg_egmf, 'w') as fp:
            dump(egms, fp, indent=4)

        trg_ecgf = os.path.join(ecg_dir, ID + '_' + 'ECG.json')
        print('Storing {} ECGs to {}'.format(ID, trg_ecgf))
        with open(trg_ecgf, 'w') as fp:
            dump(ecgs, fp, indent=4)

    return VT_IDs

def plot_VT_ID_ecgs(VT_IDs, ecg_dir):

    for ID in VT_IDs:

        # load directly from json
        egmf = os.path.join(ecg_dir, ID + '_' + 'EGMs.json')
        ecgf = os.path.join(ecg_dir, ID + '_' + 'ECG.json')
        egms = j2c.load_json(egmf)
        ecgs = j2c.load_json(ecgf)

        mlf.lf_ecg_plot(egms, ecgs, ID)


def stimulus_j2c(stim, idx, stim_name):

    if not stim.get('active'):
        return None

    stim_cmd = []

    elec = stim.get('electrode')
    stim_cmd += j2c.build_stim_geom(elec, idx, stim_name)

    prtcl = stim.get('protocol')
    for key,val in prtcl.items():
        stim_cmd += ['-stim[{}].ptcl.'.format(idx) + key, val]

    strength = stim.get('strength')
    stim_cmd += ['-stim[{}].pulse.strength'.format(idx), strength]
    type = stim.get('type')
    stim_type = None
    if type == 'Transmembrane Voltage clamp':
        stim_type = 9
    elif type == 'Transmembrane Current':
        stim_type = 0

    if stim_type is not None:
        stim_cmd += ['-stim[{}].crct.type'.format(idx), stim_type]
    else:
        print('Unsupported stimulus type {}, ignoring stimulus {}'.format(type, stim_name))
        stim_cmd = []

    return stim_cmd


def update_vita_sim_plan(sim_plan, vita_heart_cnf, torso_cnf):

    # overwrite non-cardiac tags in simulation plan
    sim_cnf = sim_plan.get('config')
    for key,dat in torso_cnf.items():
        domain = sim_cnf.get(key)
        if domain:
            domain['tags'] = dat.get('tag')

    # overwrite cardiac tags
    for key, tags in vita_heart_cnf.items():
        domain = sim_cnf.get(key)
        if domain:
            domain['tags'] = tags

    return sim_plan


def jobID(args):
    """
    Generate a job ID
    """
    sID = '{}_{}'.format(args.caseID,args.vita_heart_mesh)

    return sID

@tools.carpexample(parser, jobID, clean_pattern='^(\d{4}-)|^(S1_)|^(S2_)|^(MT_)|^(PR_)')
def ecg_vita(args, job):

    print('\n\nRunning VITA experiment ...')
    print('-'*80)

    # select torso geometry and transformation
    torso_dir = args.torso_dir
    torso_ID = args.torso

    if args.no_ecg:
        torso_cnf = {}
    else:
        # select affine transform
        if args.trfm is None:
            trfmf = os.path.join(args.caseID, '{}.trfm'.format(args.torso))
        else:
            trfmf = os.path.join(args.caseID, args.trfm)

        # read in affine transformation matrix
        trfm = None
        if os.path.isfile(trfmf):
            print('Using affine transformation from {}.'.format(trfmf))
            # read transform
            trfm = read_trfm(trfmf)
        else:
            print('Affine transformation file {} not found.'.format(trfmf))
            sys.exit()

        # read torso configuration
        t_cfgf = os.path.join(torso_dir, torso_ID, torso_ID + '.torso' + '.cfg' + '.json')
        print('Reading torso configuration from {}'.format(t_cfgf))
        torso_cnf = read_torso_cnf(t_cfgf)

    # read target heart configuration for running VITA
    vita_heart_mesh = args.vita_heart_mesh
    vita_heart_mesh_file = os.path.join(args.caseID, vita_heart_mesh)
    vita_heart_cfgf = os.path.join(args.caseID, vita_heart_mesh + '.cfg' + '.json')
    print('Reading vita heart configuration from {}'.format(vita_heart_cfgf))
    vita_heart_cnf = j2c.load_json(vita_heart_cfgf)

    # combined heart and torso mesh for VITA, available only after stage --MESH
    if args.no_ecg:
        vita_ecg_torso_mesh = vita_heart_mesh
    else:
        vita_ecg_torso_mesh = vita_heart_mesh + '.torso' + '.{}'.format(torso_ID)
    # heart extracted from combined grid for VITA
    intra_dir = os.path.join(args.caseID, 'intra')
    if not os.path.exists(intra_dir):
        job.mkdir(intra_dir, parents=True)
    vita_ecg_heart_mesh = os.path.join('intra', os.path.basename(vita_ecg_torso_mesh) + '_i')
    vita_ecg_torso_mesh_file = os.path.join(args.caseID, vita_ecg_torso_mesh)
    vita_ecg_heart_mesh_file = os.path.join(args.caseID, vita_ecg_heart_mesh)

    # read template simulation plan and update for current settings
    vita_plan_tmpl = j2c.load_json('plan.vita.tmpl.json')
    vita_plan = update_vita_sim_plan(vita_plan_tmpl, vita_heart_cnf, torso_cnf)
    vita_planf = os.path.join(args.caseID, 'plan.vita.' + vita_ecg_torso_mesh + '.json')
    if not os.path.exists(vita_planf):
        j2c.dump_json(vita_planf, vita_plan)


    # transform torso into VITA heart space
    # -------------------------------------
    if args.stage in 'TORSO':
        trfm = trfm_torso(torso_dir, torso_ID, torso_cnf, trfmf, args.caseID)


    # combine VITA heart and surrounding torso in a combined mesh
    # -----------------------------------------------------------
    if args.stage in 'MESH':
        
        if args.no_ecg:
            # extract intra vita mesh from heart mesh
            extract_intra_mesh_with_tags(vita_heart_mesh_file, vita_heart_cnf, vita_ecg_heart_mesh_file)
        else:
            # mesh torso and combine with vita mesh
            add_torso(torso_ID, vita_heart_mesh_file, vita_ecg_torso_mesh_file, torso_cnf)

            # extract new intra vita mesh from combined torso-heart mesh

            # extract_vita_mesh(vita_ecg_torso_mesh_file, vita_ecg_heart_mesh_file)
            extract_intra_mesh_with_tags(vita_ecg_torso_mesh_file, vita_heart_cnf, vita_ecg_heart_mesh_file)
            # extract_vita_intra_mesh(job, vita_planf, intra_dir, vita_ecg_torso_mesh_file)


    # transform leadfield configuration
    # ---------------------------------
    if args.stage in 'LEADFIELD':
        # inputs
        if args.torso is None:
            src_lff = os.path.join(torso_dir, torso_ID, torso_ID + '.torso' + '.lf' + '.json')
        else:
            src_lff = os.path.join(torso_dir, torso_ID, args.leadfield)

        src_ptsf = os.path.join(torso_dir, torso_ID, torso_ID + '.torso' + '.bpts')
        trg_ptsf = os.path.join(vita_ecg_torso_mesh_file + '.bpts' )

        # transform leadfield configuration, select matching nodes between source and transformed
        # target points
        trfm_lff = mlf.trfm_leadfield(src_lff, src_ptsf, trg_ptsf, trfm)

        # compute leadfields, store in leadfield directory
        if args.lf_compute:
            #lf_dir = os.path.join(args.caseID, 'leadfield')
            mlf.compute_leadfields(args.caseID, trfm_lff, vita_ecg_torso_mesh_file, vita_planf, job)

    # run VITA
    # --------
    if args.stage in 'VITA':
        rvi_mapper = str(settings.execs.RVI_MAPPER)
        vita_mesh = vita_ecg_heart_mesh

        sr_vita.vita(rvi_mapper, args.ek_params, args.rtt, args.step_size, args.caseID, vita_mesh, args.num_stim)

    # convert VITA output to studio format
    # ------------------------------------
    if args.stage in 'STUDIO':
        v2s(args.caseID, vita_ecg_torso_mesh, vita_ecg_heart_mesh, vita_planf, args.launch)

    # compute ECGs from VITA induced sequences
    # ----------------------------------------
    if args.stage in 'ECG':
        VT_IDs = ecg_gen(args.caseID, vita_ecg_torso_mesh, vita_heart_mesh, job)
        visual = True
        if visual:
            plot_VT_ID_ecgs(VT_IDs, os.path.join(args.caseID, 'ecgs'))


if __name__ == '__main__':
    ecg_vita()
