#!/usr/bin/env python3

r"""
"""
import os, sys, shutil
import argparse

from datetime import date
from json import load, dump

from carputils import settings
from carputils import tools
from carputils import model
from carputils import restitute as r
from carputils.cep4cms import j2carp as j2c
from carputils.cep4cms import cell_limcyc as clc

EXAMPLE_DESCRIPTIVE_NAME = 'Limit cycle experiment'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>'
EXAMPLE_DIR = os.path.dirname(__file__)
GUIinclude = False


def parser():
    parser = tools.standard_parser()

    exp  = parser.add_argument_group('experiment specific options')

    exp.add_argument('--experiment', type=str, required=True, default='exp.json',
                       help='Macro parameter experiment description in .json format.')

    exp.add_argument('--domain',
                        type=str, nargs='+', default=argparse.SUPPRESS,
                        help='List of functional domains to be processed, ignoring any mutable flags')

    exp.add_argument('--update', action='store_true',
                       help='Update ep initial state in experiment description')

    exp.add_argument('--ignore-mutable', action='store_true',
                       help='Ignore any mutable flags in the experiment description (default is ignore).')

    # set up limit cycle protocol
    lc = parser.add_argument_group('limit cycle protocol')

    lc.add_argument('--bcl',
                       type=float,
                       help='basic cycle length for limit cycle experiment in [ms].')

    lc.add_argument('--cycles',
                       type=int, default=20,
                       help='Number of cycles to approximated stable limit cycle.')

    # APD restitution settings
    apd_res = parser.add_argument_group('action potential duration restitution options')

    apd_res.add_argument('--restitute', action='store_true',
                          help='Compute restitution curves from limit cycle state.')

    apd_res.add_argument('--plot-apd-res',  action='store_true',
                        help='Plot APD restitution curves')

    return parser


def jobID(args):
    """
    Generate name of top level output directory.
    """
    if not args.restitute:
        exp_str = 'ep_init'
    else:
        exp_str = 'restitute'

    today = date.today()
    out_DIR = '{}_{}'.format(today.isoformat(), exp_str)
    return out_DIR


def ep_limit_cycle(job,args, exps, lim_cyc_dir):

    n_acts, exps = clc.ep_limit_cycle(job, exps, args.bcl, args.cycles, lim_cyc_dir, args.domain, args.visualize, args.ignore_mutable)

    return n_acts, exps


def ep_apd_restitution(job,args, exps, lim_cyc_dir):

    n_acts, exps, apdResFiles, apdResLabels, apResBeats = clc.ep_apd_restitution(job, exps, args.bcl, args.cycles, lim_cyc_dir,
                                                                     args.domain, args.visualize, args.ignore_mutable, args.dry)

    return n_acts, exps, apdResFiles, apdResLabels, apResBeats


def visualize_limit_cycle(job, exps, experiment, domain_lst):

    # limit cycle visualization
    print('Visualize limit cycle experiments')
    print('=' * 88 + '\n\n')

    domains = exps.get('functions')
    for dkey, domain in domains.items():

        # only look at specified domains
        if domain_lst is not None and dkey not in domain_lst:
            continue

        ep = domain.get('EP', None)

        if ep is None:
            print('\nNo EP defined for domain {} !\n'.format(dkey))
            continue

        if ep.get('mutable'):

            print('\nVisualize {} limit cycle experiment.'.format(dkey))
            print('-' * 88 + '\n\n')

            # create binary state variable trace file
            b2h5 = [settings.execs.SV2H5B,
                    '{}/{}_limit_cycle_header.txt'.format(job.ID, dkey),
                    '{}/{}.h5'.format(job.ID, dkey)]
            job.bash(b2h5)

            # create binary state variable trace file
            vh5 = [settings.execs.LIMPETGUI,
                   '{}/{}.h5'.format(job.ID, dkey)]

            job.bash(vh5)

        else:
            print('\nLimit cycle experiment not executed for {}.'.format(dkey))
            print('Set mutable flag in {} EP section of {}.'.format(dkey, experiment))
            print('-' * 88 + '\n\n')


@tools.carpexample(parser, jobID, clean_pattern='^(\d{4}-)|(_restitute)|(_ep_init)|(trace_header)|(Trace)')
def run(args, job):

    # read experiment description
    with open(args.experiment) as fp:
        exps = load(fp)

    # domain restrictions, check validity of domain names
    if not 'domain'in args:
        args.domain = None
    else:
        args.domain = j2c.valid_keys(exps.get('functions'), args.domain)

    # store all limit cycle initial state vectors in one place
    lim_cyc_dir = j2c.get_init_dir(exps.get('tuning_base_dir', './init'), args.update)

    # make sure limit cycle directory exists
    if not os.path.isdir(lim_cyc_dir):
        os.mkdir(lim_cyc_dir)

    if not args.restitute:
        n_acts, exps = ep_limit_cycle(job, args, exps, lim_cyc_dir)

    else:
        n_acts, exps, apdResFiles, apdResLabels, apResBeats = ep_apd_restitution(job, args, exps, lim_cyc_dir)

        if args.plot_apd_res:
            # restitution experiment
            if args.dry:
                print('Show APD restitution curves')
                print('-'*88)
                print('plotAPDrestitution {} --labels {} --slope\n'.format(' '.join(apdResFiles), ' '.join(apdResLabels)))
                print('plotAPDrestitution {} --labels {} --beats {}\n'.format(' '.join(apdResFiles), ' '.join(apdResLabels), ' '.join(apResBeats)))
            else:
                # plot restitution curve
                r.plotAPDrestitution( apdResFiles, apdResLabels, plt_slope=True, plt_pcl=True)
                # plot restituted APs
                r.plotRestitutionAPs(apdResFiles, apdResLabels, apResBeats)

    if n_acts > 0:
        clc.print_init_summary(exps.get('functions'), args.cycles)

        if args.update:
            with open(args.experiment, 'w') as fp:
                dump(exps, fp, indent=4)

    if args.visualize:
        if not args.restitute:
            # limit cycle experiment
            visualize_limit_cycle(job, exps, args.experiment, args.domain)


if __name__ == '__main__':
    run()
