#!/usr/bin/env python3

"""
Plot APD restitution curve as generated with bench
"""
import argparse
from matplotlib import pyplot as plt
import numpy as np
from carputils import restitute as r

def parser():

    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('files',
                        nargs='+',
                        help='Path(s) of the restitution table files to load and plot')
    parser.add_argument('--labels',
                        nargs='+',
                        default='',
                        help='labels for APD restitution experiment. If given, number of labels should match number of files.')
    parser.add_argument('--slope',
                        action='store_true',
                        help='Plot slope of APD restitution curve')
    parser.add_argument('--pcl',
                        action='store_true',
                        help='Plot APD as function of pacing cycle length')
    parser.add_argument('--beats',
                        type=str,
                        nargs='+',
                        help='Action potential trace files to extract APs of premature beats.')
    return parser


if __name__ == '__main__':
   
   # build parser
   opts = parser().parse_args()

   r.plotAPDrestitution(opts.files, opts.labels, bool(opts.slope), bool(opts.pcl))

   if opts. beats is not None:
       # make sure we have a matching trace file for each restitution table
       if len(opts.beats) != len(opts.files):
           print('Number of AP trace files must match number of restitution files.')
       else:
           r.plotRestitutionAPs(opts.files, opts.labels, opts.beats)