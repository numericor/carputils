#!/usr/bin/env python3

import argparse
import os.path, time
import sys
import numpy as np
import martinsVITAtools as VITA

from carputils import tools
from carputils.vita import sr_vita

def parser():

    parser = tools.standard_parser()
    exp  = parser.add_argument_group('VITA experiment  options')


    exp.add_argument('--folder', type=str, default=None,
                        help='Provide location of folder containing the mesh and all surface files')
    exp.add_argument('--mesh', type=str, default=None, help='Provide name of the mesh')
    exp.add_argument('--rvi_mapper', type=str, default=None, help='Provide location of rvi_mapper binary')
    exp.add_argument('--ek_params', type=str, default=None, help='Provide location of ek init parameter file')
    exp.add_argument('--num_stim', type=int, default=None, help='Provide number of stimuli')
    exp.add_argument('--step_size', type=float, default=None, help='Provide step size for distance field')
    exp.add_argument('--RTT', type=float, default=None, help='Provide min RTT threshold')

    return parser


def jobID(args):
    """
    Generate a job ID
    """
    sID = 'caseID'

    return sID

@tools.carpexample(parser, jobID, clean_pattern='^(\d{4}-)|^(lf_tmp)')
def vita(args, job):
    sr_vita.vita(args.rvi_mapper, args.ek_params, args.RTT, args.step_size, args.folder, args.mesh, args.num_stim)

if __name__ == "__main__":
    vita()

