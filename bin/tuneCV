#!/usr/bin/env python3

"""
tuning

tune conductivity settings to match prescribed conduction velocities
"""
from carputils import tools
from carputils import ep
from carputils import tuning
import matplotlib.pyplot as plt
from numpy import loadtxt


def parser():
    parser = tools.standard_parser()
    parser.register('type', 'bool', str2bool)

    # mesh settings
    mesher = parser.add_argument_group('Mesher Options')
    mesher.add_argument('--resolution',
                         type=float, default=100,
                         help='mesh resolution in um (default is 100. um)')
    mesher.add_argument('--length',
                         type=float, default=1.0,
                         help='length of strand in cm (default is 1.0 cm)')
    mesher.add_argument('--surf',
                         type='bool', default=False,
                         help='set True to run in a surface mesh')

    # ionic model settings
    ionic = parser.add_argument_group('Ionic Model Options')
    ionic.add_argument('--model',
                         type=str, default='MBRDR',
                         help='Ionic model (default: MBRDR)')
    ionic.add_argument('--modelpar',
                         type=str, default='',
                         help='Modification of model parameters (default: \'\' )')
    ionic.add_argument('--plugin',
                         type=str, default='',
                         help='Ionic model (default: \'\')')
    ionic.add_argument('--plugpar',
                         type=str, default='',
                         help='Modification of model plugin parameters (default: \'\' )')
    ionic.add_argument('--svinit',
                         type=str, default='',
                         help='File holding state variable initialization vector (default: \'\' )')

    # conductivity/velocity settings
    electrics =  parser.add_argument_group('Electrical Options')
    electrics.add_argument('--velocity',
                         type=float, default=0.6,
                         help='desired conduction velocity in m/s (default: 0.6 m/s)')
    electrics.add_argument('--gi',
                         type=float, default=0.174,
                         help='Intracellular conductivity in S/m (default: 0.174 S/m)')
    electrics.add_argument('--ge',
                         type=float, default=0.625,
                         help='Extracellular conductivity in S/m (default: 0.625 S/m)')
    electrics.add_argument('--beta',
                         type=float, default=0.14,
                         help='Membrane surface-to-volume ratio (default: 0.14 cm^-1)')
    electrics.add_argument('--beqm',
                        type=int,
                        default=1, 
                        choices=[0,1], 
                        help='Use bidomain equivalent harmonic mean tensor (1) or intracellular tensor (0).'+
                             '(default is using the harmonic mean tensor)')      
    electrics.add_argument('--sourceModel',
                        default='monodomain', 
                        choices=ep.MODEL_TYPES, 
                        help='pick type of electrical source model (default is monodomain)')    
    electrics.add_argument('--tol',
                        type=float, default=0.01,
                        help='Percentage of error in conduction velocity (default: 0.01)')
    electrics.add_argument('--converge',
                        type='bool', default=False,
                        help='Iterate until converged velocity (default: False)')
    electrics.add_argument('--mode',
                        choices=['gm', 'gi'], default='gm',
                        help='Pick tuning strategy, iterate harmonic mean (gm) or intracellular conductivity (default: gm)')
    electrics.add_argument('--maxit',
                        type=int, default=20,
                        help='Maximum number of conduction velocity iterations (default: 20)')

    # CV restitution settings
    cv_res = parser.add_argument_group('Conduction Velocity Restitution Options')
    cv_res.add_argument('--pcl', 
                        type=float, nargs='+',
                        help='List of pacing cycle length for measuring CV restitution')
    cv_res.add_argument('--plot-cv-res',  action='store_true',
                        help='Plot CV restitution curve')

    # numerical settings
    # it is important that the defaults match those used by openCARP
    numerics = parser.add_argument_group('Numeric Options')
    numerics.add_argument('--dt',
                        type=float, default=10.,
                        help='Integration time step on micro-seconds (default: 10. us)')
    numerics.add_argument('--stimS',
                        type=float, default=250.,
                        help='Stimulus strength delivered to the strang (default: 250. uA/cm^2')
    numerics.add_argument('--stimV',action='store_true',
                        help='Use transmembrane voltage clamp to stimulate cable' )
    # -parab_solve: current openCARP default is 1 (Crank-Nicolson)
    numerics.add_argument('--ts',
                        type=int, default=1,
                        choices=[0,1],
                        help='Choose time stepping method 0: explicit, 1: CN (default: 1)')
    numerics.add_argument('--stol',
                        type=float, default=1.e-9,
                        help='Solver tolerance (default: 10^-9)')
    # -mass_lumping: current openCARP default is True
    numerics.add_argument('--lumping',
                        type='bool', default=True,
                        help='Use mass lumping (default: True)')
    # -operator_splitting: current openCARP default is 1 (use splitting)
    numerics.add_argument('--opsplit',
                        type='bool', default=True,
                        help='Use operator splitting (default: True)')

    # output settings
    output = parser.add_argument_group('Output Options')
    output.add_argument('--log',
                        type=str, default='tuneCV.log',
                        help='Store simulation output to a log file.')
    output.add_argument('--cvres',
                        type=str, default='cv_restitution.dat',
                        help='Store CV restitution data to file.')
    output.add_argument('--table',
                        type=str, default='table.dat',
                        help='Store tuneCV parameters and obtained velocities in a table.')
    output.add_argument('--carp-params', action='store_true',
                        help='Write parameter settings for carp simulations.')

    return parser


def str2bool(s):
    """
    Convert string to bool (in argparse context).
    """
    if s.lower() not in ['true', 'false']:
        raise ValueError('Need bool; got %r' % s)
    return {'true': True, 'false': False}[s.lower()]


def jobID(args):
    """
    Generate name of top level output directory.
    """
    #today = date.today()
    jID = None
    if not args.ID:
        # use default job ID
        tpl = 'imp_{}_vel_{}_dx_{}'
        jID = tpl.format(args.model, args.velocity, args.resolution)

    else:
        jID = args.ID

    if args.suffix:
        jID = jID + args.suffix

    return jID


@tools.carpexample(parser,jobID)
def run(args,job):

    # create CV tuning object
    to = tuning.CVtuning()

    # configure with given args
    to.config(args)

    # overview of parameters used
    to.summary()
    
    # output carp parameter structures
    if args.carp_params:
        to.carp_params()

    # run iterative tuning experiment
    if args.pcl is None:
        gi, ge, CV, gm, it, flg = to.iterateCV(job)
    else:
        pcl, CVs = to.restituteCV(job)
        if args.plot_cv_res:
            to.plotCVRestitutionCurve()

        
    # visualize experiment
    if args.visualize:
        
        # Prepare file paths
        if args.pcl is not None:
            jID = '{}_CVres'.format(job.ID)
        else:
            if not args.converge:
                jID = '{}'.format(job.ID)
            else:
                jID ='{}_It_{}'.format(job.ID, to.last_it)


        geom = '{}/block_i'.format(jID)
        data = '{}/vm.igb'.format(jID)
        view = 'view_cvtune_vm.mshz'
        
        # Call meshalyzer
        job.meshalyzer(geom, data, view)

    # need this exit, otherwise job.bash() does not realize that the job finished
    exit(0)

if __name__ == '__main__':
    run()
