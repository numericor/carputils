#!/usr/bin/env python3

r"""
"""
import os, sys, argparse
from datetime import date
from json import load, dump
#from shutil import copy
#import numpy as np

#from carputils import settings
from carputils import tools
#from carputils import model
#from carputils import mesh
from carputils.cep4cms import j2carp as j2c
from carputils.cep4cms import tissue_limcyc as tlc
from carputils import tuning as tune

EXAMPLE_DESCRIPTIVE_NAME = 'Tissue experiment velocity and conductivity tuning'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>'
EXAMPLE_DIR = os.path.dirname(__file__)
GUIinclude = False


def parser():
    parser = tools.standard_parser()

    # experiment selection and generic overall options
    exp  = parser.add_argument_group('Experiment selection and general options')

    exp.add_argument('--experiment', type=str, required=True, default='exp.json',
                       help='Macro parameter experiment description in .json format.')

    exp.add_argument('--domain',
                        type=str, nargs='+', default=argparse.SUPPRESS,
                        help='List of domains to be processed, ignoring any mutable flags')
    
    exp.add_argument('--update', action='store_true',
                       help='Update conductivity and velocity settings in experiment description')

    # tuning stage arguments
    tune_args  = parser.add_argument_group('Tuning specific options')

    tune_args.add_argument('--tune-mode', 
                       choices=['gi', 'gm'], default='gm',
                       help='Select tuning strategy, change gi, gm or beta to obtain prescribed velocities.')

    tune_args.add_argument('--converge', action='store_true', default=False,
                       help='Enforce recomputation of conductivities to match given reference conduction velocities. ' + 
                            'This overrules settings in the experiment description to create a new set of conductivity values.' )

    tune_args.add_argument('--ctol',
                       type=float, default=1e-4,
                       help='Sets conduction velocity error limit for conduction velocity tuning.')

    tune_args.add_argument('--resolution',
                       type=float, default=argparse.SUPPRESS,
                       help='mesh resolution in [um], default is: 250. um' +
                            'if given, overrules any resolution settings  given in the --experiment file.' )

    tune_args.add_argument('--tag',
                       type=str, default='tune_cv',
                       help='Use tag for named output')
    
    # CV restitution settings
    cv_res = parser.add_argument_group('Conduction Velocity Restitution Options')
    cv_res.add_argument('--restitute-pcl', 
                        type=float, nargs='+',
                        help='List of pacing cycle length for measuring CV restitution')
    cv_res.add_argument('--plot-cv-res',  action='store_true',
                        help='Plot CV restitution curve')

    return parser


def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    out_DIR = '{}_tissue_tuning'.format(today.isoformat())
    return out_DIR


def converge_CVs(job, exps, args):
    """
    """

    cvg   = args.converge
    ctol  = args.ctol
    tmode = args.tune_mode
    if 'resolution' in args:
        dx = args.resolution
    else:
        dx = None
    if 'restitute_pcl' in args:
        pcls = args.restitute_pcl
    else:
        pcls = None
    np = args.np
    visual = args.visualize
    dry = args.dry
    tag = args.tag
    domain_lst = args.domain
    carp_opts = args.CARP_opts
    ID = args.ID

    # store all limit cycle initial state vectors in one place
    lim_cyc_dir = j2c.get_init_dir(exps.get('tuning_base_dir', './init'), args.update)

    cv_tune = tlc.converge_CVs(job, exps, cvg, ctol, tmode, dx, pcls, lim_cyc_dir, domain_lst, tag, np, visual,
                               ID, carp_opts, dry)

    if not pcls:
        with open('cv_tune_all.json', 'w') as fp:
            dump(cv_tune, fp, indent=4)
    else:
        with open('cv_restitution_all.json', 'w') as fp:
            dump(cv_tune, fp, indent=4)

        if args.plot_cv_res:
            CV_res_files, CV_res_labels = tlc.get_cv_res_files(cv_tune)

            if not dry:
                tune.plotCVRestitution(CV_res_files, CV_res_labels)

    return cv_tune


@tools.carpexample(parser, jobID, clean_pattern='^(\d{4}-)|(imp_)|(tunecv_)')
def run(args, job):

    # read experiment description
    with open(args.experiment) as fp:
        exps = load(fp)

    # domain restrictions, check validity of domain names
    if not 'domain'in args:
        args.domain = None
    else:
        args.domain = j2c.valid_keys(exps.get('functions'), args.domain)

    # carry out tissue initialization experiment
    # - measure CV
    # - tune CV
    # - measure CV restitution
    cv_tune = converge_CVs(job, exps, args)

    #tlc.print_restitute_results(cv_tune)

    # print results and update experiment settings
    if not args.dry:

        if args.restitute_pcl:
            # summarize CV restitution results
            tlc.print_restitute_results(cv_tune)

            # update tissue settings
            new_exp = tlc.update_exp_CVres(exps, cv_tune)

        else:
            # get domains
            domains = exps.get('functions')

            # get solver options
            solver = exps['solver']

            # summarize tuning results for all domains
            tlc.print_tune_cv_results(cv_tune, domains, solver)

            # update tissue settings
            new_exp = tlc.update_exp_CV(exps, cv_tune)

        # update experiment description
        tlc.update_exp_desc(args.update, args.experiment, new_exp)

if __name__ == '__main__':
    run()
