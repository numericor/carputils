#!/usr/bin/env python3

r"""
"""
import os, sys
from datetime import date

#from carputils import settings
from carputils import tools
#from carputils import model
#from carputils import mesh
from carputils.cep4cms import j2carp as j2c
from carputils.cep4cms import prepace as prp

EXAMPLE_DESCRIPTIVE_NAME = 'Prepacing and validation of tissue/organ models'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>'
EXAMPLE_DIR = os.path.dirname(__file__)
GUIinclude = False


def parser():
    parser = tools.standard_parser()

    # experiment selection and generic overall options
    exp  = parser.add_argument_group('Experiment selection and general options')

    exp.add_argument('--config', type=str, required=True, default='conf.json',
                       help='Describe tissue model in terms of labels and function association.')

    exp.add_argument('--experiment', type=str, required=True, default='exp.json',
                       help='Macro parameter experiment description in .json format.')

    exp.add_argument('--plan', type=str, required=True, default='plan.json',
                       help='Simulation plan description in .json format.')

    exp.add_argument('--cohort', type=str, default=None,
                     help='Directory storing cohort of all cases belonging to a given study')

    exp.add_argument('--caseID', type=str, default=None,
                     help='Pick a specific case from the cohort directory.')
    
    exp.add_argument('--mesh', type=str, default=None,
                       help='Path and basename of mesh to be prepaced (/path_to_mesh_dir/mesh_base_name).')

    exp.add_argument('--etagsname', type=str, default=None,
                      help='Path to an alternative element tags file (*.tag, *.btag)')

    exp.add_argument('--create-split', action='store_true', default=False,
                     help='Force re-creation of the splitting file.')

    # selection of split file and tags file at the cmd line currently not supported.

    exp.add_argument('--stage', 
                       choices=['gen-lat', 'prepace', 'validate'], default='prepace',
                       help='Select stage: tune for velocities, prepace or validate prepaced state (default: tune).')

    exp.add_argument('--electrodes', type=str,
                       default=None,
                       help='List of electrode descriptions in json format. ' +
                            'This is optional, electrodes may be listed also ' +
                            'in the \"electrodes\" section of the experiment description.')

    exp.add_argument('--protocols', type=str,
                       default=None,
                       help='List of protocol descriptions in json format. ' +
                            'This is optional, protocols may be listed also ' +
                            'in the \"protocols\" section of the experiment description.')

    exp.add_argument('--protocol', type=str, required=True,
                      default=None,
                      help='Pick a specific pre-pacing protocol from protocol list ' +
                           'provided through --protocols, or within a \"protocols\" section ' +
                           'in --experiment.')

    exp.add_argument('--update', action='store_true',
                       help='Update LAT vector in experiment description')

    # generation of lat args
    gl_args  = parser.add_argument_group('LAT-generation specific options')
    gl_args.add_argument('--gen-lat',
                       choices=['ek', 'rd'], default='ek',
                       help='Generate an activation sequence to create a LAT vector.\n' +
                       'Choices are:\n' +
                       '(ek): use eikonal method to create an LAT vector \n' +
                       '(rd): use reaction-diffusion simulation to create an LAT vector.')


    gl_args.add_argument('--lat-name', type=str, default=None,
                         help='Provide file name for storing lat vector.')

    # prepacing stage args
    pp_args_hd = 'Pre-pacing specific options'
    sep_line = '-'*80 
    arg_sep  = '\n%s\n'%(sep_line)
    pp_args  = parser.add_argument_group('%s'%(pp_args_hd))

    pp_args.add_argument('--prepace', type=str, default='off', 
                       help='Select pre-pacing mode to find tissue/organ scale limit cycle.\n' +
                       'Choices are: \n' +
                       '(off): use single cell initial state only \n' +
                       '(lat): use lat-defined activation sequence \n' +
                       '(lat-n): use lat-defined activation sequence + n (#cycles given by the protocol) full activation sequences ' +
                       'where n can be between 0 (use cycle number from protocol definition) up to 3'
                       'to account for diffusion effects \n' +
                       '(full): brute force, run full pre-pacing at tissue scale, without any lat-based prepacing.')

                       #choices=['off', 'lat', 'lat-n', 'full'], default='off',

    pp_args.add_argument('--lat', type=str, default=None,
                       help='Provide lat vector for prepacing.')



    val_args_hd = 'Validation specific options'
    sep_line = '-'*80 
    arg_sep  = '\n%s\n'%(sep_line)
    val_args  = parser.add_argument_group('%s'%(val_args_hd))

    val_args.add_argument('--ref', 
                       choices=['off', 'lat', 'lat-n', 'full'], default='off',
                       help='Pick a reference to compare currently selected prepace mode.')
    return parser


# return mesh extension as function of run
def mesh_ext(rmode):

    if rmode == 'ek':
        ext = '_ek'
    else:
        ext = '_i'

    return ext


# helper function to select stimulus mode
# voltage is more robust, but currently ignored in ek and re runs


# pick run mode 'rd' or 're'
def restart(job, meshname, cnf, fncs, elecs, pp_prtcl, args, mode, rmode,
            restart_chkpt, visual):

    split_file, use_split = j2c.generate_split_file(job, cnf, meshname,
                                                    force_recreation=args.create_split)

    # set up overall command line
    cmd = prp.setup_prepace(meshname, cnf, fncs, elecs, pp_prtcl, rmode,
                            tags_file=args.tags_file, use_split=use_split,
                            split_file=split_file)

    simID = 'validate_pp_{}'.format(mode)

    # determine checkpoint timing
    pp_bcl = pp_prtcl['bcl']
    if mode == 'full':
        pp_cyc = pp_prtcl['cycles']
        t_chk  = pp_bcl * pp_cyc
    else:
        t_chk  = 0

    # determine checkpoint to restart from
    cmd += ['-start_statef', restart_chkpt]
    
    cmd += ['-simID', simID, '-experiment', 0]

    # simulate one bcl
    cmd += [ '-tend', t_chk + pp_bcl ]

    vis = []
    if visual:
        vis = [ '-gridout_i', 3,
                '-spacedt',   1 ]

    cmd += vis

    return cmd, simID

# create mesh ID
def mesh_ID(meshname):
   
    meshID = os.path.basename(meshname)

    return meshID

# ensure consistent prepacing checkpoint filenames for restarting
def chkpt_file_ID(prefix, ID, gen_mode, t_chk, bcl):

    ID = '%s_%s_pp_%s_bcl_%.1f_tstamp_%.1f'%(prefix, ID, gen_mode, t_chk, bcl)

    return ID


# ensure consistent lat filenames
def lat_file_ID(prefix, ID, gen_mode, ext):

    ID = '%s_%s_%s.%s'%(prefix, ID, gen_mode, ext)

    return ID

def get_mesh(mesh, cohort, caseID):

    meshname = None

    if mesh:
        meshname = mesh
    elif cohort and caseID:    
        meshname = '{}/{}/{}'.format(cohort, caseID, caseID)
    else:
        print('\n\nNo mesh specified, use either mesh or cohort+caseID arguments.\n')
        sys.exit()

    return meshname    

def stage_gen_lat_from_args(job, cnf, exps, elecs, prtcls, args):
    """
    Execute lat sequence generation

    :param job:
    :param cnf:
    :param exps:
    :param elecs:
    :param prtcls:
    :param args:
    :return:
    """
    #init_prfx = j2c.get_init_dir(args.init_dir, args.update)
    init_prfx = j2c.get_init_dir(exps.get('tuning_base_dir', './init'), args.update)
    pp_prtcl  = prtcls.get(args.protocol)
    mesh      = get_mesh(args.mesh, args.cohort, args.caseID)
    protocol  = args.protocol
    gen_lat   = args.gen_lat
    visual    = args.visualize

    split_file, use_split = j2c.generate_split_file(job, cnf, mesh,
                                                    force_recreation=args.create_split)

    # retrieve definition of all functional domains
    fncs = exps.get('functions')

    # generate a new LAT vector if required
    lat_file = None
    if args.lat_name:
        lat_file = os.path.join(init_prfx, args.lat_name)
    else:
        lat_pfix = '{}_act_seq'.format(protocol)
        lat_file = os.path.join(init_prfx, lat_file_ID(lat_pfix, mesh_ID(mesh), gen_lat, 'dat'))
    print('Generating {}-based lat vector file {}'.format(gen_lat, lat_file))

    cmd_lat, tmp_lat, simID = prp.generate_lat(mesh, cnf, exps, elecs, pp_prtcl, args.gen_lat,
                                               lat_file, visual, tags_file=args.etagsname,
                                               use_split=use_split, split_file=split_file)

    # generate lat vector
    job.carp(cmd_lat)

    # check whether lats file has been properly created
    print('\nCopying LAT vector')
    job.cp(tmp_lat, lat_file)

    # update LAT vector in experiment description
    if not args.dry:
        # update path to lat file in experiment protocols section
        p = exps.get('protocols')
        if p and p.get(args.protocol):
            p['lat'] = lat_file
            j2c.update_exp_desc(args.update, args.experiment, exps)

        if args.protocols:
            dct = j2c.load_json(args.protocols)
            prtcls = dct.get('protocols')
            p = prtcls.get(args.protocol)

            if p:
                p['lat'] = lat_file
                j2c.update_exp_desc(args.update, args.protocols, dct)

    if visual:
        lat_mesh = os.path.join(simID, os.path.basename(mesh) + mesh_ext(args.gen_lat))
        job.meshalyzer(lat_mesh, lat_file)


def stage_prepace_from_args(job, cnf, exps, elecs, prtcls, args):
    """
    Execute prepacing stage

    :param job:
    :param cnf:
    :param exps:
    :param elecs:
    :param prtcls:
    :param args:
    :return:
    """
    #init_prfx = j2c.get_init_dir(args.init_dir, args.update)
    init_prfx = j2c.get_init_dir(exps.get('tuning_base_dir', './init'), args.update)
    pp_prtcl  = prtcls.get(args.protocol)
    mesh      = get_mesh(args.mesh, args.cohort, args.caseID)
    protocol  = args.protocol
    gen_lat   = args.gen_lat
    prepace   = args.prepace
    lat       = args.lat
    visual    = args.visualize

    # retrieve definition of all functional domains
    fncs = exps.get('functions')

    # prepacing using lat, make sure we have a proper lat vector
    pp_lat_file = pp_prtcl.get('lat')
    lat_file = None
    if prepace.startswith('lat'):
        # we need an activation lat vector
        if lat:
            # command line provided gets precedence
            lat_file = lat
        elif pp_lat_file is not None:
            # experiment description provides lat
            # check whether given lat file exists
            if os.path.exists(pp_lat_file):
                lat_file = pp_lat_file
            else:
                print('\nPrepacing lat file {} does not exist.'.format(pp_lat_file))

        if lat_file is None:
            # we don't have a lat file, need to generate one
            print('\n\n')
            print('LAT-based prepacing requires LAT vector which is not provided.')
            print(' 1) Provide lat file using --lat')
            print(' 2) Or generate lat file first using --gen-lat ek or rd.\n')
            sys.exit()

    # before running, check all specified initial states are there
    if not j2c.check_initial_state_vectors(fncs, True):
        # some initial state vectors are missing
        print('\nMissing initial state vectors, exiting!\n\n')
        sys.exit()

    split_file, use_split = j2c.generate_split_file(job, cnf, mesh,
                                                    force_recreation=args.create_split)

    # ready to run prepacing to create initial checkpoint for restarting
    print('Run prepacing in mode {}'.format(prepace))
    propagation = pp_prtcl.get('propagation')

    cmd_pp, chkptf, t_chkpts, simID = prp.prepace(mesh, cnf, fncs, elecs, pp_prtcl, prepace,
                                                  lat_file, propagation, visual, tags_file=args.etagsname,
                                                  use_split=use_split, split_file=split_file)

    if propagation!='ek':
        slv = exps.get('solver')
        cmd_pp += j2c.add_slv_settings(slv)

    # run
    job.carp(cmd_pp)

    # copy checkpoints to init directory for restarting
    for t_chk in t_chkpts:
        print('\nCopying prepacing checkpoint file')
        chkpt_file = os.path.join(simID, chkptf + '.%.1f' % (t_chk) + '.roe')
        rstrt_file = os.path.join(init_prfx,
                                  chkpt_file_ID(protocol, mesh_ID(mesh), prepace, pp_prtcl.get('bcl'),
                                                t_chk) + '.roe')

        job.cp(chkpt_file, rstrt_file)

    if not args.dry:

        # update path to restart file in experiment protocols section
        pp_prtcl = prtcls.get(args.protocol)

        p = exps.get('protocols')
        if p and p.get(args.protocol):
            p['restart'] = rstrt_file
            j2c.update_exp_desc(args.update, args.experiment, exps)

        if args.protocols:
            dct = j2c.load_json(args.protocols)
            prtcls = dct.get('protocols')
            p = prtcls.get(args.protocol)

            if p:
                p['restart'] = rstrt_file
                j2c.update_exp_desc(args.update, args.protocols, dct)

    # summarize prepacing
    prp.print_prepace_summary(args.protocol, p, cnf, exps, args.prepace, args.lat, rstrt_file)

    if args.visualize:
        mesh = os.path.join(simID, os.path.basename(mesh) + mesh_ext(propagation))
        data = os.path.join(simID, 'vm.igb')
        job.meshalyzer(mesh, data)

    if not args.dry:
        # return handle to pre-paced checkpoint for restarting
        return rstrt_file


def check_input(cnf, exps, elecs, prtcls, prtcl, pp_arg):

    ok = True

    # check whether configuration matches up with functional description
    fncs = exps.get('functions')
    #fncs = exps
    if not j2c.check_config(cnf, fncs):
        ok = False
        return ok

    # check if requested prepacing protocol is in list
    if not prtcls.get(prtcl):
        print('\n\nRequested protocol {} not provided.'.format(prtcl))
        ok = False
        return ok

    # check validity of prepace argument
    if pp_arg == 'off' or pp_arg.startswith('lat') or pp_arg == 'full':
        # separate check on lat-
        if pp_arg.startswith('lat-'):
            rep = pp_arg.split('lat-')[1]
            if rep.isdigit():
                n = int(rep)
                if n in range(0,4):
                    ok = True
                else:
                    print('Cycle parameter n provided through --prepace lat-n must be in range 0-3.')
                    ok = False
        else:
            ok = True
    else:
        print('Argument --prepace {} is out of range.'.format(pp_arg))
        ok = False
        
    return ok

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    out_DIR = '{}_tissue_tuning'.format(today.isoformat())
    return out_DIR


@tools.carpexample(parser, jobID, clean_pattern='^(\d{4}-)|(imp_)|(tunecv_)')
def run(args, job):

    # read tissue/organ configuration
    #cnf, exps, elecs, prtcls, plan = j2c.load_experiment(cnf_file=args.config, exp_file=args.experiment,
    #                                               plan_file=args.plan,
    #                                               prtcls_file=args.protocols, elecs_file=args.electrodes)

    cnf, exps, fncs, elecs, prtcls, slv_, plan_ = j2c.load_experiment(args.config, args.experiment, args.plan, args.electrodes, args.protocols)

    if not check_input(cnf, exps, elecs, prtcls, args.protocol, args.prepace):
        sys.exit()

    # ---------- GEN-LAT --------------------------------------------
    if args.stage == 'gen-lat':
        stage_gen_lat_from_args(job, cnf, exps, elecs, prtcls, args)

    # ---------- PREPACE --------------------------------------------
    if args.stage == 'prepace':
        rstrt_chkpt = stage_prepace_from_args(job, cnf, exps, elecs, prtcls, args)

    # ---------- VALIDATE --------------------------------------------
    if args.stage == 'validate':

        print('\n\nValidate stage not developed yet.')
        sys.exit()

        # read in all electrode and relative timings used for prepacing
        elecs  = exps['electrodes']
        prtcls = exps['protocols']
        pp_prtcl    = prtcls['prepace']
        pp_lat_file = pp_prtcl['lat']
        propagation = pp_prtcl.get('propagation')
        mesh        = exps['mesh']

        # pick prepacing checkpoint to restart from 
        chkpt_file = os.path.join('./init', chkpt_file_ID('prepaced', mesh_ID(mesh), args.prepace, pp_prtcl.get('bcl')))

        cmd_chk, simID = restart(job, mesh, cnf, fncs, elecs, pp_prtcl, args.prepace,
                                 propagation, chkpt_file, args.visualize)

        slv = exps.get('solver')
        cmd_chk += j2c.add_slv_settings(slv)

        # before running, check all specified initial states are there
        # as we are restarting, we should remove initial state vectors anyways as this is encoded in the checktpoint
        if not j2c.check_initial_state_vectors(fncs, True):
            # some initial state vectors are missing
            print('\nMissing initial state vectors, exiting!\n\n')
            sys.exit()

        job.carp(cmd_chk)

        if args.visualize:
            rst_mesh = os.path.join(simID, os.path.basename(mesh) + mesh_ext(propagation))
            rst_data = os.path.join(simID, 'vm.igb')
            job.meshalyzer(rst_mesh, rst_data)


if __name__ == '__main__':
    run()
