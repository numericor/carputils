#!/usr/bin/env python3

r"""
"""
import os, sys
from datetime import date
from json import load, dump

#from carputils import settings
from carputils import tools
from carputils.cep4cms import j2carp as j2c


EXAMPLE_DESCRIPTIVE_NAME = 'Translate json dictionaries into carp* command line'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>'
EXAMPLE_DIR = os.path.dirname(__file__)
GUIinclude = False


def parser():
    parser = tools.standard_parser()

    # experiment selection and generic overall options
    exp  = parser.add_argument_group('Experiment selection and general options')

    exp.add_argument('--plan', type=str, default=None,
                       help='Describe simulation plan in json format including configuration, experimental settings, ' +
                            'electrodes and protocols. Other settings provided through --config, --experiment, ' +
                            '--electrode* and --protocol* are ignored.' )   

    exp.add_argument('--config', type=str, default=None,
                       help='Describe tissue model in terms of labels and function association in json format.')

    exp.add_argument('--experiment', type=str, default=None,
                       help='Macro parameter experiment description in .json format.')
 
    exp.add_argument('--electrodes', type=str,
                       default=None,
                       help='List of electrode descriptions in json format. ' +
                            'This is optional, electrodes may be listed also ' +
                            'in the \"electrodes\" section of the experiment description.')
    exp.add_argument('--electrode', type=str,
                       nargs='+',
                       default=None,
                       help='List of electrode names to be selected from electrode definitions.')

    exp.add_argument('--protocols', type=str,
                       default=None,
                       help='List of protocol descriptions in json format. ' +
                            'This is optional, protocols may be listed also ' +
                            'in the \"protocols\" section of the experiment description.')

    exp.add_argument('--protocol', type=str,
                      default='prepace',
                      help='Pick a specific pre-pacing protocol from protocol list' +
                           'provided through --protocols, or within a \"protocols\" section' +
                           'in --experiment.')

    exp.add_argument('--rmode', choices=['ek', 'rd', 're'], default='rd', 
                      help='Pick run mode')

    exp.add_argument('--cohort', type=str, default=None,
                     help='Directory storing cohort of all cases belonging to a given study')

    exp.add_argument('--case', type=str, default=None,
                     help='Pick a specific case from the cohort directory.')
    
    exp.add_argument('--mesh', type=str, default=None,
                       help='Path and basename of mesh to be used (/path_to_mesh_dir/mesh_base_name).')

    return parser


# ----- J2C -------------------------------------------------------

# helper function to select stimulus mode
# voltage is more robust, but currently ignored in ek and re runs
# decide on stimulus type as function of propagation mode
def stim_mode(prop):

    # by defaul in rd runs, use voltage clamp to achieve robust capture
    s_type = 9
    s_strength = -30

    # eikonal mediated propagation picks up only transmembrane stimuli
    if prop == 'ek' or prop == 're':
        # eikonal-mediated triggers on transmembrane current stimuli
        s_strength = 250
        s_type     = 0

    return s_type, s_strength


# get mesh
def get_mesh(mesh, cohort, case):

    meshname = None

    if mesh:
        meshname = mesh
    elif cohort and case:    
        meshname = '{}/{}/{}'.format(cohort, case, case)
    else:
        print('\n\nNo mesh specified, use either mesh or cohort+case arguments.\n')
        #sys.exit()

    return meshname 

# generate split-file
#def generate_split_file(job, cnf, mesh_name, force_recreation=False):
#    # check if 'SPLIT' entry exists.
#    # if not, return None
#    split = cnf.get('SPLIT', None)
#    if split is None:
#        return None
#    msg = 'Found splitting definition.'
#    msg += '\n'+'-'*len(msg)
#    print('\n'+msg)
#    # check if a file name is specified,
#    # if not create a default file name
#    fname = split.get('fname', None)
#    if fname is None:
#        fname = mesh_name+'.splt'
#    if not fname.endswith('.splt'):
#        fname += '.splt'
#    # if the file exists and creation is not forced
#    # return the file name
#    if os.path.exists(fname) and not force_recreation:
#        print('Splitting file {} already exists!'.format(fname))
#        return fname
#    bl, _ = j2c.get_bath_labels(cnf, pop=False)
#    # construct the op-string for the meshtool command
#    ops = split.get('ops', None)
#    op_list = []
#    for op in ops:
#        lhs_tags, rhs_tags = [], []
#        for label in op['lhs']:
#            if label not in bl:
#                lhs_tags.extend(cnf[label]['tags'])
#        for label in op['rhs']:
#            if label not in bl:
#                rhs_tags.extend(cnf[label]['tags'])
#        if len(lhs_tags) == 0 or len(rhs_tags) == 0:
#            continue
#        lhs_tags = map(str, sorted(lhs_tags))
#        rhs_tags = map(str, sorted(rhs_tags))
#        op_list.append(','.join(lhs_tags)+':'+','.join(rhs_tags))
#    if len(op_list) == 0:
#        return None
#    op_str = '/'.join(op_list)
#    # construct the meshtool command and run it
#    mt_cmd = ['generate', 'split',
#              '-msh={}'.format(mesh_name),
#              '-op={}'.format(op_str),
#              '-split={}'.format(fname)]
#    job.meshtool(mt_cmd)
#    print('Splitting file {} created!'.format(fname))
#
#    return fname


# setup mesh-related command options
def setup_mesh(meshname, split_file=None):

    cmd = []

    # use split information if available
    if split_file is not None:
        if os.path.exists(split_file):
            cmd += [ '-use_splitting', 1 ,
                     '-splitname',     split_file ]
        else:
            print('Splitting selected, but split file {} does not exist!'.format(split_file))
            print('Proceed without splitting.')

    cmd += [ '-meshname', meshname ]

    return cmd

    
# load all settings
def load_experiment(args):

    if args.plan is not None: 
        print('Using a simulation plan is not supported yet.')
        sys.exit()

    elif args.config and args.experiment:

        # read tissue/organ configuration
        cnf = j2c.load_json(args.config)
        if not cnf:
            print('\n\nFailed to load configuration from {}.\n'.format(args.config))
            sys.exit()
    
        # read experiment description
        exps = j2c.load_json(args.experiment)
        if not exps:
            print('\n\nFailed to load experiment from {}.\n'.format(args.experiment))
            sys.exit()
    
        # use consistent solver settings
        slv = exps.get('solver')
        if not slv:
            print('\n\nNo solver settings provided in {}, using default settings.\n'.format(args.experiment))
    
        # read in all available electrodes, either from experiment or from dedicated electrode file
        elecs = j2c.get_dict_sec('electrodes', args.experiment, args.electrodes)
        act_elecs = {}
        if elecs and args.electrode:
            for e in args.electrode:
                if e in elecs:
                    act_elecs[e] = elecs.get(e)
                    print(e)
        else:
            print('No electrode definitions provided.')
        #    sys.exit()
    
        # read in all defined protocols, either from experiment or from dedicated protocol file
        prtcls = j2c.get_dict_sec('protocols', args.experiment, args.protocols)
        if prtcls:
            # pick prepacing protocol from protocol list
            prtcl = prtcls.get(args.protocol)
            if not prtcl:
                print('\n\nProtocol %s not found in list of protocols.\n'%(args.protocol))
                #sys.exit()
        else:
            print('\n\nNo protocol sections found!\n')
        #    sys.exit()

    else:
        print('Provide either a simulation plan through --plan, ' +
              'or both a configuration and experimental description though --config and -experiment.' )
        sys.exit()


    # additional checks

    # retrieve definition of all functional domains
    fncs = exps.get('functions')

    # check whether configuration matches up with functional description
    if not j2c.check_config(cnf, fncs):
        sys.exit()

    # check of request prepacing protocol is in list
    if not prtcls.get(args.protocol):
        print('\n\nRequested protocol {} not provided.'.format(args.protocol))
        #sys.exit()

    # if a mesh-name is provided via CLI, overwrite or
    # insert the mesh in the experiments dictionary
    mesh = get_mesh(args.mesh, args.cohort, args.case)
    if mesh is not None:
        exps['mesh'] = mesh

    return cnf, exps, elecs, act_elecs, prtcls, slv


def stimuli(elecs, rmode):

    # assign protocols to all electrodes used for pre-pacing
    stims = []
    for i, e in enumerate(elecs):
        elec, idx_hd = j2c.elec_sec_to_carp(elecs, i, e)

        ptcl = []
#        ptcl = [ '-stim[{}].ptcl.start'.format(i),     pp_rtimes[i],
#                 '-stim[{}].ptcl.duration'.format(i),  2,
#                 '-stim[{}].pulse.strength'.format(i), s_strength, 
#                 '-stim[{}].crct.type'.format(i),      s_type ]

        stims += elec + ptcl
    
    num_stims = len(elecs)
    if rmode == 're':
        stims += ['-stim[{}].crct.type'.format(num_stims), 8 ]
        num_stims = num_stims + 1

    stims = ['-num_stim', num_stims] + stims

    return stims


def active_electrodes(elec_lst, elec):

    act_elecs = {}
    if elec_lst and elec:
        for e in elec:
            if e in elec_lst:
                act_elecs[e] = elec_lst.get(e)
                print(e)
    else:
        print('No electrode definitions provided.')

    return act_elecs


def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    out_DIR = '{}_j2c'.format(today.isoformat())
    return out_DIR


@tools.carpexample(parser, jobID)
def run(args, job):
    cnf, exps, fncs, elecs, prtcls, slv, plan = j2c.load_experiment(None, None, args.plan,
                                                                    None, None)
    # read tissue/organ configuration
    #cnf, exps, elecs, act_elecs, prtcls, slv = load_experiment(args)
    cnf, exps, fncs, elecs, prtcls, slv, plan = j2c.load_experiment(args.config, args.experiment, args.plan, args.electrodes, args. protocols)
    
    # in case we have not any electrodes selected we assume all provide definitions are active
    act_elecs = active_electrodes(elecs, args.electrode)
    if not act_elecs:
        act_elecs = elecs

    # pick prepacing protocol from list
    pp_ptcl = prtcls.get(args.protocol)

    # use basename of mesh as ID
    meshname = get_mesh(args.mesh, args.cohort, args.case)
    
    # print command line
    cmd = tools.carp_cmd()

    # create split file
    split_file, split_active = j2c.generate_split_file(job, cnf, meshname, force_recreation=False)

    # setup mesh
    cmd += setup_mesh(meshname, split_file)

    # set up tissue properites
    cmd += j2c.setup_tissue(cnf, fncs, args.rmode)

    # set up solver settings
    cmd += j2c.add_slv_settings(slv)

    # before running, check all specified initial states are there
    if not j2c.check_initial_state_vectors(fncs, True):
        # some initial state vectors are missing
        print('\nMissing initial state vectors, using default initial states!\n\n')

    # add lat detector
    lats_idx = 0
    cmd += ['-num_LATs', lats_idx + 1]
    cmd += j2c.ek_lat_detect(lats_idx, threshold=-20, verbose=True)

    # add sentinel detector
    cmd += j2c.sentinel(lats_idx, 0, 20)

    # add electrodes
    stims = stimuli(act_elecs, args.rmode)
    cmd += stims

    # show command line
    # never execute
    args.dry = True
    job.carp(cmd)
    

if __name__ == '__main__':
    run()
