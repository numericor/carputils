#!/usr/bin/env python
"""
Little tool to extract surface data from a given mesh and save them as VTK files.
"""

import os
import argparse
import subprocess
from carputils import settings

NDATAID = 0
EDATAID = 1
DEFAULT = ' - default: %(default)s'

def surf2tri(frm, to):
    with open(frm, 'r') as fin:
        with open(to, 'w') as fout:
            # Write header
            fout.write(fin.readline())
            # Write triangles
            for line in fin:
                fout.write(' '.join(line.split()[1:]) + '\n')


def tri2surf(frm, to):
    with open(frm, 'r') as fin:
        with open(to, 'w') as fout:
            # Write header
            fout.write(fin.readline())
            # Write triangles
            for line in fin:
                fout.write('Tr ' + ' '.join(line.split()[:3]) + '\n')


def readsurf(frm):
    elems = []
    with open(frm, 'r') as fin:
        # Read header line
        line = fin.readline()
        line = fin.readline()
        while len(line) > 0:
            elems.append(tuple(map(int, line.strip().split()[1:])))
            line = fin.readline()
    return elems


def readneubc(frm):
    elems = []
    with open(frm, 'r') as fin:
        # Read header line
        line = fin.readline()
        line = fin.readline()
        while len(line) > 0:
            elems.append(tuple(map(int, line.strip().split())))
            line = fin.readline()
    return elems


class ListCreator(object):

    def __init__(self, *args):
        self._defaults = args

    def __call__(self, *args, **kwargs):
        return list(args + self._defaults)


class ListModifierAction(argparse.Action):

    def __init__(self, idx, *args, **kwargs):
        super(ListModifierAction, self).__init__(*args, **kwargs)
        self._idx = int(idx)

    def __call__(self, parser, args, values, option_string=None):
        getattr(args, self.dest)[-1][self._idx] = values


class ListModifier(object):

    def __init__(self, idx):
        self._idx = int(idx)

    def __call__(self, *args, **kwargs):
        return ListModifierAction(self._idx, *args, **kwargs)


def run():
    settings.cli = argparse.Namespace(build=settings.config.CARP_EXE_DIR.keys()[0])

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('-m', '--mesh', action='store', default=None, type=str, help='mesh base name'+DEFAULT, required=True)
    parser.add_argument('-s', '--surface', action='store', default=None, type=str, help='mesh surface name'+DEFAULT, required=True)
    parser.add_argument('-u', '--dynpt', action='store', default=None, type=str, help='displacement file'+DEFAULT)
    parser.add_argument('-n', '--ndata', type=ListCreator(NDATAID, 1.0), dest='data', action='append', default=[], help='nodal IGB data'+DEFAULT)
    parser.add_argument('-e', '--edata', type=ListCreator(EDATAID, 1.0), dest='data', action='append', default=[], help='element IGB data'+DEFAULT)
    parser.add_argument('-f', '--factor', type=float, action=ListModifier(2), default=1.0, help='scale IGB data by a factor'+DEFAULT, dest='data')#argparse.SUPPRESS)
    parser.add_argument('-d', '--dir', action='store', default='vtk', type=str, help='ouput directory'+DEFAULT)
    parser.add_argument('--vtktstart', action='store', default=0, type=int, help='start time'+DEFAULT)
    parser.add_argument('--vtktstop', action='store', default=-1, type=int, help='stop time'+DEFAULT)
    parser.add_argument('--vtkscale', action='store', default=0.001, type=float, help='sclae mesh'+DEFAULT)
    args = parser.parse_args()

    # check if the surface file exists
    if not os.path.isfile(args.mesh + '.{}.surf'.format(args.surface)):
        print 'Error: file "{}" does not exist!'.format(args.mesh + '.{}.surf'.format(args.surface))
        print 'Run "meshtool surface extract"'
        exit(1)

    datacount = [0, 0]
    skipdata = [False, False]
    # count given nodal data files and element data file
    for data in args.data:
        datacount[data[1]] += 1
    print '{} nodal data files'.format(datacount[NDATAID])
    print '{} element data files'.format(datacount[EDATAID])

    # if element data is given, check if the *.neubc file exists for the given surface file
    # if not, the element data will be skipped
    if datacount[EDATAID] > 0 and not os.path.isfile(args.mesh + '.{}.neubc'.format(args.surface)):
        print 'Error: file "{}" does not exist!'.format(args.mesh + '.{}.surf'.format(args.surface))
        print 'Skipping element data'
        skipdata[EDATAID] = True

    # if the element data is processed, write the surface-element to element map
    if datacount[EDATAID] > 0 and not skipdata[EDATAID]:
        print 'Create element index set!'
        elems = readneubc(args.mesh + '.{}.neubc'.format(args.surface))
        with open('elemIndexSet.idx', 'w') as fout:
            for i, elm in enumerate(elems):
                fout.write('{}\n'.format(elm[5]))

    # convert surfaces to triangles
    print 'Converting surface to tris'
    surf2tri(args.mesh + '.{}.surf'.format(args.surface), '{}.tris'.format(args.surface))

    # extract the surface mesh
    print 'Extracting surface mesh'
    cmd = [settings.execs.SurfaceMesh,
           '-m', args.mesh,
           '-s', args.surface,
           '-o', '{}_extract'.format(args.surface)]
    subprocess.check_call(map(str, cmd))

    # convert the surface mesh triangles to elements
    print 'Converting surface mesh tris to elem'
    tri2surf('{}_extract.tris'.format(args.surface), '{}_extract.elem'.format(args.surface))

    # extract displacement data
    if args.dynpt is not None:
        ofname = '{}_extract_{}'.format(args.surface, os.path.basename(args.dynpt))
        if not ofname.endswith('.igb'):
            ofname += '.igb'
        print 'Extracting displacements from "{}" to "{}" '.format(args.dynpt, ofname)
        cmd = [settings.execs.igbextract,
               '-i', 'indexSet.idx',
               '-o', 'IGB',
               '-O', ofname,
               args.dynpt]
        subprocess.check_call(map(str, cmd))

    # extract nodal and element data
    for data in args.data:
        ofname = '{}_extract_{}'.format(args.surface, os.path.basename(data[0]))
        cmd = [settings.execs.igbextract,
               '-s', data[2],
               '-o', 'IGB',
               '-O', ofname,
               data[0]]

        if data[1] == NDATAID and not skipdata[NDATAID]:
            print 'Extracting nodal IGB data from "{}" to "{}" scaled by {}'.format(data[0], ofname, data[2])
            cmd += ['-i', 'indexSet.idx']
            subprocess.check_call(map(str, cmd))

        if data[1] == EDATAID and not skipdata[EDATAID]:
            print 'Extracting nodal IGB data from "{}" to "{}" scaled by {}'.format(data[0], ofname, data[2])
            cmd += ['-i', 'elemIndexSet.idx']
            subprocess.check_call(map(str, cmd))

    # convert to VTK
    print 'Converting surface, displacements, nodal and element data to VTK'
    if not os.path.exists(args.dir):
        os.mkdir(args.dir)
    cmd = [settings.execs.GlVTKConvert,
           '-m', '{}_extract'.format(args.surface),
           '-s', args.vtkscale,
           '-S', args.vtktstart,
           '-E', args.vtktstop,
           '-F', 'bin',
           '-a', '-o', '{}/{}'.format(args.dir, args.surface)]
    if args.dynpt is not None:
        ofname = '{}_extract_{}'.format(args.surface, os.path.basename(args.dynpt))
        if not ofname.endswith('.igb'):
            ofname += '.igb'
        cmd += ['-u', ofname]
    for data in args.data:
        ofname = '{}_extract_{}'.format(args.surface, os.path.basename(data[0]))
        if data[1] == NDATAID and not skipdata[NDATAID]:
            cmd += ['-n', ofname]
        if data[1] == EDATAID and not skipdata[EDATAID]:
            cmd += ['-e', ofname]
    subprocess.check_call(map(str, cmd))

    # done
    print 'Done!'


if __name__ == '__main__':
    run()
