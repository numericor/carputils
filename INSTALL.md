## Installing carputils separately

### Docker

A quick way to get started is pulling the [openCARP docker image](https://opencarp.org/download/installation#installation-of-opencarp-docker-containers).

### Linux / macOS
To locally run carputils, first install [Python3](https://www.python.org/downloads/), [pip](https://pip.pypa.io/en/stable/installing/), and [openCARP](https://opencarp.org/download/installation#building-opencarp-from-source).

The carputils has been included if you [installed openCARP with additional tools](https://opencarp.org/download/installation#additional-tools).
Otherwise, please follow the steps below to manually install it.

#### Installation

Clone the carputils repository.
```
git clone https://git.opencarp.org/openCARP/carputils.git
cd carputils
```

Build the carputils and install it.
This is recommended if you do not expect to develop the carputils.
```
pip3 install . --user
```

In this case, if `$HOME/.local/bin` is not included in your `PATH` by default, add it to your .bashrc.
```
export PATH=$PATH:$HOME/.local/bin
```

On the other hand, if you are willing to develop the carputils, manually configuring it as follows is recommended.

#### Development Installation
Install the required python packages using `pip` - preferably into user-space:
```
pip3 install --user -r requirements.txt
```

Add `carputils/bin` to your `PATH`, and `carputils` to your `PYTHONPATH`.
For example, if you clone carputils to `$HOME/install`, add the following lines in your .bashrc:
```
export PATH=$PATH:$HOME/install/carputils/bin
export PYTHONPATH=$PYTHONPATH:$HOME/install/carputils
```

Create a `settings.yaml` file, which is a summary of PATHs and (default) parameters associated with your installation.
```
cusettings settings.yaml
```

Open the file and specify the proper directories for openCARP binary files. 
Notice using the *2 or 4 characters indentation* before the keyword `CPU`.
```
CARP_EXE_DIR:
    CPU: $HOME/install/openCARP/bin
```

Set directories for other openCARP tools if you use them. For example,
```
MESHALYZER_DIR:     $HOME/install/meshalyzer
```

Move `settings.yaml` into the root directory of carputils.