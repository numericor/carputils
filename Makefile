default: all

all: cext2 cext3

cext2: build_cext.py
	@echo "* building carputils C extensions for Python2 ..."
	@(python2 build_cext.py build_ext --inplace)

cext3: build_cext.py
	@echo "* building carputils C extensions for Python3 ..."
	@(python3 build_cext.py build_ext --inplace)

clean:
	@echo "* cleaning up .."
	@(rm -rf build)
	@(rm -f carputils/cext/*.so)

doxygen:
	doxygen doxygen_doc/carputils.doxygen

status st:
	git status

update up:
	git pull

