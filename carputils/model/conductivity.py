#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

from .general import AbstractModelComponent, RegionMixin

class ConductivityRegion(RegionMixin, AbstractModelComponent):
    """
    Defines an electrical conductivity region.

    Args
        IDs : list
            The material tags in this region
        name : str, optional
            A short descriptive name for this region
        g_il : float, optional
            Intracellular longitudinal conductivity
        g_it : float, optional
            Intracellular transverse conductivity
        g_in : float, optional
            Intracellular sheet-normal conductivity
        g_el : float, optional
            Extracellular longitudinal conductivity
        g_et : float, optional
            Extracellular transverse conductivity
        g_en : float, optional
            Extracellular sheet-normal conductivity
    """

    PRM_ARRAY  = 'gregion'
    PRM_LENGTH = 'num_gregions'

    def __init__(self, IDs=[], name=None, g_il=0.174, g_it=0.019, g_in=0.019,
                                          g_el=0.625, g_et=0.236, g_en=0.236):

        super(ConductivityRegion, self).__init__(IDs=IDs, name=name)

        self._g_il = g_il
        self._g_it = g_it
        self._g_in = g_in
        self._g_el = g_el
        self._g_et = g_et
        self._g_en = g_en

    @classmethod
    def isotropic(cls, IDs=[], name=None, cond=0.1):
        """
        Define an isotropic conductivity region.

        Args:
            IDs : list
                The material tags in this region
            name : str, optional
                A short descriptive name for this region
            cond : float, optional
                The conductivity to use
        """
        obj = cls(IDs, name, cond, cond, cond, cond, cond, cond)
        return obj

    @classmethod
    def passive(cls, IDs=[], name=None):
        """
        Define an electrically passive (very low conducivity) region.

        Args:
            IDs : list
                The material tags in this region
            name : str, optional
                A short descriptive name for this region
        """
        return cls.isotropic(IDs, name, cond=0.0002)

    def opts(self):

        for opt in super(ConductivityRegion, self).opts():
            yield opt

        yield 'g_il', self._g_il
        yield 'g_it', self._g_it
        yield 'g_in', self._g_in
        yield 'g_el', self._g_el
        yield 'g_et', self._g_et
        yield 'g_en', self._g_en
