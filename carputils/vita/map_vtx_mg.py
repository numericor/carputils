#!/usr/bin/env python3

import os
import sys
import json
import numpy

import ruamel.yaml as rmyaml
import scipy.spatial as spspatial

import carputils.cext.mshread as mshread
import carputils.cext.mshwrite as mshwrite


def main():

  # read transformation and convert to 4x4 matrix
  with open('./case/s62.trfm', 'r') as fp:
    trfm_str = rmyaml.safe_load(fp)
    trfm = numpy.fromstring(trfm_str['affine transform'], sep=' ', dtype=float)
    trfm = trfm.reshape((4, 4))

  # read leadfield vertices
  # (with respect to original volumetric mesh)
  with open('./volmesh/s62/lf_vtx.json', 'r') as fp:
    volm_vtx = json.load(fp)

  # read point data of original volumetric mesh
  orig_volm_pnts = mshread.read_points_bin('./volmesh/s62/S62.1200um.bpts')

  # extract XYZ coordinates of vertices and transform them
  lf_pnts = dict()
  for key, value in volm_vtx.items():
    orig_pnt = orig_volm_pnts[value]
    pnt_ext = numpy.append(orig_pnt, 1.0)
    trfm_pnt = trfm.dot(pnt_ext)[:3]
    lf_pnts[key] = {'orig': orig_pnt, 'trfm': trfm_pnt}


  # =================================================================
  # MAP ONTO ORIGINAL (UNTRANSFORMED) TORSO SURFACE MESH
  # =================================================================
  
  # read torso surface points
  trso_surf_pnts = mshread.read_points_bin('./torsos/s62/s62.torso.bpts')

  # create kD-tree
  kdtree = spspatial.KDTree(trso_surf_pnts)

  # map indices (get index of closest vertices)
  trfm_vtx = dict()
  for key, value in lf_pnts.items():
    idx = kdtree.query(value['orig'])
    trfm_vtx[key] = idx[1]

  # write to *.vtx file
  vtx_data = numpy.array(list(trfm_vtx.values()), dtype=numpy.int32)
  mshwrite.write_vertex_data('./torsos/s62/torso.lf.vtx', vtx_data, 'intra')

  # =================================================================
  # MAP ONTO TRANSFORMED TORSO VOLUMETRIC MESH
  # =================================================================

  # read torso volumetric points
  trfm_volm_pnts = mshread.read_points_bin('./case/H2C_coarse.torso.bpts')

  # create kD-tree
  kdtree = spspatial.KDTree(trfm_volm_pnts)
  
  # map indices (get index of closest vertices)
  trfm_vtx = dict()
  for key, value in lf_pnts.items():
    idx = kdtree.query(value['trfm'])
    trfm_vtx[key] = idx[1]
  
  # write to *.vtx file
  vtx_data = numpy.array(list(trfm_vtx.values()), dtype=numpy.int32)
  mshwrite.write_vertex_data('./case/H2C_coarse.torso.lf.vtx', vtx_data, 'intra')


if __name__ == '__main__':
  main()
