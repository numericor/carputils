import argparse
import subprocess
import os.path, time
import sys
import numpy as np
from carputils.vita import martinsVITAtools as VITA


def vita(rvi_mapper, ek_param_loc, rttTHR, step, mesh_dir, modelname, num_stim):

    HDR='-'*80
    VTcorrTHR = 0.8

    # Defines model specific things
    meshname = os.path.join(mesh_dir, modelname)
    pacingSites_Dir = os.path.join(mesh_dir, 'pacingSites')
    
    # makes output directory
    output_dir = os.path.join(mesh_dir, 'vita')
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    print(HDR)
    print('Processing model {}'.format(meshname))
    print(HDR)
                
    #pts = meshIO.read_pts(meshname)
    
    # Defines arrays to store all output for all pacing sites for later analysis
    VTsSURF = []
    VTsRTT  = []
    VTsRVI  = []
                
    # File with all surfaces related to an exit site
    fVTs = open("{}/STIM_VTs.txt".format(output_dir),"w")

    # File with all unique VT surfaces
    fuVTs = open("{}/STIM_uVTs.txt".format(output_dir),"w")
                
    ####################################################
    # Applies RVI_MAPPER from all 5 pacing sites
    ####################################################
    # defines number of stimulus sites
    #num_stim = args.num_stim
    # iterates over all 5 pacing sites (0=apex, 1=base0, 2=base1, 3=base2, 4=base3)
    for stim in range(1,num_stim+1):
        print(HDR)
        print('Applying rvi_mapper from stimulus site = {}'.format(stim))
        print(HDR)

        # Extracts pacing site coordinates
        stim_filename = os.path.join(pacingSites_Dir,'STIM_{}_coords.dat'.format(stim))
        stimCoords = np.loadtxt(stim_filename)
        print('Applying pacing from location {},{},{}'.format(stimCoords[0],stimCoords[1],stimCoords[2]))
        # defines specific (pacing site dependent) place to save VITA output
        stim_output_dir = os.path.join(output_dir,'STIM_{}'.format(stim))
        
        # runs VITA
        cmd = [rvi_mapper,
               '-msh={}'.format(meshname),
               '-coord={},{},{}'.format(stimCoords[0], stimCoords[1], stimCoords[2]),
               '-min=0.01',
               '-max=0.1',
               '-rtt_thr={}'.format(rttTHR),
               '-step=1000',
               '-odat={}'.format(stim_output_dir),
               '-out_mask=7',
               '-init={}'.format(ek_param_loc) ]

        HDR='='*80
        print(HDR + '\nCALLING: ' + ' '.join(cmd) + '\n' + HDR)
        subprocess.check_call(cmd)
        print('\n' * 2)
        # Fernando's style, switching to MG style, carputils job might be better here?
        #cmd = '{} -msh={} -coord={},{},{} -min=0.01 -max=0.1 -rtt_thr={} -step=1000 -odat={} -out_mask=7 -init={}'.format(rvi_mapper_loc, meshname, stimCoords[0], stimCoords[1], stimCoords[2], rttTHR, stim_output_dir, ek_param_loc)
        #os.system(cmd)
                    
        # Extracts information about all split surfaces that were above the rtt threshold
        nSURFs, SURFs, RTTs, RVIs, ENTRs, EXITs, ABLs = VITA.read_allthres_file(stim_output_dir)
                    
        # Collates all of the info about split surfaces above the threshold for each pacing site
        for k in range(nSURFs):
            # only is concerned with exits (simplifies things)
            if(EXITs[k] == 'yes') or (ENTRs[k] == 'yes'):
                VTsSURF.append(SURFs[k])
                VTsRTT.append(RTTs[k])
                VTsRVI.append(RVIs[k])
                            
                # File with all surfaces related to the exit sites of all detected VTs
                fVTs.write("{} {} {}\n".format(SURFs[k],RTTs[k],RVIs[k]))
                            
            #else:
                # Delete split surfaces which are not of interest (entries)
                #os.system('rm {}.*'.format(SURFs[k]))
    # closes files
    fVTs.close()
                
    ####################################################
    # Finds unique VTs out of all induced VTs from all pacing sites
    ####################################################
    print(HDR)
    print('Finding unique VTs...')
    print(HDR)
    # Check if VTs were inducible
    nVTs = 0
    num_VTs = 0
    max_rtt = 0
    mean_rtt = 0
    if(len(VTsSURF) > 0):
                    
        # Convert list to numpy array
        VTsRTT = np.asarray(VTsRTT,dtype=float)
        VTsRVI = np.asarray(VTsRVI,dtype=float)

        # Sort VTs based on VTsRTT or VTsRVI
        VTsSURF, VTsRTT, VTsRVI = VITA.sort_VT_surfs(VTsSURF,VTsRTT,VTsRVI,flag="RTT")
                    
        # Get correlation matrix
        VTcorr = VITA.get_correlation_matrix(VTsSURF,VTsRTT,VTsRVI)

        # Find all unique VTs
        uVTs, freqVTs = VITA.get_unique_VTs(VTcorr,VTcorrTHR)
                    
        # Unique VTs
        nVTs = np.size(uVTs)
        uVTsSURF = []
        for n in range(0,nVTs):
            uVTsSURF.append(VTsSURF[uVTs[n]])
        uVTsRTT = VTsRTT[uVTs]
        uVTsRVI = VTsRVI[uVTs]

        # prints out the information regarding unique VT stats for this model
        for n in range(0,nVTs):
            # File with all surfaces related to the exit sites of all detected VTs
            fuVTs.write("{} {} {}\n".format(uVTsSURF[n],uVTsRTT[n],uVTsRVI[n],freqVTs[n]))
                    
        # closes files
        fuVTs.close()
            
        ####################################################
        # Computes mean data for this model
        ####################################################
        print(HDR)
        print('Computing mean VT data...')
        print(HDR)

        # computes a few metrics on the induced VTs for analysis
        num_VTs = len(VTsSURF)
        mean_rtt = np.mean(uVTsRTT)
        max_rtt = np.max(uVTsRTT)
        # weighted mean
        wm_rtt = 0
        for n in range(0,nVTs):
            wm_rtt = wm_rtt + uVTsRTT[n]*freqVTs[n]
        wm_rtt = wm_rtt/np.sum(freqVTs)
        
        # most freq
        mf_rtt = uVTsRTT[np.argmax(freqVTs)]

        # prints out the total number of unique VTs
        fnumUVTs = open(output_dir + 'num_uVTs.dat',"w")
        fnumUVTs.write('{}\n'.format(nVTs))
        fnumUVTs.close()
        # prints out the total number of VTs
        fnumVTs = open(output_dir + 'num_VTs.dat',"w")
        fnumVTs.write('{}\n'.format(num_VTs))
        fnumVTs.close()
        # prints out the mean rtt
        fmeanrtt = open(output_dir + 'mean_rtt.dat', "w")
        fmeanrtt.write('{}\n'.format(mean_rtt))
        fmeanrtt.close()
        # prints out max rtt
        fmaxrtt = open(output_dir + 'max_rtt.dat', "w")
        fmaxrtt.write('{}\n'.format(max_rtt))
        fmaxrtt.close()
        
        # prints out wm rtt
        fwmrtt = open(output_dir + 'wm_rtt.dat', "w")
        fwmrtt.write('{}\n'.format(wm_rtt))
        fwmrtt.close()
        
        # print out most freq uVT
        fmfrtt = open(output_dir + 'mf_rtt.dat', "w")
        fmfrtt.write('{}\n'.format(mf_rtt))
        fmfrtt.close()
                
        # Clears out splits with rtt less than the threshold
        #extensions = ['splt','ek.dat']
        #for filename in os.listdir(output_dir):
        #    filename2 = output_dir + filename
        #    if not any(filename2.startswith(starts) for starts in VTsSURF):
        #        if any(filename2.endswith(exts) for exts in extensions):
        #            cmd = 'rm {}'.format(filename2)
        #            os.system(cmd)
                    
        
        ####################################################
        # Performs RE run to visualise VT circuit
        ####################################################
        # iterates over all unique VTs only
        #uVTdata = pd.read_csv('{}/STIM_uVTs.txt'.format(output_dir),sep = ' ', header=None)
        #for i in range(0,num_uVTs):
            
        #   uVTactFile = uVTdata.iloc[i,0]
            
        #    cmd = '{} '
