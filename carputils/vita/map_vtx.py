#!/usr/bin/env python3

import os

import sys
import json
import numpy
import argparse
from os.path import join, dirname, basename, splitext

import ruamel.yaml as rmyaml
import scipy.spatial as spspatial

import carputils.cext.mshread as mshread
import carputils.cext.mshwrite as mshwrite


def parser():
  parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)

  parser.add_argument('--src-torso',
                      type=str,
                      default=None,
                      help='source (volumetric) torso mesh upon which lead field configuration is built upon')

  parser.add_argument('--src-lf-conf', type=str, default=None,
                      help='source lead field configuration defined on source (volumetric) mesh in .json format.')

  parser.add_argument('--trg-torso', type=str, default=None,
                      help='target (surface) torso mesh matching the source (volumetric) mesh.-')

  parser.add_argument('--trfm', type=str, default=None,
                      help='affine transformation file')

  return parser


# read leadfield json, return indices and vertex locations
def get_leadfield_vtcs(lf, ptsf):

  idx = None
  with open(lf,'r') as fp:
    lf_setup = json.load(fp)
    # go over leadfield definition and extract name - vertex pairs
    lf_vtcs = {}
    lf = lf_setup.get('leadfields')
    for l in lf:
      lf_vtcs[l] = lf.get(l).get('srcvtx')

    # read in reference vertex as well
    ref = lf_setup.get('refvtx')

    lf_vtcs['REF'] = ref

  # read mesh
  pts = mshread.read_points_bin(ptsf)

  # extract
  lf_xyz = []
  lf_lbl = []
  lf_ids = []
  for i, val in lf_vtcs.items():
    lf_lbl.append(i)
    lf_ids.append(val)
    lf_xyz.append(pts[val])

  return lf_setup, lf_lbl, lf_ids, lf_xyz

# find location pts among point cloud msh_pts
def find_matching_vtcs(pts, mesh_pts):

  # create kD-tree
  kdtree = spspatial.KDTree(mesh_pts)

  # map indices (get index of closest vertices)
  inds = []
  for pt in pts:
    idx = kdtree.query(pt)
    inds.append(idx[1])

  return inds

# update leadfield configuration with new indices
def write_lf_conf(lf_conff, lf_cnf, lf_lbls, lf_inds):

    lf = lf_cnf.get('leadfields')
    for i,l in enumerate(lf_lbls):
      if lf.get(l):
          lf[l]['srcvtx'] = int(lf_inds[i])
          lf[l]['file'] = ""
      else:
        if 'REF' in l:
          lf_cnf['refvtx'] = int(lf_inds[i])

    with open(lf_conff, 'wt') as fp:
      json.dump(lf_cnf, fp, indent=4)

    return lf_cnf


def main(opts):

 # parser.add_argument('--src-torso',
 #                     type=str,
 #                     default=None,
 #                     help='source (volumetric) torso mesh upon which lead field configuration is built upon')

 # parser.add_argument('--src-lf-conf', type=str, default=None,
 #                     help='source lead field configuration defined on source (volumetric) mesh in .json format.')

 # parser.add_argument('--trg-torso
  # inputs
  # trfmf = opts.trfm
  trfmf = './caseID/s62.trfm'
  lff = opts.src_lf_conf
  #lff   =  '../s62model/lf.12lead.json'
  vptsf = opts.src_torso
  #vptsf = '../s62model/S62.1200um.bpts'
  sptsf = opts.trg_torso
  #sptsf='./torsos/s62/s62.torso.bpts'

  # opts.vol_trfm
  vptsf_trfm = './caseID/H2C_coarse.torso.bpts'

  # Read out vertex information from lead field file
  # Determine vertex node coordinates from volumetric mesh
  # Save vertex indices to vtx file
  print('\n\bDetermining volume mesh leadfield electrode locations from:\n{}\n{}'.format(lff,vptsf))
  print('-'*80)
  lf_conf, lf_lbls, lf_vinds, lf_vpts = get_leadfield_vtcs(lff, vptsf)
  print('%4s %10s %10s %10s %10s'%('ID', 'index', 'x', 'y', 'z'))
  for i,l in enumerate(lf_lbls):
    print('%4s %10d %10.2f %10.2f %10.2f'%(l, lf_vinds[i], lf_vpts[i][0], lf_vpts[i][1], lf_vpts[i][2]))
  print('\n\n')

  # write volumetric leadfield indices to vertex file
  vvtx_data = numpy.array(list(lf_vinds), dtype=numpy.int32)
  bs, ex = splitext(basename(vptsf))
  lf_vtxf = join(dirname(vptsf), bs + '.lf.vtx')
  mshwrite.write_vertex_data( lf_vtxf, vvtx_data, 'extra')

  # Find location of leadfield pts on surface torso
  # Read in points spanning torso surface
  # Write vertex file of leadfield nodes
  # Write new leadfield json
  print('Determining surface mesh leadfield electrode locations from:\n{}'.format(sptsf))
  print('Reading torso surface')
  print('-'*80)
  spts = mshread.read_points_bin(sptsf)
  lf_sinds = find_matching_vtcs(lf_vpts, spts)

  print('%4s %10s %10s %10s %10s'%('ID', 'index', 'x', 'y', 'z'))
  for i,l in enumerate(lf_lbls):
    print('%4s %10d %10.2f %10.2f %10.2f'%(l, lf_sinds[i], lf_vpts[i][0], lf_vpts[i][1], lf_vpts[i][2]))
  print('\n\n')

  # write surface indices to vertex file and modify leadfield configuration file
  svtx_data = numpy.array(list(lf_sinds), dtype=numpy.int32)
  bs, ex = splitext(basename(sptsf))
  lf_vtxf = join( dirname(sptsf), bs + '.lf.vtx')
  print('Writing surface leadfield vertices to {}'.format(lf_vtxf))
  mshwrite.write_vertex_data( lf_vtxf, svtx_data, 'intra')

  lf_conff = join(dirname(sptsf), bs + '.lf.json')
  print('Writing surface leadfield configuration to {}'.format(lf_conff))
  write_lf_conf(lf_conff, lf_conf, lf_lbls, lf_sinds)
  print('Done with template surface-based leadfield generation.')

  # read transformation and convert to 4x4 matrix
  print('\n\nDetermine leadfield vertices on transformed volumetric torso mesh')
  print('-'*80)
  print('Reading affine transformation from {}.'.format(trfmf))
  with open(trfmf, 'r') as fp:
    trfm_str = rmyaml.safe_load(fp)
    trfm = numpy.fromstring(trfm_str['affine transform'], sep=' ', dtype=float)
    trfm = trfm.reshape((4, 4))

  # =================================================================
  # MAP ONTO TRANSFORMED TORSO VOLUMETRIC MESH
  # =================================================================

  print('Determining volumetric mesh leadfield electrode locations from  {}'.format(sptsf))
  print('Reading torso surface')
  spts = mshread.read_points_bin(sptsf)

  # transform surface points
  print('Transforming vertex locations on torso surface')
  lf_spts = []
  lf_spts_trfm = []
  for i,l in enumerate(lf_sinds):
      lf_spts.append(spts[l])
      pnt_ext = numpy.append(lf_spts[i], 1.0)
      trfm_pnt = trfm.dot(pnt_ext)[:3]
      lf_spts_trfm.append(trfm_pnt)

  # read in points of transformed volumetric mesh
  print('Reading transformed volumetric mesh {}'.format(vptsf_trfm))
  vpts_trfm = mshread.read_points_bin(vptsf_trfm)
  print('Determine vertex indices of leadfield on transformed torso')
  lf_vinds_trfm = find_matching_vtcs(lf_spts_trfm, vpts_trfm)

  print('\n%4s %10s %10s %10s %10s'%('ID', 'index', 'x', 'y', 'z'))
  for i,l in enumerate(lf_lbls):
    print('%4s %10d %10.2f %10.2f %10.2f'%(l, lf_vinds_trfm[i],
                                           lf_spts_trfm[i][0], lf_spts_trfm[i][1], lf_spts_trfm[i][2]))
  print('\n\n')

  # write volumetric leadfield indices of transformed mesh to vertex file
  vvtx_data_trfm = numpy.array(list(lf_vinds_trfm), dtype=numpy.int32)
  bs, ex = splitext(basename(vptsf_trfm))
  lf_vtxf_trfm = join(dirname(vptsf_trfm),bs+'.lf.vtx')
  print('Writing vertices of transformed leadfield to {}'.format(lf_vtxf_trfm))
  mshwrite.write_vertex_data( lf_vtxf_trfm, vvtx_data_trfm, 'extra')

  # write transformed leadfield configuration file
  lf_conff = join(dirname(vptsf_trfm), bs+'trfm.lf.json')
  print('Writing transformed leadfield to {}.\n'.format(lf_conff))
  write_lf_conf(lf_conff, lf_conf, lf_lbls, lf_vinds_trfm)

  print('-'*80)
  print('Done with generating leadfield configuration on transformed volumetric mesh.\n\n')

if __name__ == '__main__':

  # build parser
  opts = parser().parse_args()

  main(opts)
