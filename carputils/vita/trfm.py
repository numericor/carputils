#!/usr/bin/env python3

import numpy as np
import argparse
from os.path import join, dirname, basename, splitext

import ruamel.yaml as rmyaml

import carputils.cext.mshread as mshread
import carputils.cext.mshwrite as mshwrite


def parser():
  parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)

  parser.add_argument('--src-pts',
                      type=str,
                      default=None,
                      help='Source points data to be transformed.')

  parser.add_argument('--trfm-pts',
                      type=str,
                      default=None,
                      help='Output transformed points data to file (default is src-pts name + trfm).')

  parser.add_argument('--trfm', type=str, default=None,
                      help='affine transformation file')

  return parser



def read_trfm(trfmf):

    # read transformation and convert to 4x4 matrix
    print('Reading affine transformation from {}.'.format(trfmf))

    trfm = None
    with open(trfmf, 'r') as fp:
        trfm_str = rmyaml.safe_load(fp)
        trfm = np.fromstring(trfm_str['affine transform'], sep=' ', dtype=float)
        trfm = trfm.reshape((4, 4))

    return trfm


def apply_trfm(pts, trfm):

    pts_trfm = np.zeros(np.shape(pts))
    for i,pt in enumerate(pts):
        pt_ext = np.append(pt, 1.0)
        pt_trfm = trfm.dot(pt_ext)[:3]
        pts_trfm[i][:] = pt_trfm

    return pts_trfm

def main(opts):

    src_ptsf = opts.src_pts
    trfmf = opts.trfm

    # read source points
    print('\n\nRead source points file {}.'.format(src_ptsf))
    src_pts = mshread.read_points_bin(src_ptsf)

    # read transform
    trfm = read_trfm(trfmf)

    # apply transform
    print('Apply transform {} to {}.'.format(trfmf,src_ptsf))
    src_pts_trfm = apply_trfm(src_pts, trfm)

    # write transformed points to file
    trfm_ptsf = opts.trfm_pts
    if not opts.trfm_pts:
        bs, ex = splitext(basename(src_ptsf))
        trfm_ptsf = os.path.join(os.path.dirname(src_ptsf),bs+'.trfm'+ex)

    print('Writing transformed data points to {}'.format(trfm_ptsf))
    #src_pts_trfm_flat = src_pts_trfm.flatten()
    mshwrite.write_points_bin(trfm_ptsf, src_pts_trfm)


if __name__ == '__main__':

  # build parser
  opts = parser().parse_args()

  main(opts)
