#!/usr/bin/env python3

import os
import shutil
import sys
import json
import numpy
import argparse
from os.path import join, dirname, basename, splitext
import matplotlib.pyplot as plt

import ruamel.yaml as rmyaml
import scipy.spatial as spspatial

from carputils import tools
from carputils.forcepss import j2carp as j2c
from carputils.vita.trfm import read_trfm, apply_trfm
import carputils.cext.mshread as mshread
import carputils.cext.mshwrite as mshwrite


def parser():
  parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)

  parser.add_argument('--src-torso',
                      type=str,
                      default=None,
                      help='source (volumetric) torso mesh upon which lead field configuration is built upon')

  parser.add_argument('--src-lf-conf', type=str, default=None,
                      help='source lead field configuration defined on source (volumetric) mesh in .json format.')

  parser.add_argument('--trg-torso', type=str, default=None,
                      help='target (surface) torso mesh matching the source (volumetric) mesh.-')

  parser.add_argument('--trfm', type=str, default=None,
                      help='affine transformation file')

  return parser


# read leadfield json, return indices and vertex locations
def get_leadfield_vtcs(lf):

  idx = None
  with open(lf,'r') as fp:

    lf_setup = json.load(fp)

    # go over leadfield definition and extract name - vertex pairs
    lf_vtcs = {}
    lf = lf_setup.get('leadfields')
    for l in lf:
      lf_vtcs[l] = lf.get(l).get('srcvtx')

    # read in reference vertex as well
    ref = lf_setup.get('refvtx')
    lf_vtcs['REF'] = ref

  return lf_setup,  lf_vtcs


# read leadfield json, return indices and vertex locations
def get_leadfield_pts(lf_vtcs, ptsf):

    # read mesh
    pts = mshread.read_points_bin(ptsf)

    # extract
    lf_pts = []
    lf_lbl = []
    lf_ids = []
    for i, val in lf_vtcs.items():
        lf_lbl.append(i)
        lf_ids.append(val)
        lf_pts.append(pts[val])

    return lf_lbl, lf_ids, lf_pts

# find location pts among point cloud msh_pts
def find_matching_vtcs(pts, mesh_pts):

  # create kD-tree
  kdtree = spspatial.KDTree(mesh_pts)

  # map indices (get index of closest vertices)
  inds = []
  for pt in pts:
    idx = kdtree.query(pt)
    inds.append(idx[1])

  return inds

# update leadfield configuration with new indices
def write_lf_conf(lf_conff, lf_cnf, lf_lbls, lf_inds):

    lf = lf_cnf.get('leadfields')
    for i,l in enumerate(lf_lbls):
      if lf.get(l):
          lf[l]['srcvtx'] = int(lf_inds[i])
          lf[l]['file'] = ""
      else:
        if 'REF' in l:
          lf_cnf['refvtx'] = int(lf_inds[i])

    with open(lf_conff, 'wt') as fp:
      json.dump(lf_cnf, fp, indent=4)

    return lf_cnf

def print_lf_table(lf_lbls, lf_inds, lf_pts):

    print('%4s %10s %10s %10s %10s' % ('ID', 'index', 'x', 'y', 'z'))
    for i, l in enumerate(lf_lbls):
        print('%4s %10d %10.2f %10.2f %10.2f' % (l, lf_inds[i], lf_pts[i][0], lf_pts[i][1], lf_pts[i][2]))
    print('\n')


def trfm_leadfield(src_lff, src_ptsf, trg_ptsf, trfm):

  # Read out vertex information from lead field file
  # Determine vertex node coordinates from source mesh
  print('\n\bDetermining source mesh leadfield electrode locations from:\n{}\n{}'.format(src_lff,src_ptsf))
  print('-'*80)

  lf_conf, lf_vtcs = get_leadfield_vtcs(src_lff)
  lf_lbls, lf_src_inds, lf_src_pts = get_leadfield_pts(lf_vtcs, src_ptsf)
  print_lf_table(lf_lbls, lf_src_inds, lf_src_pts)

  # write source leadfield  to vertex file
  lf_src_vtx_data = numpy.array(list(lf_src_inds), dtype=numpy.int32)
  ref_idx = lf_lbls.index('REF')
  lf_src_vtx_data = numpy.delete(lf_src_vtx_data, ref_idx, 0)
  bs, ex = splitext(basename(src_ptsf))
  lf_src_vtxf = join(dirname(src_ptsf), 'lf.' + bs + '.vtx')
  print('Extracting leadfield vertices from {}.'.format(src_lff))
  print('Write leadfield source vertex data to {}.\n\n'.format(lf_src_vtxf))
  mshwrite.write_vertex_data(lf_src_vtxf, lf_src_vtx_data, 'extra')

  # Find location of leadfield pts on target torso
  if trg_ptsf is not None:
      print('Determining target mesh leadfield electrode locations from:\n{}'.format(trg_ptsf))

      # Read in points spanning target torso mesh
      print('Reading torso surface')
      print('-'*80)
      trg_pts = mshread.read_points_bin(trg_ptsf)

      # target torso related by transform?
      lf_src_pts_trfm = lf_src_pts
      if trfm is not None:
          lf_src_pts_trfm = apply_trfm(lf_src_pts, trfm)

      lf_trg_inds = find_matching_vtcs(lf_src_pts_trfm, trg_pts)
      print_lf_table(lf_lbls, lf_trg_inds, lf_src_pts_trfm)

      # write surface indices to vertex file and modify leadfield configuration file
      lf_trg_vtx_data = numpy.array(list(lf_trg_inds), dtype=numpy.int32)
      ref_idx = lf_lbls.index('REF')
      lf_trg_vtx_data = numpy.delete(lf_trg_vtx_data, ref_idx, 0)
      bs, ex = splitext(basename(trg_ptsf))
      lf_trg_vtxf = join( dirname(trg_ptsf), 'lf.' + bs + '.vtx')
      print('Writing target leadfield vertices to {}'.format(lf_trg_vtxf))
      mshwrite.write_vertex_data(lf_trg_vtxf, lf_trg_vtx_data, 'extra')

      # Write new leadfield json
      lf_trg_conff = join(dirname(trg_ptsf), 'lf.' + bs + '.json')
      print('Writing target leadfield configuration to {}.\n\n'.format(lf_trg_conff))
      write_lf_conf(lf_trg_conff, lf_conf, lf_lbls, lf_trg_inds)

      print('-'*80)
      print('Done with generating leadfield configuration on transformed target mesh.\n\n')

      return lf_trg_conff


def compute_leadfields(caseID, lf_conff, lf_mesh, plan, job):

    print('\n\bComputing leadfield data from:\n{}\n{}'.format(lf_conff, lf_mesh))
    print('-' * 80)

    # check if lf_dir is there, otherwise create
    lf_dir = 'leadfield'
    lf_pth = os.path.join(caseID, lf_dir)
    if not os.path.isdir(lf_pth):
        os.makedirs(lf_pth)

    # read in lf_conf, extract vertex settings, call carp to compute leadfields
    src_ptsf = lf_mesh + '.bpts'
    #lf_conf, lf_lbls, lf_src_inds, lf_src_pts = get_leadfield_vtcs(lf_conff, src_ptsf)

    lf_conf, lf_vtcs = get_leadfield_vtcs(lf_conff)
    lf_lbls, lf_src_inds, lf_src_pts = get_leadfield_pts(lf_vtcs, src_ptsf)
    ref_lst_idx = lf_lbls.index('REF')
    ref_idx = lf_src_inds[ref_lst_idx]

    # write out leadfield source vertices to vertex file
    caseID = os.path.basename(lf_mesh)
    lf_src_vtxf = os.path.join(lf_pth, 'lf.' + caseID + '.vtx')
    # remove REF from vertex list
    lf_lbls.pop(ref_lst_idx)
    lf_src_inds.pop(ref_lst_idx)
    lf_src_pts.pop(ref_lst_idx)
    mshwrite.write_vertex_data(lf_src_vtxf, numpy.array(list(lf_src_inds), dtype=numpy.int32), 'extra')

    # build carp command
    carp_lf_cmd = tools.carp_cmd()
    carp_lf_cmd += j2c.plan2carp(job, plan, lf_mesh, 'rd')
    # add leadfield specific parameters (experiment 9),
    # requires bidomain=1 and num_stim=0
    sim_dir = os.path.join(lf_pth, 'lf_compute')
    carp_lf_cmd += ['-simID', sim_dir,
                    '-experiment', 9,
                    '-bidomain', 1,
                    '-num_stim', 0,
                    '-meshname', lf_mesh,
                    '-lead_field_mode', 0,
                    '-lead_field_meth', 1,
                    '-lead_field_ref', ref_idx,
                    '-lead_field_sources', os.path.splitext(lf_src_vtxf)[0],
                    '-dump_lead_fields', 0
                    ]

    job.carp(carp_lf_cmd)

    # copy back compute lead fields, remove temporary simulation directory
    print('\n\nCopying leadfield data to target directory.')
    print('-'*80)
    if os.path.dirname(sim_dir):
        # copy all LF vector files
        for src_idx in lf_src_inds:
            lf_dataf_ex = os.path.join(sim_dir, 'LF_Z_extra_Ref_{}_Field_{}.dat'.format(ref_idx, src_idx))
            print('Copying {} to {}.'.format(lf_dataf_ex, lf_pth))
            shutil.copy2(lf_dataf_ex, lf_pth)

            lf_dataf_in = os.path.join(sim_dir, 'LF_Z_intra_Ref_{}_Field_{}.dat'.format(ref_idx, src_idx))
            print('Copying {} to {}.'.format(lf_dataf_in, lf_pth))
            shutil.copy2(lf_dataf_in, lf_pth)
        print('\n')

        # done with copying remove leadfield simulation directory
        keep_lf_sim = False
        if not keep_lf_sim:
            shutil.rmtree(sim_dir)

    # update leadfield configuration with path to leadfield data files
    lfs = lf_conf.get('leadfields')

    for key, val in lfs.items():
        # leadfield vector filenames are relative to location of simulation plan
        val['file'] = os.path.join(lf_dir, 'LF_Z_extra_Ref_{}_Field_{}.dat'.format(ref_idx, val['srcvtx']))

    print('Updating leadfield configuration file {}.'.format(lf_conff))
    print('-'*80)
    print('\n')
    trg_lf_conff = os.path.join(lf_pth, 'lf.' + caseID + '.json')
    j2c.dump_json(trg_lf_conff, lf_conf)

    # add leadfield section to simulation plan
    sim_plan = j2c.load_json(plan)
    sim_plan['lfsetup'] = lf_conf
    j2c.dump_json(plan, sim_plan)

    #with open(trg_lf_conff, 'w') as fp:
    #    json.dump(lf_conf, fp, indent=4)


def lf_egms_to_ecg(lf_egmf, dt, lf_vtcs, lf_src_vtxf):

    # load raw leadfield EGM data
    lf_egms = numpy.loadtxt(lf_egmf)

    # read order of EGMs in leadfield EGM file
    src_vtx_list, extra_flg = mshread.read_vertex_data(lf_src_vtxf)

    # we need 9 egms
    shp = lf_egms.shape
    assert(shp[1] == 9)

    # re-create time axis
    t = numpy.arange(0, shp[0])*dt

    # parse egm data
    egms = {}
    egms['t'] = t

    src_vtx_labels = []
    for idx, vtx in enumerate(src_vtx_list):
        lbl = list(lf_vtcs.keys())[list(lf_vtcs.values()).index(vtx)]
        src_vtx_labels.append(lbl)
        egms[lbl] = lf_egms[:, idx]

    for idx, vtx in enumerate(src_vtx_list):
        print('label {} - vertex {}'.format(src_vtx_labels[idx], src_vtx_list[idx]))

    # compute ECG
    ecgs = {}

    # Einthoven
    ecgs['t'] = t
    ecgs['I'] = egms['LA'] - egms['RA']
    ecgs['II'] = egms['RA'] - egms['LL']
    ecgs['III'] = egms['LA'] - egms['LL']

    # Goldberger
    ecgs['aVL'] = egms['LA'] - 0.5 * (egms['RA'] + egms['LL'])
    ecgs['maVR'] = egms['RA'] - 0.5 * (egms['RA'] + egms['LL'])
    ecgs['aVF'] = egms['LA'] - 0.5 * (egms['RA'] + egms['LL'])

    # Wilson
    WCT = (egms['LA'] + egms['RA'] + egms['LL']) / 3
    ecgs['V1'] = egms['V1'] - WCT
    ecgs['V2'] = egms['V2'] - WCT
    ecgs['V3'] = egms['V3'] - WCT
    ecgs['V4'] = egms['V4'] - WCT
    ecgs['V5'] = egms['V5'] - WCT
    ecgs['V6'] = egms['V6'] - WCT

    # make serializeable
    for k, v in egms.items():
        egms[k] = list(v)

    for k, v in ecgs.items():
        ecgs[k] = list(v)

    return egms, ecgs


def lf_ecg_plot(egms, ecgs, ID):

    import matplotlib.pyplot as plt

    # determine axes
    s_mn = 0
    s_mx = 0
    for k,v in egms.items():
        # skip time axis
        if k == 't':
            continue

        mn = min(v)
        mx = max(v)
        if mn < s_mn:
            s_mn = mn
        if mx > s_mx:
            s_mx = mx

    for k,v in ecgs.items():
        # skip time axis
        if k == 't':
            continue

        mn = min(v)
        mx = max(v)
        if mn < s_mn:
            s_mn = mn
        if mx > s_mx:
            s_mx = mx

    # split off time axis
    t = egms['t']
    t_mn = min(t)
    t_mx = max(t)

    # plot EGMs first
    fig_egms, axs_egms = plt.subplots(len(egms)-1)
    idx = 0
    for k, v in egms.items():
        if k == 't':
            continue
        fig_egms.suptitle('EGMs - {}'.format(ID))
        axs_egms[idx].plot(t, v)
        axs_egms[idx].set_title(k)
        axs_egms[idx].set_xlim([t_mn, t_mx])
        axs_egms[idx].set_ylim([s_mn, s_mx])
        idx += 1

    # plot two columns, limb leads and precordial, following Cabrera
    limb_leads = ['aVL', 'I', 'maVR', 'II', 'aVF', 'III']
    prec_leads = ['V1', 'V2', 'V3', 'V4', 'V5', 'V6']

    fig, axs = plt.subplots(len(limb_leads), 2)

    for k, v in ecgs.items():
        if k == 't':
            continue

        if k in limb_leads:
            c_idx = 0
            r_idx = limb_leads.index(k)
        else:
            c_idx = 1
            r_idx = prec_leads.index(k)

        axs[r_idx, c_idx].plot(t, ecgs[k])
        axs[r_idx, c_idx].set_title(k)
        axs[r_idx, c_idx].set_xlim([t_mn, t_mx])
        axs[r_idx, c_idx].set_ylim([s_mn, s_mx])

    for ax in axs.flat:
        ax.set(xlabel='Time [ms]', ylabel='Lead')

    # Hide x labels and tick labels for top plots and y ticks for right plots.
    for ax in axs.flat:
        ax.label_outer()

    plt.show()


if __name__ == '__main__':

  # build parser
  opts = parser().parse_args()

  # inputs
  src_lff = opts.src_lf_conf
  src_ptsf = opts.src_torso
  trg_ptsf = opts.trg_torso

  # check whether we have a transform to be applied to the target mesh
  trfm = None
  if opts.trfm is not None:
      trfm = read_trfm(opts.trfm)

  trfm_leadfield(src_lff, src_ptsf, trg_ptsf, trfm)
