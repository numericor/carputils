#!/usr/bin/env python3

import numpy as np
import json as j
import argparse
import glob as g
from os.path import basename, dirname, join, isfile, splitext
from os import listdir
from re import findall
import sys
import subprocess
from carputils.forcepss import j2carp as j2c
import carputils.cext.mshread as mshread
import carputils.cext.mshwrite as mshwrite

def parser():

    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--plan',
                        type=str,
                        default='plan.vita.tmpl.json',
                        help='studio simulation plan template for VITA, default is plan.vita.tmpl.json')
    
    parser.add_argument('--vita-mesh', type=str, default=None,
                       help='Basename of myocardial mesh to run vita on.')

    parser.add_argument('--torso-mesh', type=str, default=None,
                        help='Basename of combined torso+mycardial mesh for ECG computation. (Default: None)')

    parser.add_argument('--vita-dir', type=str, default=None,
                       help='output directory of vita run holding the meshes')

    parser.add_argument('--launch', type=bool, default=False,
                        help='Launch studio')
    return parser


def convert_stims_vtx2json(ffilt, lfilt):

    # filter stimulus files
    lbl_cut = len(lfilt)-1
    files = g.glob(ffilt)    

    edict = {}
 
    r = 1000.
    t = 'sphere'
 
    for f in files:
        s_label = basename(f)[:-lbl_cut]

        print('Converting {} to stimulus {}.'.format(f,s_label))
        vtcs = np.loadtxt(f)
 
        # redefine as sphere
        e = {"p0": list(vtcs), "type":t, "radius":r}
        
        s_label = basename(f)[:-lbl_cut]
        edict[s_label] = e

    return edict    
 

def read_VT_stims(vtsf):

    with open(vtsf,'rt') as f:
        lines = f.readlines()

    files = []
    rtts  = []
    wes   = []
    for l in lines:
        f, r, w = l.split()
        files.append(f)
        rtts.append(r)
        wes.append(w)

    return files, rtts, wes

def splt2vtcs(sf):

    vtcs = np.loadtxt(sf, dtype=np.uint, skiprows=1, usecols=0)

    vtcs = np.unique(np.sort(vtcs))

    return vtcs

def write_vtx_file(vf, vtcs, domain):

    hd = '%d\n%s'%(len(vtcs), domain)
    np.savetxt(vf, vtcs, fmt='%d', header=hd, comments='')


def block_stimulus(blk_vtcs, blk_dur, clmp_vm):

    active = 0

    blk = {}
    blk['active'] = active
    
    elec = {}
    elec['type'] = 'vtxdata'
    elec['vtxdata'] = list(blk_vtcs)

    prtcl = {}
    prtcl['bcl'] = 1.0
    prtcl['duration'] = blk_dur
    prtcl['npls'] = 1
    prtcl['start'] = 0.0

    blk['electrode'] = elec
    blk['protocol'] = prtcl

    blk['strength'] = clmp_vm
    blk['type'] = 'Transmembrane Voltage clamp'

    return blk


def vt_trigger_stimulus(vt_trigger_vtcs, start, strength):

    # initially deactivated
    active = 0

    stim = {}
    stim['active'] = active

    elec = {}
    elec['type'] = 'vtxdata'
    elec['vtxdata'] = list(vt_trigger_vtcs)

    prtcl = {}
    prtcl['bcl'] = 1.0
    prtcl['duration'] = 1.0
    prtcl['npls'] = 1
    prtcl['start'] = 0.0

    stim['electrode'] = elec
    stim['protocol'] = prtcl

    stim['strength'] = strength
    stim['type'] = 'Transmembrane Current'

    return stim


def pacing_stimulus(elec, start, strength):

    active = 0

    stim = {}
    stim['active'] = active
    
    prtcl = {}
    prtcl['bcl'] = 1.0
    prtcl['duration'] = 1.0
    prtcl['npls'] = 1
    prtcl['start'] = 0.0

    stim['electrode'] = elec
    stim['protocol'] = prtcl

    stim['strength'] = strength 
    stim['type'] = 'Transmembrane Current'

    return stim


def vt_trigger_from_vtx_blk(mesh, vtxf, xtags):

    s_xtags = [str(tg) for tg in xtags]

    cmd = ['meshtool', 'extract', 'vtxhalo',
           '-msh={}'.format(mesh),
           '-ifmt=carp_bin',
           '-vtx={}'.format(vtxf),
           '-tags={}'.format(','.join(s_xtags))]
    HDR = '-'*80
    print(HDR + '\nCALLING: ' + ' '.join(cmd) + '\n' + HDR)
    subprocess.check_call(cmd)
    print(cmd)
    print('\n' * 2)

    # Read in split halos
    print('reading split halo files')
    vtxfs_halo_base = splitext(vtxf)[0]
    halo_filt = vtxfs_halo_base+'_halo?.vtx'
    halo_fs = g.glob(halo_filt)

    vt_triggers = []
    for halo_f in halo_fs:
        halo = mshread.read_vertex_data(halo_f)
        vt_triggers.append(halo[0])

    return vt_triggers


def map_vtcs(vtcs, map):

    if map is not None:
        print('Mapping intra nodal indices to extra mesh indices')
        for i, v in enumerate(vtcs):
            vtcs[i] = map[vtcs[i]]
    else:
        print('Empty map, skip vertex mapping.')

    return vtcs


def vita2studio(vita_dir, vita_torso_grid, vita_grid, vita_plan, launch):

    # define input data
    # pacing site directory
    pacing_dir = '{}/pacingSites'.format(vita_dir)
    pacing_filt = '*_coords.dat'
    # unique VTs
    uVTs = '{}/vita/STIM_uVTs.txt'.format(vita_dir)

    # vita runs on intra grid only for now, studio may run on combined grid
    sep = '-'*80
    print('\n\nConverting vita output to studio simulation plan')
    print('%s\n'%(sep))
    print('vita directory        - %s'%(vita_dir))
    print('mesh (vita)           - %s'%(vita_grid))
    if vita_torso_grid:
        print('mesh with torso (ECG)  - %s'%(vita_torso_grid))
    print('pacing directory      - %s'%(pacing_dir))
    print('pacing filter         - %s'%(pacing_filt))
    print('vita unique VT blocks - %s'%(uVTs))

    i2e_map = None
    # do we have both intra and extra grid?
    intra_mesh_file = g.glob('{}.belem'.format(join(vita_dir, vita_grid)))
    if not intra_mesh_file:
        print('\n\nMyocardial mesh file {}.belem required for VITA not found, exiting.\n\n'.format(join(vita_dir, vita_grid)))
        sys.exit()

    if vita_torso_grid:
        extra_mesh_file = g.glob('{}.belem'.format(join(vita_dir, vita_torso_grid)))

        if not extra_mesh_file:
            print('\n\nTorso mesh file {}.belem not available, ECG computation disabled.'.format(join(vita_dir, vita_torso_grid)))
        else:
            # extra mesh found, look for mapping
            # read nod file to extract index mapping
            nod_file = join(vita_dir,'{}.nod'.format(vita_grid))
            if isfile(nod_file):
                print('\n\nNodal mapping from {} to {} found in {}.'.format(vita_grid, vita_torso_grid, nod_file))
                print('Reading nodal index map from {}.'.format(nod_file))
                i2e_map = np.fromfile(nod_file, dtype=np.int64)
                vita_studio_grid = '{}'.format(join(vita_dir, vita_torso_grid))
            else:
                print('\n\nNodal mapping file {} not found.'.format(nod_file))
                print('Disabling ECG computation')
    else:
        vita_torso_grid = vita_grid


    vita_studio_grid = '{}'.format(join(vita_dir, vita_torso_grid))

    # convert pacing electrodes from coordinates to json
    print('\nConverting electrode coordinate files to json electrodes.')
    print('{}\n'.format(sep))
    
    sffilt = join(pacing_dir, pacing_filt)
    slfilt = pacing_filt 

    elecs = convert_stims_vtx2json(sffilt, slfilt)

    # save electrodes in json
    elec_file = join(vita_dir, '{}.electrodes.json'.format(vita_torso_grid))
    print('\nSaving electrodes to {}.'.format(elec_file))
    j.dump(elecs, open(elec_file, 'wt'), indent=4)

    pacing_stims = {}
    for e in elecs: 
       pacing_stims[e] = pacing_stimulus(elecs.get(e), 0., 100.)

    pacing_stim_file = join(vita_dir, '{}.pacing.stimuli.json'.format(vita_torso_grid))
    print('Saving pacing stimuli to {}.\n'.format(pacing_stim_file))
    j.dump(pacing_stims, open(pacing_stim_file, 'wt'), indent=4)

    # create unique VT initiation blocks
    print('\n\nConverting unique VT initiation sites from split to nodal block')
    print('{}\n'.format(sep))

    # read in VT stims file
    print('Reading out VITA unique VT initiation sites from {}.'.format(uVTs))
    splfls, rtts, wes = read_VT_stims(uVTs)
    if not splfls:
        print('No VITA unique initiation sites found in {}'.format(uVTs))
    else:
        mx_len = 50
        for i, splfl in enumerate(splfls):
            print('{:50s} {:5.2f} {:5.2f}'.format(splfl, float(rtts[i]), float(wes[i])))

    # iterate over split files 
    blk_stimuli = {}
    vt_stimuli = {}
    for f in splfls:
        spltf = basename(f)
        splt_ID = '_'.join(findall(r'\d+', spltf))

        print('Converting split %s.splt to vertex file %s.vtx'%(f, join(dirname(f), spltf)))

        vtcs = splt2vtcs('%s.splt'%(f))
        vtx_file = join(dirname(f), '%s.intra.vtx'%(spltf))
        write_vtx_file(vtx_file, vtcs, 'intra')

        # map back onto combined heart-torso grid, if mapped
        if i2e_map is not None:
            vtcs = map_vtcs(vtcs, i2e_map)
            vtx_file = join(dirname(f), '%s.torso.vtx'%(spltf))
            write_vtx_file(vtx_file, vtcs, 'extra')
            #mshwrite(vtx_file, vtcs, 'extra')

        # write json block section
        blk_dur = 150.
        clmp_vm = -90.
        blk_stimuli['BLK_{}'.format(splt_ID)] = block_stimulus(vtcs.tolist(), blk_dur, clmp_vm)

        print('Computing VT trigger surfaces from VITA split {}'.format(join(dirname(f), spltf)))
        #vt_halos = vt_trigger_from_vtx_blk(join(vita_dir, vita_torso_grid), '{}.torso.vtx'.format(f), [0,10,200])
        vt_halos = vt_trigger_from_vtx_blk(join(vita_dir, vita_grid), '{}.intra.vtx'.format(f), [])

        # write json vt trigger section
        for i, vt_halo in enumerate(vt_halos):
            vt_halo = map_vtcs(vt_halo, i2e_map)
            start = 0.
            strength = 100.
            vt_stimuli['VT_{}--{}'.format(splt_ID,i)] = vt_trigger_stimulus(vt_halo.tolist(), start, strength)

    # write blocks to stimulus json section
    blks = join(vita_dir, '{}.blocking.stimuli.json'.format(vita_torso_grid))
    print('\nWriting unique VT initiation sites to block stimulus {}.\n'.format(blks))
    j.dump(blk_stimuli, open(blks,'wt'), indent=4) 

    
    # load vita template and insert stimuli and blocks
    #with open(plan_tmpl,'rt') as f:
    plan = j2c.load_json(vita_plan)

    plan['electrodes'] = elecs
    plan['simulation']['stimuli'] = blk_stimuli
    plan['simulation']['stimuli'].update(vt_stimuli)
    plan['simulation']['stimuli'].update(pacing_stims)

    print('\n{}'.format(sep))
    print('Writing vita plan for studio simulation to {}.\n\n'.format(vita_plan))
    j2c.dump_json(vita_plan, plan)

    if launch:
        studio_cmd = ['studio', vita_studio_grid, vita_plan]
        subprocess.check_call(studio_cmd)
    else:
        print('\n{}'.format(sep))
        print('Launch studio: studio {} {}\n\n'.format(vita_studio_grid, vita_plan))


if __name__ == '__main__':

    # build parser
    opts = parser().parse_args()

    # run vita to studio conversion
    vita2studio(opts.vita_dir, opts.torso_mesh, opts.vita_mesh, opts.plan, opts.launch)