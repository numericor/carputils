#!/usr/bin/env python3

import os
import sys
import subprocess
import argparse

HDR = '='*80

def parser():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('--heart',
                        type=str,
                        default=None,
                        help='Volumetric heart to be integrated into torso')

    parser.add_argument('--torso', type=str, default=None,
                        help='Surface torso mesh.')

    parser.add_argument('--lungs', type=str, default=None,
                        help='Surface lung mesh.')

    parser.add_argument('--trfm', type=str, default=None,
                        help='Affine transformation file to apply to lungs and torso.')

    parser.add_argument('--mesh', type=str, default=None,
                        help='Filename for storing the combined mesh.')

    parser.add_argument('--keep', action='store_true',
                        help='Keep intermediate files written during meshing procedure.')
    return parser


def remove_file(path):
  if os.path.exists(path) and os.path.isfile(path):
    os.remove(path)
    print('file "{}" removed !'.format(path))
  else:
    print('failed to remove file "{}" !'.format(path))


def mg_torso(torso, lungs, ins_heart_mesh, out_mesh, keep_files):

  # parse input data
  #heart_mesh = heart.get('mesh')
  #heart_tag  = heart.get('tag')

  torso_surf = torso.get('mesh')
  torso_tag  = torso.get('tag')[0]

  if lungs is not None:
    lungs_surf = lungs.get('mesh')
    lungs_tag  = lungs.get('tag')[0]

  # output data
  merged_mesh = out_mesh #(output)

  odir = os.path.dirname(merged_mesh)
  if not os.path.isdir(odir):
      os.makedirs(odir)

  # intermediate torso-lung mesh
  torso_lungs_mesh = os.path.join(odir, 'torso_lungs.vtk')

  # basename of the heart mesh
  ins_heart_base = os.path.splitext(ins_heart_mesh)[0]


  # Extract the surface of the volumetric heart mesh
  # ------------------------------------------------
  cmd = ['meshtool', 'extract', 'surface',
         '-msh={}'.format(ins_heart_mesh),
         '-ifmt=carp_bin',
         '-surf={}'.format(ins_heart_base),
         '-op=_all_',
         '-ofmt=vtk_bin']
  print(HDR + '\nCALLING: ' + ' '.join(cmd) + '\n' + HDR)
  heart_surf = '{}.surfmesh.vtk'.format(ins_heart_base)
  subprocess.check_call(cmd)
  print('\n'*2)

  if not keep_files:
    remove_file('{}.surf'.format(ins_heart_base))
    remove_file('{}.surf.vtx'.format(ins_heart_base))
    remove_file('{}.neubc'.format(ins_heart_base))
    remove_file('{}.surfmesh.nod'.format(ins_heart_base))
    print('\n'*2)

  # Create volumetric mesh from torso, lungs and heart 
  # surface meshes. Take the heart surface to define the "hole".
  # ------------------------------------------------------------------
  hole_tag = 0 # in case of doubt, assume torso
  if lungs is not None:
    cmd = ['meshtool', 'generate', 'mesh',
          '-surf={},{},{}'.format(torso_surf, lungs_surf, heart_surf),
          '-ins_tag={},{},{}'.format(torso_tag, lungs_tag, hole_tag),
          '-holes=0,0,1',
          '-outmsh={}'.format(torso_lungs_mesh)]
  else:
    cmd = ['meshtool', 'generate', 'mesh',
          '-surf={},{}'.format(torso_surf, heart_surf),
          '-ins_tag={},{}'.format(torso_tag, hole_tag),
          '-holes=0,1',
          '-outmsh={}'.format(torso_lungs_mesh)]
  print(HDR + '\nCALLING: ' + ' '.join(cmd) + '\n' + HDR)
  subprocess.check_call(cmd)
  print('\n'*2)

  if not keep_files:
    remove_file('{}.surfmesh.vtk'.format(ins_heart_base))
    print('\n'*2)

  # Merge newly created mesh with the volumetric heart mesh
  # ------------------------------------------------------------------
  cmd = ['meshtool', 'merge', 'meshes',
         '-msh1={}'.format(torso_lungs_mesh),
         '-msh2={}'.format(ins_heart_mesh),
         '-outmsh={}'.format(merged_mesh),
         '-ofmt=carp_bin']
  print(HDR + '\nCALLING: ' + ' '.join(cmd) + '\n' + HDR)
  subprocess.check_call(cmd)
  print('\n'*2)

  if not keep_files:
    torso_lungs_base = os.path.splitext(torso_lungs_mesh)[0]
    remove_file('{}.fcon'.format(torso_lungs_base))
    remove_file('{}.fcon'.format(ins_heart_base))
    remove_file(torso_lungs_mesh)
    print('\n'*2)


def main(opts):

  # input data
  heart_mesh = opts.heart
  torso_surf = opts.torso
  lungs_surf = opts.lungs
  keep_files = opts.keep

  # Tag of the torso region in the final volumetric mesh
  torso_tag = 0
  # Tag of the lungs region in the final volumetric mesh
  lungs_tag = 1
  # Tag of the heart region in the final volumetric mesh.
  # Just a dummy value since the heart is not meshed.
  heart_tag = 2

  odir = os.path.dirname(opts.mesh)
  if not os.path.isdir(odir):
      os.makedirs(odir)

  #torso_lungs_mesh = os.path.join(os.output_dir, 'torso_lungs.vtk')  # (output)
  #merged_mesh = os.path.join(opts.output_dir, 'merged_mesh.vtk')       # (output)
  torso_lungs_mesh = os.path.join(odir, 'torso_lungs.vtk') #(output)
  merged_mesh = opts.mesh #(output)

  # basename of the heart mesh
  heart_base = os.path.splitext(heart_mesh)[0]

  # Extract the surface of the volumetric heart mesh
  cmd = ['meshtool', 'extract', 'surface',
         '-msh={}'.format(heart_mesh),
         '-surf={}'.format(heart_base),
         '-op=_all_',
         '-ofmt=vtk_bin']
  print(HDR + '\nCALLING: ' + ' '.join(cmd) + '\n' + HDR)
  heart_surf = '{}.surfmesh.vtk'.format(heart_base)
  subprocess.check_call(cmd)
  print('\n'*2)

  if not keep_files:
    remove_file('{}.surf'.format(heart_base))
    remove_file('{}.surf.vtx'.format(heart_base))
    remove_file('{}.neubc'.format(heart_base))
    remove_file('{}.surfmesh.nod'.format(heart_base))
    print('\n'*2)

  # Get point inside the heart surface. Needed for 
  # the mesh generation
  cmd = ['meshtool', 'query', 'insidepoint',
         '-msh={}'.format(heart_mesh)]
  print(HDR + '\nCALLING: ' + ' '.join(cmd) + '\n' + HDR)
  output_bytes = subprocess.check_output(cmd)
  output = output_bytes.decode("utf-8") 
  print(output)
  output = list(filter(lambda s: len(s.strip()) > 0, output.split('\n')))
  ipnt_str = output[-1].strip()
  ipnt = list(map(float, ipnt_str.split(',')))
  print('\nINSIDE-POINT:  {0}, {1}, {2}\n'.format(*ipnt))

  # Create volumetric mesh from torso, lungs and heart 
  # surface meshes. Take center of first heart element
  # to define the "hole".
  cmd = ['meshtool', 'generate', 'mesh',
         '-surf={},{},{}'.format(torso_surf, lungs_surf, heart_surf),
         '-ins_tag={},{},{}'.format(torso_tag, lungs_tag, heart_tag),
         '-holes={},{},{}'.format(*ipnt),
         '-outmsh={}'.format(torso_lungs_mesh)]
  print(HDR + '\nCALLING: ' + ' '.join(cmd) + '\n' + HDR)
  subprocess.check_call(cmd)
  print('\n'*2)

  if not keep_files:
    remove_file('{}.surfmesh.vtk'.format(heart_base))
    print('\n'*2)

  # Merge newly created mesh with the volumetric heart mesh
  cmd = ['meshtool', 'merge', 'meshes',
         '-msh1={}'.format(torso_lungs_mesh),
         '-msh2={}'.format(heart_mesh),
         '-outmsh={}'.format(merged_mesh),
         '-ofmt=carp_bin']
  print(HDR + '\nCALLING: ' + ' '.join(cmd) + '\n' + HDR)
  subprocess.check_call(cmd)
  print('\n'*2)

  if not keep_files:
    torso_lungs_base = os.path.splitext(torso_lungs_mesh)[0]
    remove_file('{}.fcon'.format(torso_lungs_base))
    remove_file('{}.fcon'.format(heart_base))
    remove_file(torso_lungs_mesh)
    print('\n'*2)


if __name__ == '__main__':

  # build parser
  opts = parser().parse_args()

  heart = {}
  heart['mesh'] = opts.heart
  heart['tag']  = 10

  torso = {}
  torso['mesh'] = opts.torso
  torso['tag']  = 0

  lungs = {}
  lungs['mesh'] = opts.lungs
  lungs['tag']  = 200
 
  mg_torso(heart, torso, lungs, opts.mesh, opts.keep)


  #main(opts)
