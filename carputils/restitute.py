#!/usr/bin/env python3

import os
EXAMPLE_DESCRIPTIVE_NAME = 'Single-cell APD restitution'
EXAMPLE_AUTHOR = 'Jason Bayer <jason.bayer@ihu-liryc.fr>'
EXAMPLE_DIR = os.path.dirname(__file__)
GUIinclude = True

from datetime import date
from carputils import settings
from carputils import tools
from matplotlib import pyplot as plt
import numpy as np


def plotResults(di,apd,xmin,xmax,ymin,ymax,webgui,idExp):
    if (webgui):
        datadic = {'labels':{'labelsAB':[]}, \
                'datasets':{'xlim':[xmin,xmax], 'ylim':[ymin,ymax],'labelXY':['Diastolic Interval (ms)', 'Action Potential Duration (ms)'],\
                'valueX':di, 'valueY1':apd, 'valueY2':[]}}
        
        with open(idExp + "_" + "matplotM.txt", 'w') as f:
            for key, value in datadic.items():
                for key2, value2 in value.items():
                    f.write('%s\n' % (value2))
    else:
        # Plot APD vs DI
        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)
    
        ax.plot(di, apd, 'rx-')
        
        ax.set_xlabel('Diastolic Interval (ms)')
        ax.set_ylabel('Action Potential Duration (ms)')
        ax.set_ylim(ymin,ymax)
        ax.set_xlim(xmin,xmax)
        
        plt.show()

# read APD restitution file or AP statistics file
def readAPDrestitutionFile(APDfile, flt):

    f = open(APDfile, 'r')
    lines = f.readlines()

    beat = []
    DI   = []
    APD  = []
    DIp  = []
    Tri  = []
    Vmx  = []
    Vmn  = []
    tAct = []

    PCL  = []
    PCLp = []
    APA  = []

    cnt = int(0)
    diffDI = float(0)
    diffAPD = float(0)

    for line in lines:
        if line[0] == '#':
          continue
        p = line.split()
        if cnt > 0:
            diffDI = DI[cnt-1] - float(p[5])
            diffAPD = APD[cnt-1] - float(p[3])

        add_line = False
        if flt=='all':
            add_line = True
        elif flt=='premature':
            if p[1] == 'P':
                add_line = True
        elif flt=='res-curve':
            # make sure to pick only premature beats within a given
            # window in terms of change in DI and APD
            if p[1]=='P' and diffDI > -5. and diffAPD > -10. and diffAPD < 80.:
                add_line = True
        else:
            print('Invalid filter option for restitution/AP statistics data given.')
            print('Switch to filter setting \'all\'.')
            flt='all'
            add_line=True

        if add_line:
            beat.append(float(p[0]))
            APD.append(float(p[3]))
            DI .append(float(p[4]))
            DIp.append(float(p[5]))
            Tri.append(float(p[6]))
            Vmx.append(float(p[7]))
            Vmn.append(float(p[8]))
            tAct.append(float(p[9]))

            PCL.append(APD[cnt] + DI[cnt])
            PCLp.append(APD[cnt] + DIp[cnt])
            APA.append(Vmx[cnt] - Vmn[cnt])

            cnt += 1

    f.close()

    # determine tAct of premature beat
    tAct = list(np.array(tAct) - np.array(PCL))

    apdTbl = {}
    apdTbl['beat'] = beat
    apdTbl['APD']  = APD
    apdTbl['DIp']  = DIp
    apdTbl['DI' ]  = DI
    apdTbl['Tri']  = Tri
    apdTbl['Vmx']  = Vmx
    apdTbl['Vmn']  = Vmn
    apdTbl['APA']  = APA
    apdTbl['tAct'] = tAct
    apdTbl['PCL']  = PCL
    apdTbl['PCLp'] = PCLp

    return apdTbl


def min_max(x, y, lims):

    xmin = min(x)
    xmax = max(x)
    ymin = min(y)
    ymax = max(y)

    cur_lims = [ xmin, xmax, ymin, ymax ]

    if not len(lims):
        lims = cur_lims
    else:
        if cur_lims[0] < lims[0]:
            lims[0] = cur_lims[0]
        if cur_lims[2] < lims[2]:
            lims[2] = cur_lims[2]
        if cur_lims[1] > lims[1]:
            lims[1] = cur_lims[1]
        if cur_lims[3] > lims[3]:
            lims[3] = cur_lims[3]

    return lims


def plotAPDrestitutionSlope(ax, APD, DI, lb, col):

    # compute slope of restitution curve
    dDI   = np.gradient(DI)
    dAPD  = np.gradient(APD)
    slope = dAPD/dDI   # determine axes

    di_marg = 10
    slope_marg = 0.3

    xmin = min(DI) - di_marg
    xmax = max(DI) + di_marg
    ymin = min(slope) - slope_marg
    ymax = max(slope) + slope_marg

    ax.plot(DI, slope, color='black', linestyle='dashed', marker='o',
            markerfacecolor=col, markersize=8, label=lb)

    # plot steepness threshold 
    ax.plot([xmin, xmax], [1., 1.], 'r--', label='threshold')

    
    ax.set_title('Restitution Slope d APD / d DI)')
    ax.set_ylabel('Restitution Slope (-)')
    ax.set_xlabel('Diastolic Interval (ms)')
    ax.set_ylim(ymin,ymax)
    ax.set_xlim(xmin,xmax)


def getAPDrestitutionTable(apdfile, tbl_file, flt):

       # plot APD restitution
       apdTbl = readAPDrestitutionFile(apdfile, flt)

       # write table to console first
       print('\n\nRestitution Table {}'.format(apdfile))
       print('----------------------------------------------')
       print('Beat %6s %6s %6s %6s %6s %6s %6s %9s %6s %6s' % ('APD', 'DIp', 'DI', 'Tri', 'APA', 'Vmx', 'Vmn', 'tAct', 'PCL', 'PCLp'))
       for i, d in enumerate(apdTbl['APD']):
            print('%4d %6.2f %6.2f %6.2f %6.2f %6.2f %6.2f %6.2f %9.2f %6.2f' % ( apdTbl['beat'][i], apdTbl['APD'][i],
            apdTbl['DIp'][i], apdTbl['DI'][i], apdTbl['Tri'][i], apdTbl['APA'][i],
            apdTbl['Vmx'][i], apdTbl['Vmn'][i], apdTbl['tAct'][i], apdTbl['PCL'][i]))

       # dump cleaned restitution table to file
       with open(tbl_file,'w') as fp:
           fp.write('Beat %6s %6s %6s %6s %6s %6s %6s %9s %6s %6s\n'%('APD', 'DIp', 'DI', 'Tri', 'APA', 'Vmx', 'Vmn', 'tAct', 'PCL', 'PCLp'))
           for i,d in enumerate(apdTbl['APD']):
              fp.write('%4d %6.2f %6.2f %6.2f %6.2f %6.2f %6.2f %6.2f %9.2f %6.2f %6.2f\n'%(apdTbl['beat'][i], apdTbl['APD'][i],
                       apdTbl['DIp'][i], apdTbl['DI'][i], apdTbl['Tri'][i], apdTbl['APA'][i],
                       apdTbl['Vmx'][i], apdTbl['Vmn'][i], apdTbl['tAct'][i], apdTbl['PCL'][i], apdTbl['PCLp'][i]))

           fp.close()

       return apdTbl


def plotAPDrestitution(apdfiles, labels, plt_slope, plt_pcl):

   # Plot APD vs DI
   fig = plt.figure()

   subs = 1 + int(plt_slope) + int(plt_pcl)

   r_idx = s_idx = a_idx = 1
   r_ax  = fig.add_subplot(subs, 1, 1)

   if plt_slope:
       s_idx = r_idx +1
       s_ax = fig.add_subplot(subs, 1, s_idx)
   if plt_pcl:
       a_idx = s_idx + 1
       a_ax = fig.add_subplot(subs, 1, a_idx)

   r_lims = []
   s_lims = []
   a_lims = []

   # multi-trace plot, chose from set of colors
   colors = ['b', 'r', 'g', 'c', 'm', 'y']
   ncols  = len(colors)

   for i,f in enumerate(apdfiles):

       l = labels[i]

       # plot APD restitution
       apdrf, ext = os.path.splitext(f)
       apdTbl = getAPDrestitutionTable(f,apdrf+'.tbl'+ext, flt='res-curve')
       apdTblAll = getAPDrestitutionTable(f, apdrf + '.all.tbl' + ext, flt='all')

       r_ax.plot(apdTbl['DIp'], apdTbl['APD'], color='black', linestyle='dashed', marker='o',
                 markerfacecolor=colors[i%ncols], markersize=8, label=l)

       # keep track of min/max
       r_lims = min_max(apdTbl['DIp'], apdTbl['APD'], r_lims)

       if plt_slope:
           # plot slope of APD restitution
           dDI   = np.gradient(apdTbl['DIp'])
           dAPD  = np.gradient(apdTbl['APD'])
           slope = dAPD/dDI   # determine axes

           s_ax.plot(apdTbl['DIp'], slope, color='black', linestyle='dashed', marker='o',
                 markerfacecolor=colors[i%ncols], markersize=8, label=l)

           # keep track of min/max
           s_lims = min_max(apdTbl['DIp'], slope, s_lims)

       if plt_pcl:
           a_ax.plot(apdTblAll['PCLp'], apdTblAll['APD'], marker='o', linestyle='None', markerfacecolor=colors[i%ncols], markersize=8, label=l+'-PCL')
           a_ax.plot(apdTblAll['DIp'], apdTblAll['APD'], marker='.', linestyle='None', markerfacecolor=colors[i%ncols], markersize=8, label=l+'-DI')
           a_lims = min_max(apdTblAll['PCLp'] + apdTblAll['DIp'], apdTblAll['APD'], a_lims)

   # finish after adding all restitution curves to plot
    
   r_ax.set_title('Restitution APD(n+1)=f(DI(n))')
   r_ax.set_xlabel('Diastolic Interval (ms)')
   r_ax.set_ylabel('Action Potential Duration (ms)')

   xmin, xmax, ymin, ymax = r_lims
   x_marg = y_marg = 10
   r_ax.set_ylim(ymin - y_marg, ymax + y_marg)
   r_ax.set_xlim(xmin - x_marg, xmax + x_marg)

   r_ax.legend()
   
   if plt_slope:
      
      s_ax.set_title('Restitution Slope d APD / d DI)')
      s_ax.set_ylabel('Restitution Slope (-)')
      s_ax.set_xlabel('Diastolic Interval (ms)')

      # plot steepness threshold 
      s_ax.plot([xmin, xmax], [1., 1.], 'r--', label='threshold')

      xmin, xmax, ymin, ymax = s_lims
      x_marg = 10
      y_marg = 0.2
      s_ax.set_ylim(ymin - y_marg, ymax + y_marg)
      s_ax.set_xlim(xmin - x_marg, xmax + x_marg)

      s_ax.legend()

   if plt_pcl:
       a_ax.set_title('Restitution APD(n+1)(PCL(n)=APD(n+1)+DI(n))')
       a_ax.set_ylabel('Action Potential Duration (ms)')
       a_ax.set_xlabel('Pacing Cycle length, Diastolic Interval(ms)')

       xmin, xmax, ymin, ymax = a_lims
       x_marg = y_marg = 10
       a_ax.set_ylim(ymin - y_marg, ymax + y_marg)
       a_ax.set_xlim(xmin - x_marg, xmax + x_marg)

       a_ax.legend()

   # print to file
   plt.savefig('APD_restitution.pdf', orientation='landscape')
   plt.savefig('APD_restitution.png', orientation='landscape')

   plt.show()


def getAPs(aptrace, tActs):

    # read in trace file
    ap_trc = np.loadtxt(aptrace)

    # time and AP Vm vector
    t = ap_trc[:,0]
    ap = ap_trc[:,1]

    # determine number of samples per AP observation periods
    AP_window = 600
    dt = t[1] - t[0]
    samples = int(AP_window/dt)

    aps = np.zeros((2,samples,len(tActs)))
    #ts  = np.zeros((samples,len(tActs)))
    for i, ta in enumerate(tActs):
        idx_ap0 = np.argwhere(np.diff(np.sign(t - ta))).flatten()[0]
        aps[0,:,i] = ap[idx_ap0:idx_ap0+samples]
        aps[1,:,i] = t [idx_ap0:idx_ap0+samples]

    return aps


# extract all premature APs
def getRestitutionAPs(apdfiles, labels, aptraces):

    APs  = {}
    DIps = {}
    APDs = {}
    for i, f in enumerate(apdfiles):
        l = labels[i]

        # get APD restitution table
        apdrf, ext = os.path.splitext(f)
        apdTbl = getAPDrestitutionTable(f, apdrf + '.tbl' + ext, flt='res-curve')

        # get AP trace file
        APs[l]  = getAPs(aptraces[i], apdTbl['tAct'])
        DIps[l] = apdTbl['DIp']
        APDs[l] = apdTbl['APD']

    return APs, APDs, DIps

def plotRestitutionAPs(apdfiles, labels, aptraces):

    aps, apds, dips = getRestitutionAPs(apdfiles, labels, aptraces)

    nAPs = len(aps)
    fig, axs = plt.subplots(nAPs)
    if nAPs == 1:
        axs = np.array([axs])

    fig.suptitle('Restituted APs')
    apc = 0
    for key, APs in aps.items():

        # save restituted APs to file
        # extract time vector, start at t=0 ms
        t  = APs[1,:,0] - APs[1,0,0]

        # combined single time vector and Vm into matrix
        Vm = APs[0,:,:]
        Vm = np.column_stack((t,Vm))
        np.savetxt('Restituted_APs_{}.dat'.format(key), Vm,
                   fmt='%.18e',
                   delimiter=' ', newline='\n', header='', footer='',
                   comments='# ',
                   encoding=None)

        axs[apc].set_title(key)

        dims, samples, traces = APs.shape
        for i in range(0,traces):
            di  = dips[key][i]
            apd = apds[key][i]
            # time and voltage, time vector should start at 0.0
            t   = APs[1,:,i] - APs[1,0,i]
            Vm  = APs[0,:,i]
            axs[apc].plot(t, Vm, label='APD/DI: %6.2f  / %6.2f'%(apd,di))

        axs[apc].legend()
        axs[apc].set_xlabel('Time [ms]')
        axs[apc].set_ylabel('Vm [mV]')

        apc += 1

    # plot restituted APs to file
    plt.savefig('Restituted_APs.pdf', orientation='portrait')
    plt.savefig('Restituted_APs.png', orientation='portrait')

    plt.show()


def visualize_APD_DI(job_id, webgui, ID):
    """
    """
    apdfile = os.path.join(job_id, 'restout_APD_restitution.dat')
    apdTbl = readAPDrestitutionFile(apdfile, flt='res-curve')

    APD = apdTbl['APD']
    DIp = apdTbl['DIp']
    plotResults(apdTbl['DIp'], apdTbl['APD'], min(DIp)-10, max(DIp)+10, min(APD)-10, max(APD)+10, webgui, ID)
#    plotResults(di,apd,0.0,args.CI1,0.0,args.CI1)


def jobID(args):
    today = date.today()
    ID = '{}_CI0-{}_CI1-{}_{}'.format(today.isoformat(), args.CI0, args.CI1, args.Protocol)
    if args.params:
        ID += '_{}'.format(args.imp,args.params)
    return ID


def restitute(job, imp, params, plgs, plgs_params, CI0, CI1, CIinc, nbeats, BCL, Protocol, prebeats, initial, dry):


    # Determine the threshold for the user input parameters
    stimcurr = 0
    thresh_achieved = False
    delta_curr = 2

    # check input args
    if not CI1:
        CI1 = BCL

    # set up ionic model
    imp_setup = ['--imp', imp]
    if params:
        imp_setup += ['--imp-par', params]

    # add plugins if any
    if plgs:
        imp_setup += ['--plug-in', plgs]

    if plgs_params :
        imp_setup += ['--plug-par', plgs_params]


    # build baseline command line
    bcmd = imp_setup

    while not thresh_achieved:
        stimcurr += delta_curr
        print('Currently applied stimulus current: {}'.format(stimcurr))

        # define protocol
        pars = ['--duration', 100.1,
                '--numstim', 1,
                '--stim-start', 1,
                '--bcl', 100,
                '--stim-curr', stimcurr,
                '--stim-dur', 2,
                '--fout={}'.format(os.path.join(job.ID, 'thresh')),
                '--save-time', 100,
                '--save-file', os.path.join(job.ID, 'thresh_save.sv')]
        
        # run threshold
        job.bench(bcmd+pars)


        # Now read in the data
        if dry:
            thresh_achieved = True
        else:
            vmfile = os.path.join(job.ID, 'thresh.txt')
            Vm = np.loadtxt(vmfile)
            if Vm[50,1] > -10.:     # why t=50ms?
                thresh_achieved = True

    stimcurr = stimcurr*2
    print('Chosen stimulus current: {}'.format(stimcurr))

    # run threshold
    job.bench(bcmd+pars)


   # Write the S1S2 restitution file
    if Protocol != 'S1S2' and Protocol != 'dynamic':
        print('Specified protocol {} not valid, using default settings for S1S2 protocol.'.format(Protocol))
        Protocol = 'S1S2'

    if Protocol == 'S1S2':
        ropt = 'S1S2'
        if not dry:
            with open(os.path.join(job.ID, 'restitution_protocol.txt'), 'w') as fp:
                fp.write('  1   # protocol selection 1=S1S2 0=dynamic\n')
                fp.write('{:3d} # number of prepacing beats before starting protocol\n'.format(prebeats))
                fp.write('{:3d} # basic cycle length\n'.format(BCL))
                fp.write('{:3d} # S2 prematurity start\n'.format(CI1))
                fp.write('{:3d} # S2 prematurity end\n'.format(CI0))
                fp.write('{:3d} # number of beats preceding premature one\n'.format(nbeats))
                fp.write('{:3d} # decrement in S2 prematurity in ms\n'.format(CIinc))

    if Protocol == 'dynamic':
        ropt='dyn'
        if not dry:
            with open(os.path.join(job.ID, 'restitution_protocol.txt'), 'w') as fp:
                fp.write('  0   # protocol selection 1=S1S2 0=dynamic\n')
                fp.write('{:3d} # number of prepacing beats before starting protocol\n'.format(prebeats))
                fp.write('{:3d} # initial basic cycle length\n'.format(BCL))
                fp.write('{:3d} # final basic cycle length\n'.format(CI0))
                fp.write('{:3d} # number of beats preceding premature one\n'.format(nbeats))
                fp.write('{:3d} # decrement in S2 prematurity in ms\n'.format(CIinc))

    # Run bench with restitution file
    pars = ['--stim-curr', stimcurr,
            '--stim-dur', 2,
            '--restitute', ropt,
            '--res-file',  os.path.join(job.ID, 'restitution_protocol.txt'),
            '--res-trace',
            '--fout={}'.format(os.path.join(job.ID, 'restout'))]

    if initial:
        pars += ['--read-ini-file', initial]

    # run threshold
    job.bench(bcmd+pars)

