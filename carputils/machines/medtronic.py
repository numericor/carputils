#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

from carputils.machines.general import BatchPlatform

TEMPLATE = """#!/bin/bash --login
#$ -N Job_{jobID:.15s}
#$ -pe pe_20 {nproc}
#$ -l h_rt={walltime}
#$ -q {queue}

# Change to the directory that the job was submitted from
# (remember this should be on the /work filesystem)
cd $SGE_O_WORKDIR

# Set the number of threads to 1
#   This prevents any system libraries from automatically
#   using threading.
#export OMP_NUM_THREADS=1

module load carp/mdt-1.0
"""

class Medtronic(BatchPlatform):
    """
    Run jobs on the Medtronic HPC
    """

    SUBMIT    = 'qsub'
    LAUNCHER  = 'mpirun'
    BATCH_EXT = '.uge'

    DEFAULT_QUEUE = 'aib.q'
    AVAILABLE_QUEUES = ['aib.q', 'aib2.q', 'eda.q']

    @classmethod
    def add_launcher(cls, carp_cmd, nproc, ddt=False, cuda=False, *args, **kwargs):
        cmd = [cls.LAUNCHER, '-np', nproc]
        cmd += carp_cmd
        return cmd

    @classmethod
    def header(cls, jobID, nproc, walltime, queue, cuda, email, config=None):

        nproc = int(nproc)

        # Check sensible number of nodes
        assert nproc % 20 == 0, 'Use a multiple of 20 processes on Medtronic HPC cluster'

        return TEMPLATE.format(jobID=jobID, nproc=nproc, walltime=walltime,
                               queue=cls.determine_queue(queue))
