#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

from carputils.machines.general import BatchPlatform

TEMPLATE = """#!/bin/bash --login
#PBS -N {jobID:.15s}
#PBS -l select={nnode}:ncpus=36:mpiprocs=36
#PBS -l walltime={walltime}
#PBS -A Pra15_3333
#PBS -m bea
#PBS -M {email}

# Make sure any symbolic links are resolved to absolute path
export PBS_O_WORKDIR=$(readlink -f $PBS_O_WORKDIR)

# Change to the directory that the job was submitted from
# (remember this should be on the /work filesystem)
cd $PBS_O_WORKDIR

# Set the number of threads to 1
#   This prevents any system libraries from automatically
#   using threading.
export OMP_NUM_THREADS=1

module load intel intelmpi

"""

class Marconi(BatchPlatform):
    """
    Run jobs on the MARCONI HPC Cluster

    Info: http://www.hpc.cineca.it/
    """

    SUBMIT = 'qsub'
    LAUNCHER = 'mpirun'
    BATCH_EXT = '.pbs'

    @classmethod
    def add_launcher(cls, carp_cmd, nproc, ddt=False, cuda=False,
                     *args, **kwargs):
        cmd = [cls.LAUNCHER]
        cmd += carp_cmd

        if ddt:
            # Run with ddt in reverse connection mode
            cmd = ['ddt', '--connect',
                   '--cuda' if cuda else '--no-cuda']+cmd

        return cmd

    @classmethod
    def header(cls, jobID, nproc, walltime, queue, cuda, email):
        """
        Define header for submission script on Marconi
        """
        nproc = int(nproc)

        # Check sensible number of nodes
        assert nproc % 36 == 0, 'Use a multiple of 36 processes on MARCONI'

        nnode = int(nproc / 36)

        return TEMPLATE.format(jobID=jobID, nnode=nnode, walltime=walltime,
                               email=email)
