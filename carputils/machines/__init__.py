#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Platform-specific functionality such as MPI launcher generation.

To load a particular hardware profile:

>>> from carputils import machines
>>> profile = platforms.get('myplatform')

To add new hardware profiles, duplicate an existing one inside this directory,
adapt it to your needs, and add it to the PLATFORMS dictionary below.
"""

from collections import OrderedDict

from carputils.machines.desktop       import Desktop
from carputils.machines.archer        import Archer
from carputils.machines.archer2       import ARCHER2
from carputils.machines.archer_intel  import ArcherIntel
from carputils.machines.archer_camel  import ArcherCamel
from carputils.machines.curie         import Curie
from carputils.machines.marconi       import Marconi
from carputils.machines.marconi_slurm import *
from carputils.machines.medtronic     import Medtronic
from carputils.machines.mephisto      import Mephisto
from carputils.machines.supermuc      import *
from carputils.machines.vsc2          import VSC2
from carputils.machines.vsc3          import VSC3
from carputils.machines.vsc4          import VSC4
from carputils.machines.vsc5          import VSC5
from carputils.machines.wopr          import Wopr
from carputils.machines.bw            import BW
from carputils.machines.exo           import EXOSCALE

# Use ordered dictionary to store platforms, ensuring that desktop is first and
# others are in alphabetical order
PLATFORMS = OrderedDict()

PLATFORMS['desktop']       = Desktop
PLATFORMS['archer']        = Archer
PLATFORMS['archer2']       = ARCHER2
PLATFORMS['archer_intel']  = ArcherIntel
PLATFORMS['archer_camel']  = ArcherCamel
PLATFORMS['curie']         = Curie
PLATFORMS['marconi']       = Marconi
PLATFORMS['marconi_slurm'] = MarconiSlurm
PLATFORMS['marconi_debug'] = MarconiDebug
PLATFORMS['medtronic']     = Medtronic
PLATFORMS['mephisto']      = Mephisto
PLATFORMS['smuc_f']        = SuperMUCFat
PLATFORMS['smuc_t']        = SuperMUCThin
PLATFORMS['smuc_i']        = SuperMUCInteractive
PLATFORMS['vsc2']          = VSC2
PLATFORMS['vsc3']          = VSC3
PLATFORMS['vsc4']          = VSC4
PLATFORMS['vsc5']          = VSC5
PLATFORMS['wopr']          = Wopr
PLATFORMS['bw']            = BW
PLATFORMS['exoscale']      = EXOSCALE

def list():
    return PLATFORMS.keys()

def get(key):
    return PLATFORMS[key]
