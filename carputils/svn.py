#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

import os
import subprocess
from carputils import settings

MESHES_URL = 'https://carpentry.medunigraz.at/meshes'

def checkout(root, url):
    """
    Check out the bare bones of the repo at the specified location.
    """
    # Make sure there are no trailing slashes
    root = os.path.normpath(root)

    # Make sure parent directory exists
    parent = os.path.dirname(root) 
    if not os.path.exists(parent):
        os.makedirs(parent)

    # Prompt for username
    username = raw_input('Username for {}: '.format(url))

    # Check out bare bones of svn repo
    cmd = ['svn', 'checkout', url,
           '--username', username,
           '--depth',    'empty',
           root]
    subprocess.call(cmd)

def update(root, subpath):
    """
    Update the SVN repo at the specified location.
    """
    cmd = ['svn', 'update', subpath, '--parents']
    subprocess.call(cmd, cwd=root)

def get_mesh(subpath):
    """
    Update the specified mesh directory
    """
    # Get the mesh SVN if not already set up
    if not os.path.exists(settings.config.MESH_DIR):
        checkout(settings.config.MESH_DIR, MESHES_URL)

    # Update the requested subdirectory
    update(settings.config.MESH_DIR, subpath)
