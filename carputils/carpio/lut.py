#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Read binary lookup tables into python numpy arrays
"""

import numpy as np
import gzip
import struct
from carputils.carpio.filelike import FileLikeMixin

INT_SIZE = 4

# Save original file open
fopen = open

class LUTHeader(object):
    """
    Describes a LUT file header.
    """

    def __init__(self, rows, cols, big_endian):
        """
        Args:
            rows: (int) The number of rows in the file
            cols: (int) The number of columns in the file
            big_endian: (bool) Whether the file is big endian or not
        """
        self.rows = int(rows)
        self.cols = int(cols)
        self.big_endian = int(big_endian)

    @classmethod
    def from_binary(cls, binary):
        """
        Generate a LUT header from the binary header data.

        Args:
            binary: (bytes) The binary header from a LUT file
        Returns:
            `LUTHeader` class.
        """

        big_endian, rows, cols = struct.unpack('iii', binary)
        return cls(rows, cols, big_endian)

class LUTFile(FileLikeMixin):
    """
    Read from a LUT file.

    The file may be gzipped - the code automatically determines if this is the
    case from the file extension.
    """

    def __init__(self, filename):
        """
        Args:
            filename: (str) Path of the file to read.
        """
        if filename.endswith('.gz'):
            self._fp = gzip.open(filename, 'rb')
        else:
            self._fp = fopen(filename, 'rb')

        self._hdr_len = INT_SIZE * 3
        self._hdr = None

    def close(self):
        """
        Close the file.
        """
        self._fp.close()

    def header(self):
        """
        Read the file header.

        Returns:
            header: The file header
        """

        if self._hdr is not None:
            return self._hdr

        # Rewind file
        self._fp.seek(0, 0)

        # Get binary data and create header
        binary = self._fp.read(self._hdr_len)
        self._hdr = LUTHeader.from_binary(binary)

        return self._hdr

    def data(self):
        """
        Read the file data as a numpy array.

        Returns:
            data: (numpy.ndarray) The file data contents.
        """
        
        # Move to start of content
        self._fp.seek(self._hdr_len, 0)

        if isinstance(self._fp, gzip.GzipFile):
            # Read remaining file
            byte_str = self._fp.read()
            # Create a numpy array view on content
            data = np.frombuffer(byte_str, dtype='double')

        else:
            # Use more efficient direct read from file
            # This function uses the underlying C FILE pointer directly
            data = np.fromfile(self._fp, dtype='double')

        # Reshape table
        hdr = self.header()
        data = data.reshape(hdr.rows, hdr.cols)

        return data

def open(*args, **kwargs):
    """
    Open a LUT file.

    Convenience method to provide normal python style interface to create a
    file type object.

    Parameters inherited from `LUTFile.__init__()`
    """
    return LUTFile(*args, **kwargs)
