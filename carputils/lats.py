import numpy as np
from os.path import exists

def read_lats_all(fname):

    if exists(fname):
        data = np.genfromtxt( fname, dtype=[('nod_idx','i8'),('lat','f8')], delimiter='\t' )
    else:
        print('LAT activation file %s not found.'%(fname))
        return None

    nod_idx = data['nod_idx']
    lats    = data['lat']

    # sort by nodal indices
    si = np.argsort(nod_idx)

    nod_idx = nod_idx[si]
    lats    = lats[si]

    # rearrange in matrix form
    nodes = np.max(nod_idx) + 1

    # determine max number of repeated nodes
    n_rep = np.zeros((nodes), dtype='int')
    n_rep[:] = -1

    unique, counts = np.unique(nod_idx, return_counts=True)

    for i,nidx in enumerate(unique):
        n_rep[nidx] = counts[i]

    # check activation pattern
    # maximum number of actiations at a single node
    max_acts = int(np.max(n_rep))

    # all nodes were at least activated once
    all_acts = np.min(n_rep) > -1

    # all nodes were activated equally often
    same_mlt = np.all(n_rep == n_rep[0])

    # create matrix nodes x activations
    lat_mat = np.zeros((nodes,max_acts))

    # initalize with lat >> than latest lat detected 
    lat_mat[:] = np.max(lats) + 1000

    # fill lat matrix, nodal index is matrix line, column is the j-th activation time
    lc = 0
    for i in range(nodes):
        mlt = n_rep[i]
        if mlt > -1:
            for j in range(mlt):
                lat_mat[i,j] = lats[lc]
                lc += 1

        # final sort for ascending activation times
        lat_mat[i,:] = sorted(lat_mat[i,:])

    # now that we are sorted, replace not actived nodes with -1 again
    lat_mat[lat_mat>(np.max(lats)+1)] = -1

    return lat_mat

def convert_lats_all(fname, newfname):

    lat_mat = read_lats_all(fname)

    save_lats_all(newfname,lat_mat)

    return


def save_lats_all(fname, lat_mat):

    np.savetxt(fname,lat_mat)

    return


