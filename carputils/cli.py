#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Some argparse extensions implementing some custom behaviour.
"""

import sys
from argparse import ArgumentParser, ArgumentTypeError

POSTPROCESS_MODES = ('phie', 'optic', 'activation',
                     'axial', 'filament',
                     'efield', 'mechanics')

class CallbacksMixin(object):

    def __init__(self, *args, **kwargs):
        super(CallbacksMixin, self).__init__(*args, **kwargs)
        self._callbacks = {}

    def add_argument(self, *args, **kwargs):

        callback = kwargs.pop('callback', None)

        arg = super(CallbacksMixin, self).add_argument(*args, **kwargs)

        if callback is not None:
            self._callbacks[arg.dest] = callback

        return arg

    def parse_args(self, args=None, namespace=None):
        """
        Overridden to inject args into settings module when parsed.
        """

        args = super(CallbacksMixin, self).parse_args(args, namespace)

        for dest, callback in self._callbacks.items():
            setattr(args, dest, callback(getattr(args, dest)))

        return args

class GroupAccessMixin(object):

    def get_argument_group(self, title):

        # Check for exact matches first
        for group in self._action_groups:
            if group.title == title:
                return group

        # Then check for starting matches
        for group in self._action_groups:
            if group.title.startswith(title):
                return group

        raise ValueError('no group titled "{}"'.format(title))

    def __getitem__(self, title):
        try:
            self.get_argument_group(title)
        except ValueError as err:
            raise KeyError(err.message)

class CarputilsArgumentParser(CallbacksMixin, GroupAccessMixin, ArgumentParser):

    def error(self, message):
        """
        To make error message retrievable from SystemExit exception.
        """
        self.print_usage(sys.stderr)
        self.exit('error: {}'.format(message))

def bool(string):
    """
    Bool checker for use as ArgumentParser type.
    """

    choices = {'true':  True,
               'false': False,
               'on':    True,
               'off':   False}
    ambiguous = ['o']

    # First check for partial case-insensitive match with string choices
    if string not in ambiguous:
        for key, ret in choices.items():
            if string.lower() == key[:len(string)]:
                return ret

    # Check for ints
    try:
        return bool(int(string))
    except:
        msg = 'must be int or partial case-insensitive match of one of '
        msg += ', '.join(["{}".format(k) for k in choices.keys()])
        raise ArgumentTypeError(msg)

def time_tuple(string):
    """
    Time tuple converter for use as ArgumentParser type.
    """
    try:
        vals = [int(v) for v in string.split(':')]
        return (vals[0], vals[1], vals[2])
    except:
        msg = 'must be of format HH:MM:SS'
        raise ArgumentTypeError(msg)

def range_tuple(string):
    """
    Range tuple converter for use as ArgumentParser type.
    """
    try:
        vals = [int(v) for v in string.split(':')]
        return (vals[0], vals[1], vals[2])
    except:
        msg = 'must be of format min:max:num'
        raise ArgumentTypeError(msg)

def bitflags_callback(choices):
    """
    Set up a callback to convert provided options into an integer value.

    Parameters
    ----------
    choices : list or dict
        The strings that may be used as command line arguments - if a dict, the
        strings must be the keys and the values the corresponding integer values
        - if a list, they will be mapped to powers of 2 in order

    Returns
    -------
    callable
        A function which can be passed as the callback argument of
        CarputilsArgumentParser.add_argument().
    """

    try:
        choices = dict(choices)
    except ValueError:
        # Default to sequential powers of two
        choices = {key: 2**i for i, key in enumerate(choices)}

    def callback(values):
        if values is None:
            return None

        return sum([choices[v] for v in values])

    return callback
