#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Functions to build checks and assist in the creation of new checks.

Some functions to quantify the error between the generated and reference
solutions. At present, these checks always take two arguments, the reference
and test files, which then compute some error and return it. A more complex
return value could be used when adding checks to your tests, but then an
appropriate function would need to be passed as the check's tolerance test.
"""

import numpy as np

__all__ = ['max_error',
           'error_norm']

def max_error(array1, array2):
    return np.max(np.abs(array1 - array2))

def error_norm(array1, array2):
    return np.sqrt(np.sum((array1 - array2)**2))
