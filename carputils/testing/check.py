#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

import os
import sys
import pickle
import base64

import numpy as np

from .. import carpio
from .exception import CARPTestError, SerialisableError

def get_reader(filename):
    """ get reader """
    for ext, reader in carpio.READERS.items():
        if isinstance(filename, bytes):
            filename = filename.decode('utf-8')
        if filename.endswith(ext):
            return reader
    raise CARPTestError('no reader found for file {}'.format(filename))

def has_reader(filename):
    """ check for reader """
    try:
        get_reader(filename)
    except CARPTestError:
        return False
    else:
        return True

def load_data(filename, loc_msg):
    """ load data """

    is_ok = lambda path: os.path.exists(path) and has_reader(path)

    # Allow switches in gzipped-ness
    if os.path.exists(filename):
        pass
    elif filename.endswith('.gz') and is_ok(filename[:-3]):
        filename = filename[:-3]
    elif is_ok(filename + '.gz'):
        filename = filename + '.gz'
    else:
        # File/dir not found, raise CARPTestError instead
        tpl = 'Required file missing in {} (path {})'
        raise CARPTestError(tpl.format(loc_msg, filename))

    reader = get_reader(filename)

    with reader(filename) as fp:
        data = fp.data()

    if isinstance(data, (tuple)):
        return data[0]
    else:
        return data

def function_pickle(func):
    """
    Just store the function name if an unpicklable function.
    """
    try:
        data = base64.b64encode(pickle.dumps(func)).decode('ascii')
    except (pickle.PickleError, pickle.PicklingError, AttributeError,
            EOFError, ImportError, IndexError):
        data = pickle.dumps(func.__name__)
    return data

class FakeFunction(object):
    """
    Simple callable object to act as placeholder for unpicklable functions.
    """
    def __init__(self, name):
        self.__name__ = name
    def __call__(self, *args, **kwargs):
        pass

def function_depickle(string):
    """
    Depickle a function, making a FakeFunction object for unpicklable ones.
    """
    obj = pickle.loads(base64.b64decode(string))
    if callable(obj):
        return obj
    return FakeFunction(obj)

class CheckResult(object):

    def __init__(self, check, success, error=None, execerror=None):
        self.check = check
        self.success = bool(success)
        self.error = None if error is None else float(error)
        self.execerror = execerror

    def serialise(self):
        data = {'check': self.check.serialise(),
                'success': self.success,
                'error': self.error,
                'execerror': None if self.execerror is None
                             else self.execerror.serialise()}
        return data

    @classmethod
    def deserialise(cls, data):

        if 'execerror' in data:
            execerror = SerialisableError.deserialise(data['execerror'])
        else:
            # Backwards compatability
            exception = pickle.loads(data['exception'])
            backtrace = data['backtrace']
            execerror = SerialisableError(exception, backtrace)

        return cls(Check.deserialise(data['check']),
                   data['success'],
                   data['error'],
                   execerror)

    def report(self, pad=0):

        if self.success:
            state = 'PASS'
        elif self.execerror is not None:
            state = 'ERROR'
        else:
            state = 'FAIL'

        report = '{}{} {}'.format(' ' * pad, state, str(self.check))

        if self.error is not None:
            report += ': {}'.format(self.error)

        if self.execerror is not None:
            report += ':\n'
            if hasattr(self.execerror, '__class__'):
                report += self.execerror.report()
            else:
                report += self.execerror(pad + 6)

        return report

class Check(object):

    def serialise(self):
        return {'class': self.__class__.__name__}

    @staticmethod
    def deserialise(data):
        cls = getattr(sys.modules[__name__], data['class'])
        return cls.deserialise(data)

    def __call__(self, *args, **kwargs):
        try:
            return self.check(*args, **kwargs)
        except Exception as exc:
            backtrace = sys.exc_info()[2]
            error = SerialisableError(exc, backtrace)
            return CheckResult(self, False, execerror=error)

class ReferenceComparisonCheck(Check):

    def __init__(self, filename, cost, tolerance):

        # Check that a reader exists for this file
        get_reader(filename)
        # Check that the cost is callable
        assert callable(cost), 'cost must be a function'

        self.filename = filename
        self.cost = cost
        self.tolerance = float(tolerance)

    def serialise(self):
        data = super(ReferenceComparisonCheck, self).serialise()
        data['filename'] = self.filename
        data['cost'] = function_pickle(self.cost)
        data['tolerance'] = self.tolerance
        return data

    @classmethod
    def deserialise(cls, data):
        return cls(data['filename'], function_depickle(data['cost']),
                   data['tolerance'])

    def __str__(self):
        return '{}({})'.format(self.cost.__name__, self.filename)

    def summary(self):
        return 'Compare against stored reference: {}'.format(str(self))

    def check(self, testdir=None, refdir=None, **kwargs):

        testdata = load_data(os.path.join(testdir, self.filename),
                             'simulation output')
        refdata = load_data(os.path.join(refdir, self.filename),
                            'reference directory')

        cost = self.cost(refdata.flatten(), testdata.flatten())

        success = cost < self.tolerance

        return CheckResult(self, success, cost)

class AnalyticComparisonCheck(Check):

    def __init__(self, filename, analytic, cost, tolerance):

        # Check that a reader exists for this file
        get_reader(filename)
        # Check that the cost and analytic solution are callable
        assert callable(analytic), 'analytic must be a function'
        assert callable(cost), 'cost must be a function'

        self.filename = filename
        self.analytic = analytic
        self.cost = cost
        self.tolerance = float(tolerance)

    def serialise(self):
        data = super(AnalyticComparisonCheck, self).serialise()
        data['filename'] = self.filename
        data['analytic'] = function_pickle(self.analytic)
        data['cost'] = function_pickle(self.cost)
        data['tolerance'] = self.tolerance
        return data

    @classmethod
    def deserialise(cls, data):
        return cls(data['filename'], function_depickle(data['analytic']),
                   function_depickle(data['cost']), data['tolerance'])

    def __str__(self):
        return '{}({})'.format(self.cost.__name__, self.filename)

    def summary(self):
        tpl = 'Compare against analytic solution defined by {}(): {}'
        return tpl.format(self.analytic.__name__, str(self))

    def check(self, testdir=None, **kwargs):
        testdata = load_data(os.path.join(testdir, self.filename),
                             'simulation output')
        refdata = np.array(self.analytic())

        cost = self.cost(refdata.flatten(), testdata.flatten())

        success = cost < self.tolerance

        return CheckResult(self, success, cost)

class CustomCheck(Check):

    def __init__(self, check, *extraargs, **extrakwargs):

        # Check that the check function is indeed callable
        assert callable(check), 'check must be a function'

        self.check_function = check
        self.extraargs = extraargs
        self.extrakwargs = extrakwargs

    def serialise(self):
        data = super(CustomCheck, self).serialise()
        data['check_function'] = function_pickle(self.check_function)
        data['extraargs'] = self.extraargs
        return data

    @classmethod
    def deserialise(cls, data):
        return cls(function_depickle(data['check_function']),
                   *(data['extraargs']))

    def __str__(self):
        return '{}({})'.format(self.check_function.__name__,
                               ', '.join([str(a) for a in self.extraargs]))

    def summary(self):
        return 'Custom check: {}'.format(str(self))

    def check(self, testdir=None, **kwargs):
        success = self.check_function(testdir, *(self.extraargs), **(self.extrakwargs))
        return CheckResult(self, success)
