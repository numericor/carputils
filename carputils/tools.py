#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the
# Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Functions for generating and executing openCARP command lines and other tools.
"""

from __future__ import print_function, division, unicode_literals
import sys
import datetime
import argparse
from functools import wraps
from carputils import model
from carputils import settings
from carputils import cli
from carputils import clean
from carputils import machines
from carputils.job import Job
from carputils.job.optionlist import OptionList
from carputils import svn
from carputils import tempdir

OVERWRITE_BEHAVIOUR = tuple(clean.OVERWRITE_MODES.keys())

maxnp = None

class MaxNPAction(argparse.Action):
    """ Class to check that the maximum number of processes is not exceeded
    """
    def __call__(self, parser, namespace, values, option_string=None):
        if maxnp is not None and values > maxnp:
            values = maxnp
            print('Maximum number of processes ({}) exceeded\n'.format(maxnp))
            #parser.error("Maximum number of processes ({}) exceeded\n".format(maxnp))

        setattr(namespace, self.dest, values)

class PrintAction(argparse.Action):
    """ Class to print a unformatted message like '--help' and exit
    """
    def __init__(self, option_strings, dest=argparse.SUPPRESS,
                 default=argparse.SUPPRESS, help=None, message=None):
        super(PrintAction, self).__init__(option_strings=option_strings,
                                          dest=dest, default=default, nargs=0, help=help)
        self._message = message

    def __call__(self, parser, namespace, values, option_string=None):
        print(self._message)
        parser.exit()

def basic_parser(showdefault=True, overwrite_behaviour=OVERWRITE_BEHAVIOUR[0], formatter_cls=None):
    assert overwrite_behaviour in OVERWRITE_BEHAVIOUR

    parser = cli.CarputilsArgumentParser()
    if showdefault:
        parser.formatter_class = argparse.ArgumentDefaultsHelpFormatter
    if formatter_cls is None:
        parser.formatter_class = argparse.ArgumentDefaultsHelpFormatter
    else:
        # you may use argparse.RawTextHelpFormatter which accepts line breaks in the parameter help
        parser.formatter_class = formatter_cls

    exe = parser.add_argument_group('execution options')
    try:
        global maxnp
        maxnp = settings.config.MAX_NP
        exe.add_argument('--np',
                         type=int, default=1,
                         action=MaxNPAction,
                         help='number of processes (max={})'.format(maxnp))
    except:
        exe.add_argument('--np',
                         type=int, default=1,
                         help='number of processes')

    exe.add_argument('--dry-run',
                     dest='dry',
                     action='store_true',
                     help='show command line without running the test')

    exe.add_argument('--platform',
                     default=settings.config.PLATFORM,
                     choices=machines.list(),
                     help='pick a hardware profile from available platforms')

    output = parser.add_argument_group('output options')

    output.add_argument('--ID',
                        help='manually specify the job ID (output directory)')

    output.add_argument('--suffix',
                        help='add a suffix to the job ID (output directory)')

    output.add_argument('--overwrite-behaviour',
                        choices=OVERWRITE_BEHAVIOUR,
                        default=overwrite_behaviour,
                        help='behaviour when output directory already exists')

    output.add_argument('--silent',
                        action='store_const',
                        default=settings.config.SILENT,
                        const=not settings.config.SILENT,
                        help='toggle silent output')

    return parser

def standard_parser(showdefault=True):
    """
    Generate a standard argument parser for collection of common options.

    Returns : ArgumentParser
        The parser object from the python standard library argparse module. See
        `the online documentation <https://docs.python.org/2.7/library/argparse.html>`_
        for information on adding additional options to this parser in a run
        script. To actually parse the command line, use the ``parse_args``
        method of the returned parser object.
    """

    # Use subclass which automatically populates execs
    parser = cli.CarputilsArgumentParser()
    if showdefault:
        parser.formatter_class = argparse.ArgumentDefaultsHelpFormatter

    exe = parser.add_argument_group('execution options')

    try:
        global maxnp
        maxnp = settings.config.MAX_NP
        exe.add_argument('--np',
                         type=int, default=1,
                         action=MaxNPAction,
                         help='number of processes (max={})'.format(maxnp))
    except:
        exe.add_argument('--np',
                         type=int, default=1,
                         help='number of processes')

    exe.add_argument('--np-job',
                     type=int, default=0,
                     help='number of processes per job (for multiprocessing)')

    exe.add_argument('--tpp',
                     type=int,
                     help='threads per process')

    exe.add_argument('--runtime',
                     type=cli.time_tuple, metavar='HH:MM:SS',
                     help='max job runtime')

    builds = tuple(settings.config.CARP_EXE_DIR.keys())
    BUILD = builds[0] if settings.config.BUILD is None else settings.config.BUILD # allow fallback value
    exe.add_argument('--build',
                     type=str, default=BUILD,
                     choices=builds,
                     help='openCARP build to use')

    exe.add_argument('--flavor', '--flv',
                     dest='flv',
                     type=str, default=settings.config.FLAVOR,
                     choices=settings.FLAVORS,
                     help='openCARP flavor')

    exe.add_argument('--platform',
                     type=str, default=settings.config.PLATFORM,
                     choices=machines.list(),
                     help='pick a hardware profile from available platforms')

    exe.add_argument('--queue',
                     help='select a queue to submit job to (batch systems only)')

    exe.add_argument('--vectorized-fe',
                     type=cli.bool,
                     help='vectorized FE assembly (default: on with '
                          'FEMLIB_CUDA, off otherwise)')

    exe.add_argument('--dry-run',
                     dest='dry',
                     action='store_true',
                     help='show command line without running the test')

    exe.add_argument('--checkpoint',
                     type=int, default=0,
                     help='Set checkpoint interval.')

    exe.add_argument('--restore',
                     type=str, default='',
                     help='Restart from given checkpoint file')

    exe.add_argument('--generate-parfile',
                     type=str, default=None,
                     help='Generate parameter file with all chosen options')

    exe.add_argument('--polling-param',
                     nargs='+', type=str, default=None,
                     help='Polling parameter')

    exe.add_argument('--polling-range',
                     nargs='+', default=None,
                     type=cli.range_tuple, metavar='min:max:num',
                     help='Define polling parameter range')

    exe.add_argument('--polling-file',
                     type=str, default=None,
                     help='File including polling data for parameter sweeps')

    exe.add_argument('--sampling-type',
                     type=str, default='linear',
                     choices=['linear', 'geometric', 'lhs'],
                     help='Sampling type for parameter sweeps. Choose between '
                          '\"linear\", \"geometric\" and latin hypercube (\"lhs\") sampling')

    if hasattr(model.activetension, 'summary'):
        parser.add_argument('--stress-models',
                            action=PrintAction,
                            message=model.activetension.summary(),
                            help='print stress-models and exit')

    if hasattr(model.mechanics, 'summary'):
        parser.add_argument('--material-models',
                            action=PrintAction,
                            message=model.mechanics.summary(),
                            help='print material-models and exit')

    if hasattr(model.ionic, 'summary'):
        parser.add_argument('--ionic-models',
                            action=PrintAction,
                            message=model.ionic.summary(),
                            help='print ionic-models and exit')

    parser.add_argument('--CARP-opts',
                        action='append',
                        help='arbitrary openCARP options to append to command')

    parser.add_argument('--postprocess',
                        nargs='+',
                        choices=cli.POSTPROCESS_MODES,
                        callback=cli.bitflags_callback(cli.POSTPROCESS_MODES),
                        help='postprocessing mode(s) to execute')

    debug = parser.add_argument_group('debugging and profiling')

    debug.add_argument('--gdb',
                       nargs='*', type=int, metavar='PROC',
                       help='start (optionally specified) processes in gdb')

    debug.add_argument('--lldb',
                       nargs='*', type=int, metavar='PROC',
                       help='start (optionally specified) processes in lldb')

    debug.add_argument('--ddd',
                       nargs='*', type=int, metavar='PROC',
                       help='start (optionally specified) processes in ddd')

    debug.add_argument('--ddt',
                       action='store_true',
                       help='start in Allinea ddt debugger')

    debug.add_argument('--valgrind',
                       nargs='?', const=True, metavar='OUTFILE',
                       help='start in valgrind mode, use in conjunction with '
                            '--gdb for interactive mode')

    debug.add_argument('--valgrind-options',
                       nargs='*', metavar='OPT=VAL',
                       default=['leak-check=full', 'track-origins=yes'],
                       help='specify valgrind CLI options, without preceding '
                            '\'--\'')

    debug.add_argument('--map',
                       action='store_true',
                       help='start using Allinea map profiler')

    debug.add_argument('--scalasca',
                       action='store_true',
                       help='start in scalasca profiling mode')

    output = parser.add_argument_group('output options')

    output.add_argument('--ID',
                        type=str,
                        help='manually specify the job ID (output directory)')

    output.add_argument('--suffix',
                        help='add a suffix to the job ID (output directory)')

    output.add_argument('--overwrite-behaviour',
                        choices=OVERWRITE_BEHAVIOUR,
                        type=str, default=OVERWRITE_BEHAVIOUR[0],
                        help='behaviour when output directory already exists')

    output.add_argument('--silent',
                        action='store_const',
                        default=settings.config.SILENT,
                        const=not settings.config.SILENT,
                        help='toggle silent output')

    output.add_argument('--visualize',
                        action='store_const',
                        default=settings.config.VISUALIZE,
                        const=not settings.config.VISUALIZE,
                        help='toggle test results visualisation')

    mechanics = parser.add_argument_group('mechanics options')

    mechanics.add_argument('--mech-element',
                            default=settings.MECH_ELEMENT[0],
                            choices=settings.MECH_ELEMENT,
                            help='CARP default mechanics finite element')

    mechanics.add_argument('--mech-with-inertia',
                            action='store_const',
                            default=False,
                            const=True,
                            help='toggle mechanics generalized alpha integrator')

    exe.add_argument('--webGUI',
                        action='store_true',
                        help='run the example for visualization in the web GUI')
    return parser


def basic_job_id(args):
    """ Generate default job ID
    """
    today = datetime.date.today()
    return '{}_np{}'.format(today.isoformat(), args.np)

def basicexample(parser=basic_parser,
                 job_id=basic_job_id,
                 summary=True,
                 temp=False):
    """
    Generate a decorator for openCARP example run function.

    The decorator sets up sensible defaults for argv and sequence for an openCARP
    example's run function, and executes the command sequence on exit, where
    required.
    """

    def decorator(run):
        """ Decorator for openCARP example run function.
        """
        @wraps(run)
        def wrapped(argv=sys.argv[1:], job=None, outdir=None):

            # Generate parser
            prs = parser()

            # Parse command line
            args = prs.parse_args(argv)

            # Assign to settings
            settings.cli = args

            # Print settings summary
            if not args.silent and summary:
                print(settings.summary)

            # Generate job ID
            if temp:
                gen_id = tempdir.tempdir()
            elif outdir is None:
                gen_id = job_id(args) if args.ID is None else args.ID
                # Add suffix
                if args.suffix is not None:
                    gen_id += '_' + args.suffix
            else:
                # Override
                gen_id = outdir

            # Create a new empty command sequence when none provided
            if job is None:
                job = Job(gen_id)
                run_on_exit = True
            else:
                run_on_exit = False

            # Prevent overwrite of existing output
            if not args.dry:
                clean.overwrite_check(gen_id, args.overwrite_behaviour)

            # Make sure directory exists
            job.mkdir(job.ID, parents=True, delayed=True)

            # Run the example
            ret = run(args, job)

            # Submit the job on batch systems
            if run_on_exit and settings.platform.BATCH:
                job.submit(argv=argv)
            # return the return value of the actual command
            return ret

        return wrapped
    return decorator

def carp_job_id(args):
    """ Generate default job ID
    """
    today = datetime.date.today()
    return '{}_{}_np{}'.format(today.isoformat(), args.flv, args.np)

def carpexample(parser=standard_parser,
                job_id=carp_job_id,
                meshdir=None,
                clean_pattern='^(\d{4}-\d{2}-\d{2})|(mesh)',
                simple=False,
                summary=True,
                temp=False):
    """
    Generate a decorator for openCARP example run function.

    The decorator sets up sensible defaults for argv and sequence for a
    openCARP example's run function, and executes the command sequence on
    exit, where required.
    """

    def decorator(run):
        """ Decorator for openCARP example run function.
        """
        @wraps(run)
        def wrapped(argv=sys.argv[1:], job=None, outdir=None):

            # Generate parser
            prs = parser()

            # Add any additional arguments to the command line
            if clean is not None:
                prs.add_argument('--clean',
                                 action='store_true',
                                 help='clean generated output files and '
                                      'directories')
            if meshdir is not None:
                prs.add_argument('--get-mesh',
                                 action='store_true',
                                 help='get meshes for this example from SVN')

            # Parse command line
            args = prs.parse_args(argv)

            # Assign to settings
            settings.cli = args

            # check arguments
            if hasattr(settings.cli, 'np_job') and hasattr(settings.cli, 'polling_file'):
                if settings.cli.polling_file is None and settings.cli.np_job > 0:
                    sys.exit("Argument \"--np-job\" can only be used with parameter "
                             "sweeps, i.e. in combination with \"--polling-file\"")

            # Print settings summary
            if not args.silent and summary:
                print(settings.summary)

            # Clean existing output matching regular expression
            if clean_pattern is not None and args.clean:
                clean.clean_directory(regex=clean_pattern)
                sys.exit(0)

            # Retrieve meshes
            if meshdir is not None and args.get_mesh:
                svn.get_mesh(meshdir)
                sys.exit(0)

            # Generate job ID
            if temp:
                gen_id = tempdir.tempdir()
            elif outdir is None:
                gen_id = job_id(args) if args.ID is None else args.ID
                # Add suffix
                if args.suffix is not None:
                    gen_id += '_' + args.suffix
            else:
                # Override
                gen_id = outdir

            # Create a new empty command sequence when none provided
            if job is None:
                job = Job(gen_id)
                run_on_exit = True
            else:
                run_on_exit = False

            # Prevent overwrite of existing output
            if not args.dry and not simple:
                if not (hasattr(args, 'postprocess') and args.postprocess):
                    clean.overwrite_check(gen_id, args.overwrite_behaviour)

            # Make sure directory exists
            if not simple:
                job.mkdir(job.ID, parents=True, delayed=True)

            # Run the example
            ret = run(args, job)

            # Submit the job on batch systems
            if run_on_exit and settings.platform.BATCH:
                job.submit(argv=argv)
            # return the return value of the actual command
            return ret

        return wrapped
    return decorator

def carp_cmd(parfile=None, mech_symmetric=False):
    """
    Construct the basic openCARP command line.

    Constructs a list of command line arguments. This will automatically
    include the loading of the correct options for the specified solvers.

    Args:
        parfile : str, optional
        The openCARP parameter file to be used, if any.

    Returns:
        list of str
           The initial openCARP command line.
    """

    cmd = OptionList([settings.execs.CARP])

    if parfile is not None:
        cmd += ['+F', parfile]

    cmd += settings.solver(mech_symmetric=mech_symmetric).args()

    if settings.cli.silent:
        cmd += ['-output_level', 0]

    if settings.cli.vectorized_fe is not None:
        # Use command line option
        cmd += ['-vectorized_fe', int(settings.cli.vectorized_fe)]
    elif settings.makevars.FEMLIB_CUDA == 1:
        # Default to vectorized assembly on GPU
        cmd += ['-vectorized_fe', int(settings.makevars.FEMLIB_CUDA)]

    if settings.makevars.WITH_MECH:
        cmd += ['-mech_finite_element',
                settings.MECH_ELEMENT.index(settings.cli.mech_element)]
        cmd += ['-mech_activate_inertia', int(settings.cli.mech_with_inertia)]

    if settings.cli.postprocess is not None:
        # Force these options to be at the end to avoid override of the
        # experiment flag
        cmd.at_end(['-experiment', 4,
                    '-simID', settings.cli.ID,
                    '-ppID', 'postprocess',
                    '-post_processing_opts', settings.cli.postprocess])

    # set checkpointing interval
    if settings.cli.checkpoint > 0:
        cmd += ['-chkpt_intv', settings.cli.checkpoint]

    # restore mechanics from checkpoint file if given
    if settings.cli.restore != '':
        cmd += ['-start_statef', settings.cli.restore]

    return cmd


def list_to_string(pl):
    return ','.join(map(str, pl))


def gen_physics_opts(ExtraTags=None, IntraTags=None, EikonalTags=None, MechTags=None, FluidTags=None):
    num_phys = 0
    phys_opts = []

    if IntraTags is not None:
        preg = '-phys_region[{}]'.format(num_phys)
        phys_opts += [preg+'.name',    '"Intracellular domain"',
                      preg+'.ptype',   0,
                      preg+'.num_IDs', len(IntraTags)]
        for i, tag in enumerate(IntraTags):
            phys_opts += [preg+'.ID[{}]'.format(i), tag]
        num_phys += 1

    if ExtraTags is not None:
        preg = '-phys_region[{}]'.format(num_phys)
        phys_opts += [preg+'.name',    '"Extracellular domain"',
                      preg+'.ptype',   1,
                      preg+'.num_IDs', len(ExtraTags)]

        for i, tag in enumerate(ExtraTags):
            phys_opts += [preg+'.ID[{}]'.format(i), tag]
        num_phys += 1

    if EikonalTags is not None:
        preg = '-phys_region[{}]'.format(num_phys)
        phys_opts += [preg+'.name',    '"Eikonal domain"',
                      preg+'.ptype',   2,
                      preg+'.num_IDs', len(EikonalTags)]
        for i, tag in enumerate(EikonalTags):
            phys_opts += [preg+'.ID[{}]'.format(i), tag]
        num_phys += 1

    if MechTags is not None:
        preg = '-phys_region[{}]'.format(num_phys)
        phys_opts += [preg+'.name',    '"Mechanics domain"',
                      preg+'.ptype',   3,
                      preg+'.num_IDs', len(MechTags)]
        for i, tag in enumerate(MechTags):
            phys_opts += [preg+'.ID[{}]'.format(i), tag]
        num_phys += 1

    if FluidTags is not None:
        preg = '-phys_region[{}]'.format(num_phys)
        phys_opts += [preg+'.name',    '"Fluid domain"',
                      preg+'.ptype',   4,
                      preg+'.num_IDs', len(FluidTags)]
        for i, tag in enumerate(FluidTags):
            phys_opts += [preg+'.ID[{}]'.format(i), tag]
        num_phys += 1

    phys_opts = ['-num_phys_regions', num_phys] + phys_opts

    return phys_opts


