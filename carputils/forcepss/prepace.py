#!/usr/bin/env python3

r"""
"""
import os, sys
from datetime import date
from json import load, dump
#from shutil import copy
#import numpy as np

#from carputils import settings
from carputils import tools
#from carputils import model
#from carputils import mesh
from carputils.cep4cms import j2carp as j2c


EXAMPLE_DESCRIPTIVE_NAME = 'Prepacing and validation of tissue/organ models'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>'
EXAMPLE_DIR = os.path.dirname(__file__)
GUIinclude = False


# ------PREPACE -------------------------------------------------------

# return mesh extension as function of run
def mesh_ext(rmode):

    if rmode == 'ek':
        ext = '_ek'
    else:
        ext = '_i'

    return ext


def setup_lats_prepacing(pp_bcl, pp_cycs, lat_file):

    # compute lats sequence based on S1_elecs

    # assemble lats prepacing structure
    lats_pp = ['-prepacing_bcl',    pp_bcl,
               '-prepacing_beats',  pp_cycs,
               '-prepacing_lats',   lat_file]

    return lats_pp


# helper function to select stimulus mode
# voltage is more robust, but currently ignored in ek and re runs
# decide on stimulus type as function of propagation mode
def stim_mode(prop):

    # by defaul in rd runs, use voltage clamp to achieve robust capture
    s_type = 9
    s_strength = -30

    # eikonal mediated propagation picks up only transmembrane stimuli
    if prop == 'ek' or prop == 're':
        # eikonal-mediated triggers on transmembrane current stimuli
        s_strength = 250
        s_type     = 0

    return s_type, s_strength


# prepace tissue to generate initial state
def setup_prepace(meshname, cnf, fncs, elecs, pp_prtcl, rmode, use_split=True, split_file=None, tags_file=None):

    # unpack experimental settings
    pp_bcl    = pp_prtcl.get('bcl')
    pp_cyc    = pp_prtcl.get('cycles')
    pp_elecs  = pp_prtcl.get('electrodes')
    pp_rtimes = pp_prtcl.get('rel-timings')

    # check whether all prepacing electrodes are defined
    for e in pp_elecs:
        if e not in elecs:
            print('\n\nElectrode {} required for prepacing, but not defined.\n'.format(e))
            sys.exit()

    # decide on stimulus type as function of propagation mode
    s_type, s_strength = stim_mode(rmode)

    # assign protocols to all electrodes used for pre-pacing
    stims = []
    for i, e in enumerate(pp_elecs):
        elec, idx_hd = j2c.elec_sec_to_carp(elecs, i, e)

        ptcl = [ '-stim[{}].ptcl.start'.format(i),     pp_rtimes[i],
                 '-stim[{}].ptcl.duration'.format(i),  2,
                 '-stim[{}].pulse.strength'.format(i), s_strength, 
                 '-stim[{}].crct.type'.format(i),      s_type ]

        stims += elec + ptcl
    
    num_stims = len(pp_elecs)
    if rmode == 're':
        stims += ['-stim[{}].crct.type'.format(num_stims), 8 ]
        num_stims = num_stims + 1

    stims = ['-num_stim', num_stims] + stims

    # set up simulation command
    cmd  = tools.carp_cmd()
    cmd += stims

    # setup mesh
    #splt = True # use split-list if available
    cmd += j2c.setup_mesh(meshname, use_split, split_file, tags_file)

    # set up tissue properites
    cmd += j2c.setup_tissue(cnf, fncs, rmode)

    return cmd


# setup mesh-related command options
#def setup_mesh(meshname, splt):
#
#    cmd = []
#
#    # use split information if available
#    if splt:
#        splt_file = meshname + '.splt'
#        if os.path.exists(splt_file):
#            cmd += [ '-use_splitting', 1 ]
#        else:
#            print('Splitting selected, but split file {} does not exist!'.format(splt_file))
#            print('Proceed without splitting.')
#
#    cmd += [ '-meshname', meshname ]
#
#    return cmd


# create mesh ID
def mesh_ID(meshname):
    meshID = os.path.basename(meshname)

    return meshID


def sim_ID(prefix, pp_prtcl, caseID):

    PCL = pp_prtcl.get('bcl')
    PCLdig = '%.1f'%(PCL)
 
    # determine electrodes involved in pre-pacing
    pp_elecs = ''
    for e in pp_prtcl.get('electrodes'):
        pp_elecs += '_{}'.format(e)

    ID   = '{}{}_PCL_{}_ms_{}'.format(prefix, pp_elecs, PCL, caseID)
    
    return ID 


# generate lat vector bei running eikonal or rd
def generate_lat(meshname, cnf, exps, elecs, pp_prtcl, gen_lat, lats_file, visual,
                 use_split=True, split_file=None, tags_file=None):

    fncs = exps.get('functions')
    cmd_lat = setup_prepace(meshname, cnf, fncs, elecs, pp_prtcl, gen_lat,
                            use_split=use_split, split_file=split_file, tags_file=tags_file)

    # total activation time must be shorter than bcl
    # use bcl as an upper limit for duration of simulation
    # in the rd case it would be more efficient if simulator could detect total activation time
    dur = pp_prtcl.get('bcl')
    PCL = '%.1f'%(dur)
    
    cmd_lat += [ '-tend', dur]

    # determine electrodes involved in pre-pacing
    pp_elecs = ''
    for e in pp_prtcl.get('electrodes'):
        pp_elecs += '_{}'.format(e)

    if gen_lat == 'ek':
        
        # generate lats by eikonal solve
        simID = sim_ID('ek_lat', pp_prtcl, mesh_ID(meshname))
        grdout  = 'ek'
        cmd_lat+= [ '-simID', simID, '-experiment', 6 ]

        # lat vector handle
        tmp_lats = os.path.join(simID, 'vm_act_seq.dat')

    if gen_lat == 'rd':

        # generate lats by rd solve
        simID    = sim_ID('rd_lat', pp_prtcl, mesh_ID(meshname))
        grdout   = 'i'
        lats_idx =  0
        lats     = [ '-num_LATs', 1 ] + j2c.ek_lat_detect(lats_idx, threshold=-20) + j2c.sentinel(lats_idx, 0, 20)
        cmd_lat += ['-simID', simID, '-experiment', 0, '-spacedt', 1. ] + lats

        # lat vector handle
        tmp_lats = os.path.join(simID, 'init_acts_vm_act_seq.dat-thresh.dat')

        slv = exps.get('solver')
        cmd_lat += j2c.add_slv_settings(slv)

        # before running, check all specified initial states are there
        if not j2c.check_initial_state_vectors(fncs, True):
            # some initial state vectors are missing
            print('\nMissing initial state vectors, exiting!\n\n')
            sys.exit()

    # prepare visualization
    if visual:
        vis = [ '-gridout_{}'.format(grdout), 3,
                '-spacedt', 1 ]
        cmd_lat += vis

    # generate activation sequcne
    return cmd_lat, tmp_lats, simID


# create carp checkpoint array based on tsav's
def setup_tsavs(tsav, tsav_basename):

     # checkpoint timings
     chkpt = [ '-num_tsav', len(tsav) ]
     for i,t in enumerate(tsav):
        chkpt += [ '-tsav[{}]'.format(i), t ]
     
     chkpt += [ '-write_statef', tsav_basename]

     return chkpt


# actual prepacing
def prepace(meshname, cnf, fncs, elecs, pp_prtcl, mode, lats_file, rmode, visual,
            use_split=True, split_file=None, tags_file=None):

    # set up overall command line
    cmd = setup_prepace(meshname, cnf, fncs, elecs, pp_prtcl, rmode, use_split, split_file, tags_file)

    # unpack prepacing protocol
    pp_bcl   = pp_prtcl.get('bcl')
    pp_cyc   = pp_prtcl.get('cycles')
    pp_elecs = pp_prtcl.get('electrodes')

    # earliest stimulus timing
    stim_trg_timings = pp_prtcl.get('rel-timings')
    start = min(stim_trg_timings)
    stop  = max(stim_trg_timings)

    # specialize command for different prepacing strategies
    chkpt   = []
    lats_pp = []
    simID   = None
    t_chks  = [0]
    chkptf  = 'prepaced'
    stims   = []
    if mode == 'off':

        # don't pre-pace, use only the pre-paced initial state vectors
        simID = 'prepace_off'

        # as we write only an initial checkpoint, no actual pre-pacing
        # we turn off all stimuli active in the pre-pacing protocol in this case
        for i, e in enumerate(pp_elecs):
            stims += ['-stim[{}].ptcl.start'.format(i),    0,
                      '-stim[{}].ptcl.npls'.format(i),     0,
                      '-stim[{}].ptcl.duration'.format(i), 0 ]

        cmd += [ '-tend', 1., '-spacedt', 1. ]

    elif mode == 'lat':

        # use fast lats-based pre-pacing
        simID    = sim_ID('prepace_lat', pp_prtcl, mesh_ID(meshname))
        #simID   = 'prepace_lat'
        #chkpt   = ['-num_tsav', 1, '-tsav[0]', t_chks] + [ '-num_stim', 0]
        lats_pp = setup_lats_prepacing(pp_bcl, pp_cyc, lats_file)

        # turn off any stimuli as in the off case
        for i, e in enumerate(pp_elecs):
            stims += ['-stim[{}].ptcl.start'.format(i),    0,
                      '-stim[{}].ptcl.npls'.format(i),     0,
                      '-stim[{}].ptcl.duration'.format(i), 0 ]
    
        cmd += [ '-tend', 1., '-spacedt', 1. ]

    elif mode.startswith('lat-'):

        # use fast lats-based pre-pacing + one full activation sequence to account for diffusion
        simID   = sim_ID('prepace_{}'.format(mode), pp_prtcl, mesh_ID(meshname))
        
        # by default we use the cycles from the protocol definition
        if pp_cyc is not None:
            pp_n = pp_cyc
        # parse argument to overrule protocol definition    
        rep = mode.split('lat-')[1]
        if (rep.isdigit()):
            n = int(rep)
            if n > 0:
                pp_n = n
                
        assert(pp_n>0)
        
        # checkpoint timings at any end of a pcl
        for i in range(0,pp_n):
            t_chks += [ (i+1) * pp_bcl ]

        # lat-based prepacing, this is cheap, we can do a number of cycles
        pp_cyc=8
        lats_pp = setup_lats_prepacing(pp_bcl, pp_cyc, lats_file)

        for i, e in enumerate(pp_elecs):
            stims += ['-stim[{}].ptcl.bcl'.format(i),  pp_bcl,
                      '-stim[{}].ptcl.npls'.format(i), pp_n ]
    
        # adjust simulation duration
        cmd += [ '-tend', pp_n * pp_bcl + stop, '-spacedt', 1. ]

    else :  # mode == 'full':
        # no LAT vector needed, we do brute force pacing
        print('Full prepacing procedure')
        simID = sim_ID('prepace_full', pp_prtcl, mesh_ID(meshname))

       # checkpoint timings at any end of a pcl
        for i in range(0,pp_cyc):
            t_chks += [ (i+1) * pp_bcl + start ]


        # checkpoint at t=0 ms
        #t_chks  += [ pp_bcl * pp_cyc + start ]

        # add prepacing protocol to stimuli
        for i, e in enumerate(pp_elecs):
            stims += ['-stim[{}].ptcl.bcl'.format(i),  pp_bcl,
                      '-stim[{}].ptcl.npls'.format(i), pp_cyc ]

        # adjust simulation duration
        cmd += [ '-tend', pp_bcl*pp_cyc + start, '-spacedt', 1. ]


    vis = []
    if visual:
        vis += [ '-gridout_i', 3,
                 '-spacedt',   1 ]

    # add checkpoint for restarting
    cmd += setup_tsavs(t_chks, chkptf)
    cmd += stims
    cmd += lats_pp
    # input forces simulation beyond the end of latest stimulus
    # this is efficient, but input logic doesn't allow for early stopping with a tend of 0
    cmd += ['-simID', simID, '-experiment', 0 ] + vis 

    return cmd, chkptf, t_chks, simID


# pick run mode 'rd' or 're'
def restart(meshname, cnf, fncs, elecs, pp_prtcl, mode, rmode, restart_chkpt, visual):

    # set up overall command line
    cmd = setup_prepace(meshname, cnf, fncs, elecs, pp_prtcl, rmode)

    simID = 'validate_pp_{}'.format(mode)

    # determine checkpoint timing
    pp_bcl = pp_prtcl['bcl']
    if mode == 'full':
        pp_cyc = pp_prtcl['cycles']
        t_chk  = pp_bcl * pp_cyc
    else:
        t_chk  = 0

    # determine checkpoint to restart from
    cmd += ['-start_statef', restart_chkpt]
    
    cmd += ['-simID', simID, '-experiment', 0]

    # simulate one bcl
    cmd += [ '-tend', t_chk + pp_bcl ]

    vis = []
    if visual:
        vis = [ '-gridout_i', 3,
                '-spacedt',   1 ]

    cmd += vis

    return cmd, simID


# print a summary of prepacing protocol
def print_prepace_summary(ppname, pprtcl, cnf, exp, prepace, lat, rstrt_file):

    fncs    = exp.get('functions')
    elecs   = exp.get('electrodes')

    # determine used lat vector
    pp_lat = lat 
    if lat is None:
        pp_lat = pprtcl.get('lat')

    if prepace == 'off' or prepace == 'full':
        pp_lat = None

    hd = 'Prepacing Summary:'
    print('\n{}'.format(hd))
    print('='*len(hd) + '\n')

    # method used to compute prepaced state
    print('Prepacing mode: {}'.format(prepace))

    # protocol description
    print('{:14s}: {}'.format('Basic CL', pprtcl.get('bcl')))
    print('{:14s}: {}'.format('#cycles', pprtcl.get('cycles')))


    # state after prepacing stored in checkpoint
    print('{:14s}: {}'.format('Checkpoint', rstrt_file))
    print('{:14s}: {}'.format('LAT vector', pp_lat))

    # in all cases we use an initial state vector
    hd = 'Region-specific initialization state vectors to be used are:'
    print('\n{}'.format(hd))
    print('-' * len(hd) + '\n')

    for dkey, domain in cnf.items():

        # ignore null domains
        if domain is None:
            continue

        # get function defintion
        tags = domain.get('tags')
        fnc_name = domain.get('func')

        # definition of function
        fnc_def = fncs.get(fnc_name)

        #
        if not fnc_def:
            print('Function {} assigned in {} not defined.'.format(fnc_def, dkey))
        else:
            ep = fnc_def.get('EP')

            if ep is None:
                print('{:14s}: no EP defined in passive domain ({})'.format(dkey,tags))
                continue

            etags = None
            #if etags is not None:
             #   labels = expand_domain_labels(etags, labels)
            print('{:14s}: {} in ({})'.format(dkey, ep.get('init'), tags))

    print()

# ensure consistent prepacing checkpoint filenames for restarting
def chkpt_file_ID(prefix, ID, gen_mode, t_chk, bcl):

    ID = '%s_%s_pp_%s_bcl_%.1f_tstamp_%.1f'%(prefix, ID, gen_mode, t_chk, bcl)

    return ID
