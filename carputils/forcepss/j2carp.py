import os, sys
from carputils import tools
from json import load, dump

from carputils.job import Job
from carputils.carpio import meshtool


# artificial domains
_J2CARP_ART_DOMAINS = ('BATH', 'SPLIT')

# load dictionary from json file
def load_json(jf):

    j_dict = None
    # check json file exists
    if os.path.exists(jf):
        with open(jf) as fp:
            j_dict = load(fp)
        fp.close()

    else:
        print('\n\nJSON file {} not found.'.format(jf))

    return j_dict


def dump_json(j_file, j_dict):

    with open(j_file, 'wt') as fp:
        print('\n\nWriting JSON file {}.'.format(j_file))
        dump(j_dict, fp, indent=4)
        fp.close()


def load_experiment(cnf_file=None, exp_file=None, plan_file=None, elecs_file=None, prtcls_file=None):
    """
    Load all json dictionaries describing a given experiment

    :param cnf_file:
    :param exp_file:
    :param plan_file:
    :param elecs_file:
    :param prtcls_file:
    :return:
    """

    # prioritize plan file
    plan = {} 
    cfg  = {}
    exp  = {}
    slv  = {}
    if plan_file:
        plan = load_json(plan_file)
        
        # config from plan takes precedence
        cfg = plan.get('config')

        # solver settings are provided through config
        slv = plan.get('solver')

        # experiment included in plan, can be overruled by dedicated experiment.json
        fncs = plan.get('functions')
        exp['functions'] = fncs 
        exp['electrodes'] = plan.get('electrodes')
        exp['protocols'] = plan.get('protocols')

    # configuration linking tags and function
    if cnf_file:
        cnf = load_json(cnf_file)
    
        # check whether we have an explicit config section in the dictionary
        cfg = cnf.get('config')
        if not cfg:
            cfg = cnf

        # read out solver settings
        slv = cfg.get('solver') 

        # add also to plan 
        plan['config'] = cfg
   

    # experimental defintions
    if exp_file:

        exp = load_json(exp_file)
        fncs = exp.get('functions')
        slv = exp.get('solver')

        # override experimental section in plan
        plan['functions'] = fncs
        plan['electrodes'] = exp.get('electrodes')
        plan['protocols'] = exp.get('protocols')



    # make sure we have all needed definitions
    if plan and cfg and exp and slv:
        print('Simulation plan complete.')
    else:
        fatal = 0
        if not cfg:
            print('No configuration section provided.')
            fatal +=1
        elif not exp:
            print('No experimental functions provided.')
            fatal +=1
        elif not slv:
            print('No solver settings provided, using simulator default settings.')
        else:
            fatal +=1
            print('Simulation plan incomplete.')

        if fatal:
            sys.exit()

    # read in all available electrodes, either from experiment or from dedicated electrode file
    elecs = get_dict_sec('electrodes', exp_file, elecs_file)

    # read in all defined protocols, either from experiment or from dedicated protocol file
    prtcls = get_dict_sec('protocols', exp_file, prtcls_file)

    return cfg, exp, fncs, elecs, prtcls, slv, plan


# check validity of keys
def valid_keys(jsec, key_lst):
    """
    """

    valid_keys = []
    for key, sec in jsec.items():
        valid_keys += [ key ]

    for k in key_lst:
        if k not in valid_keys:
            print('Removing invalid key {} specified.'.format(k))
            key_lst.remove(k)

    # returned cleaned key list only containing valid keys
    return key_lst


def get_init_dir(init_dir, up):
    """
    Retrieve/create path to initialization directory
    :param init_dir:
    :param up:
    :return: path to directory for storing initialization files (state vectors, lat sequences, checkpoints, ...)
    """
    if up:
        init_prfx = init_dir
    else:
        pth, idir = os.path.split(init_dir)
        init_prfx = os.path.join(pth,'tmp-' + idir)

    if not os.path.exists(init_prfx):
        os.mkdir(init_prfx)

    return init_prfx

# dealing with mutable flag
def is_mutable(sec, ignore):

    if not ignore:
        mutable = sec.get('mutable')
        if mutable is None:
            # mutable not specified in dictionary, we assume non-mutable here
            mutable = False
    else:
        mutable = True

    return mutable


# turn CVs and Gs of a CV section into list of values
# four lists are generated
# v_lst   [ v_f,  v_s,  (v_n)  ]
# gi_list [ gi_f, gi_s, (gi_n) ]
# gi_list [ gi_f, gi_s, (gi_n) ]
# axes    [ 'f',  't',  ('n')  ]
# values for normal axis are optional, in case of rotational isotropy n will be omitted
def vel2List(CVs, G):

    v_lst  = [CVs['vf' ], CVs['vs' ]]
    gi_lst = [G  ['gil'], G  ['git']]
    ge_lst = [G  ['gel'], G  ['get']]
    axes   = ['f', 't']

    #
    if CVs['vn'] != CVs['vs']:
        v_lst.append (CVs['vn' ])
        gi_lst.append(G  ['gin'])
        ge_lst.append(G  ['gen'])
        axes.append('n')

    return v_lst, gi_lst, ge_lst, axes

# manage 'BATH' section to exclude regions from being active EP
def get_bath_labels(cnf, pop=False):
    """

    """

    # double check first whether a global BATH region is used
    bl = cnf.get('BATH')
    if bl:
        # extract list of names only
        bl = bl.get('labels')

        if pop:
            cnf = cnf.copy()
            # remove 'BATH' from dictionary
            cnf.pop('BATH')

    return bl, cnf


# translate EP dictionary sections to carp input
def ep_secs_to_carp(cnf, fncs):

    carp = []
    imp_cnt = 0
    purk = []
    purk_cnt = 0
    for dkey, domain in cnf.items():

        # get function defintion
        tags = domain.get('tags')
        if not tags:
            continue

        fnc_name = domain.get('func')

        # definition of function
        fnc_def = fncs.get(fnc_name)

        if not fnc_def:
            print('Function {} assigned in {} not defined.'.format(fnc_def, dkey))

        else:
            ep = fnc_def.get('EP')

            # if ep is not null we have a valid EP definition, increment imp region count
            if ep:
                if 'PURK' in dkey:
                    purk += ep_sec_to_carp(tags, dkey, ep, purk_cnt)
                    purk_cnt = purk_cnt + 1
                else:
                    carp += ep_sec_to_carp(tags, dkey, ep, imp_cnt)
                    imp_cnt = imp_cnt + 1

    if imp_cnt > 0:
        carp = ['-num_imp_regions', imp_cnt ] + carp

    if purk_cnt > 0:
        purk = [ '-numPurkIon', purk_cnt ] + purk

    return carp + purk


# translate single EP dictionary section to carp input
def ep_sec_to_carp(tags, key, ep, idx):

    # dealing with regular imp region or purk region?
    if 'PURK' in key:
        reg = '-PurkIon[{}]'.format(idx)
    else:
        reg = '-imp_region[{}]'.format(idx)

    # name the imp_region
    carp = [reg+'.name', key ]

    # list IDs forming the G region
    carp += [reg+'.num_IDs'.format(idx), len(tags)]
    for i, l in enumerate(sorted(tags)):
        carp += [reg+'.ID[{}]'.format(i), l]

    # model is mandatory
    carp += [reg+'.im', ep['model']]

    modelpar = ep.get('modelpar', None)
    if modelpar and len(modelpar) > 0:
        carp += [reg+'.im_param', modelpar]

    # plug-ins still missing


    init = ep.get('init', None)
    if init and len(init) > 0:
        carp += [reg+'.im_sv_init', os.path.abspath(init)]

    return carp


# translate conductivity sections to carp input
def G_secs_to_carp(cnf, fncs):

    carp = []
    reg_cnt = 0
    for dkey, domain in cnf.items():

        # get function defintion
        tags     = domain.get('tags')
        fnc_name = domain.get('func')

        # definition of function
        fnc_def = fncs.get(fnc_name)

        if not fnc_def:
            print('Function {} assigned in {} not defined.'.format(fnc_def,dkey))

        else:
            cv = fnc_def.get('CV')

            # skip if CV not defined
            if cv is None:
                print('\nNo CV defined for function domain {} !\n'.format(fnc_def))
                continue

            carp += G_sec_to_carp(tags, dkey, cv['G'], reg_cnt)
            reg_cnt += 1

    carp = ['-num_gregions', reg_cnt] + carp

    return carp


# translate single conductivity section to carp input
def G_sec_to_carp(tags, key, G, idx):

    if not tags:
        return []

    prefix = '-gregion[{}].'.format(idx)
    # assign name
    carp = [prefix+'name', key]

    # list IDs forming the G region
    carp += [prefix+'num_IDs', len(tags)]
    for i, l in enumerate(sorted(tags)):
        carp += [prefix+'ID[{}]'.format(i), l]

    # assign conductivity values
    if G.get('gil') is not None:
        carp += [prefix+'g_il', G['gil'],
                 prefix+'g_it', G['git'],
                 prefix+'g_in', G['gin'],
                 prefix+'g_el', G['gel'],
                 prefix+'g_et', G['get'],
                 prefix+'g_en', G['gen']]
    else:
        # bath domain
        carp += [prefix+'g_bath'.format(idx), G['gbath'] ]

    return carp


# translate conduction velocity sections to carp input
def CV_secs_to_carp(cnf, fncs):

    # dictionary of defined functions
    fnc_lst = fncs.get('functions')

    carp = []
    ek_cnt = 0
    for dkey, domain in cnf.items():

        # get function defintion
        tags     = domain.get('tags')
        fnc_name = domain.get('func')

        # definition of function
        fnc_def = fncs.get(fnc_name)

        #
        if not fnc_def:
            print('Function {} assigned in {} not defined.'.format(fnc_def,key))
        else:
            cv = fnc_def.get('CV')

            # skip if CV not defined
            if cv.get('CVref') is None:
                print('\nNo CV defined for function domain {} !\n'.format(dkey))
                continue

            carp += CV_sec_to_carp(cv, ek_cnt, dkey)
            ek_cnt = ek_cnt + 1

    carp = ['-num_ekregions', ek_cnt] + carp

    return carp


# translate single conduction velocity section to carp input
def CV_sec_to_carp(cv, idx, key):
    prefix = '-ekregion[{}].'.format(idx)
    carp = [prefix+'ID',    idx,
            prefix+'name',  key,
            prefix+'vel_f', cv['CVref']['vf'],
            prefix+'vel_s', cv['CVref']['vs'],
            prefix+'vel_n', cv['CVref']['vn']]

    return carp

# build physics regions from tag sets
def gen_physics(ExtraTags=None, IntraTags=None, EikonalTags=None, MechTags=None, FluidTags=None):

    num_phys = 0
    phys_opts = []

    if IntraTags is not None:

        phys_opts += domain_to_phys_region(0, '"Intracellular domain"', IntraTags, num_phys)
        num_phys += 1

    if ExtraTags is not None:
        phys_opts += domain_to_phys_region(1, '"Extracellular domain"', ExtraTags, num_phys)
        num_phys += 1

    if EikonalTags is not None:
        phys_opts += domain_to_phys_region(2, '"Eikonal domain"', EikonalTags, num_phys)
        num_phys += 1

    if MechTags is not None:
        phys_opts += domain_to_phys_region(3, '"Mechanics domain"', MechTags, num_phys)
        num_phys += 1

    if FluidTags is not None:
        phys_opts += domain_to_phys_region(4, '"Fluid domain"', FluidTags, num_phys)

    phys_opts = ['-num_phys_regions', num_phys] + phys_opts

    return phys_opts


# build single physics region definition
def domain_to_phys_region(ptype, name, domain, idx):

    preg = '-phys_region[{}]'.format(idx)
    phys_reg = [ preg+'.ptype',   ptype,
                 preg+'.name',    name,
                 preg+'.num_IDs', len(domain) ]

    for i, l in enumerate(domain):
        phys_reg += [ preg+'.ID[{}]'.format(i), l]

    return phys_reg


# get list of available dictionary sections for given key word
# from various json dictionaries
# prioritize according to order of provided dictionaries, first found is used
def get_dict_sec(dict_key, *dicts):

    sec ={}
    for dct in dicts:

        # get dictionary
        if dct is None:
            continue

        j_dict = load_json(dct)

        if not j_dict:
            print('\n\nFailed to load {}.'.format(dct))
            continue

        # read "electrodes" section from dictionary
        new_sec = j_dict.get(dict_key)

        if new_sec is not None:
            # done, we found a section for the given key
            print('Reading section %s from %s.'%(dict_key,dct))
            sec = {**sec, **new_sec}
        else:
            print('No section %s found in %s'%(dict_key, dct ))

    return sec


# build electrode list from electrode dictionary (elecs)
# assign idx to electrode within stimulus array
# pick only electrodes from active list
def elec_sec_to_carp(elecs, idx, act_lst):

    # make sure active list is given as list
    if isinstance(act_lst, str):
        act_lst = [act_lst]

    stm_elec = []
    for stm_name in act_lst:

        # get listed electrode description from dictionary
        elec = elecs.get(stm_name)

        # build electrode
        if elec is not None:
            stm_elec += build_stim_geom(elec, idx, stm_name)
            idx += 1

    return stm_elec, idx


# pick a single point [x,y,z] and turn it into stim[].elec format
def get_elec_point(pt, pt_idx, stm):

    pool = ['p0','p1']

    if len(pt)==3 and pt_idx in pool:

        e = []
        for i,x in enumerate(pt):
            e += [ stm+'.%s[%d]'%(pt_idx,i), x ]

        return e


# build a single electrode based on dictionary description
# stash carp electrode in stim array in slot idx
def build_stim_geom(elec, idx, stm_name):

    # electrode type to build
    et = elec.get('type')

    stm   = '-stim[{}]'.format(idx)
    stm_e = '-stim[{}].elec'.format(idx)

    e = [ stm + '.name', stm_name ]
    if et == 'vtxlist':
        # check vertex file
        vf = elec.get('vtxfile')
        if not vf:
            print('\n\nMissing \'vtxfile\' entry in definition of electrode')
        else:
            if os.path.exists(vf):

                e += [ stm_e+'.vtx_file', vf ]
            else:
                print('\n\nVertex file {} does not exist.\n'.format(vf))

    elif et == 'vtxdata':
        # read in vertex indices
        vtcs = elec.get('vtxdata')

        # write vtx data to vtx file
        vf = stm_name+'.vtx'
        with open(vf,'wt') as fp:
            fp.write('%d\n'%(len(vtcs)))
            fp.write('extra\n')
            for v in vtcs:
                fp.write('%d\n'%(v))
            fp.close()

        e += [stm_e + '.vtx_file', vf]

    elif et == 'tag':
        stm_tag = elec.get('tag')
        e += [ stm_e+'.geomID', stm_tag ]

    elif et == 'sphere':

        e += [ stm_e + '.geom_type', 1 ]
        e += get_elec_point(elec.get('p0'), 'p0', stm_e)
        e += [ stm_e + '.radius', elec.get('radius')]

    elif et == 'cylinder':
        e += get_elec_point(elec.get('p0'), 'p0', stm_e)
        e += get_elec_point(elec.get('p1'), 'p1', stm_e)
        e += [ stm_e + '.radius', elec.get('radius')]

    elif et == 'block':
        e += get_elec_point(elec.get('p0'), 'p0', stm_e )
        e += get_elec_point(elec.get('p1'), 'p1', stm_e )

    else:
        print('Electrode definition of type %s not supported.'%(et))

    # write dump for now to check correct selection
    e += [ stm_e + '.dump_vtx_file', 1 ]

    return e


# define single lat detector for recording the activation sequence
def ek_lat_detect(idx, threshold=-10, **kwargs):

    lat = '-lats[{}]'.format(idx)

    lats  = [ lat + '.ID', 'vm_act_seq.dat',
              lat + '.measurand', 0,
              lat + '.all', 0,
              lat + '.mode', 0,
              lat + '.threshold', threshold,
              lat + '.method', 1 ]


    if 'verbose' in kwargs:
        hdr='LAT detector added with index {}.'.format(idx)
        print(hdr)
        print('-'*len(hdr))

        print(*lats)

    return lats



# define single lat detector for recording the activation sequence
def lat(idx, ID, threshold=-10, measurand=0, mode=0, detect_all=0, method=1):

    lat = '-lats[{}]'.format(idx)

    lats  = [ lat + '.ID', ID,
              lat + '.measurand', measurand,
              lat + '.all',  detect_all,
              lat + '.mode', mode,
              lat + '.threshold', threshold,
              lat + '.method', method ]

    return lats

# add a sentinel for activity checking
def sentinel(idx, start, window):

    sentinel = ['-sentinel_ID', idx,
                '-t_sentinel_start', start,
                '-t_sentinel', window]

    return sentinel


# select propagation model
def p_mode(mode):

    if mode in [ 'ek', 'EK', 'eikonal' ]:
        return 'ek'
    elif mode in [ 're', 'R-E+', 'R-E-'] :
        return 're'
    elif mode in [ 'rd', 'R-D' ]:
        return 'rd'
    else:
        print('\nUnknown propagation mode selected.\n')
        return None

# setup mesh-related command options
def setup_mesh(meshname, use_split=True, split_file=None, tags_file=None):
    """
    Set up mesh command including the use of a split file as well as an alternative
    element tag vector

    :param meshname:
    :param use_split:
    :param split_file:
    :param tags_file:
    :return: mesh_opts command line parameters for selecting mesh, split file and alternative tag vector
    """
    mesh_opts = ['-meshname', meshname]
    # use split information?
    if use_split:
        if split_file is None:
            # use default split file
            split_file = '{}.splt'.format(meshname)

        if os.path.exists(split_file):
            mesh_opts += ['-use_splitting', 1 ,
                          '-splitname',     split_file]
        else:
            print('Splitting selected, but split file {} does not exist!'.format(split_file))
            print('Proceed without splitting.')
    else:
        print('Use of split file disabled.')

    if tags_file is not None:
        if tags_file.endswith('.tag'):
            tags_file = tags_file[:-4]
        elif tags_file.endswith('.btag'):
            tags_file = tags_file[:-5]
        if os.path.exists(tags_file+'.tag') or os.path.exists(tags_file+'.btag'):
            mesh_opts += ['-etagsname', tags_file]
        else:
            print('Tags selected, but tag file {}.{{tag,btag}} does not exist!'.format(tags_file))
            print('Proceed without re-tagging.')

    return mesh_opts

# return intra mesh extension as function of run mode
def intra_mesh_ext(rmode):

    if rmode == 'ek':
        ext = '_ek'
    else:
        ext = '_i'

    return ext
# set up tissue properties
def setup_tissue(cnf, fncs, rmode):

    # define excitable regions based on the definition of EP
    i_domain = []
    e_domain = []
    for dkey, domain in cnf.items():

        n_domain = domain.get('tags')
        # if no tags assigned, empty domain, skip
        if not n_domain:
            continue

        fnc_name = domain.get('func')

        # definition of function
        fnc_def = fncs.get(fnc_name)

        if not fnc_def:
            # function name is not defined
            print('Function {} not defined!'.format(fnc_name))
            continue

        # all domains are part of extra
        e_domain += n_domain

        if fnc_def.get('EP') is not None:
            i_domain += n_domain

    i_domain = sorted(i_domain)
    e_domain = sorted(e_domain)

    phys_regs = []
    if p_mode(rmode) == 'ek':
        phys_regs = gen_physics(IntraTags=i_domain, EikonalTags=i_domain)

    elif p_mode(rmode) == 'rd':
        phys_regs = gen_physics(IntraTags=i_domain, ExtraTags=e_domain)

    elif p_mode(rmode) == 're':
        phys_regs = gen_physics(IntraTags=i_domain, ExtraTags=e_domain, EikonalTags=i_domain)

    else:
        print('Unknown propagation mode {}.'.format(rmode))

    # set up gregion and ek_region
    g_regs  = G_secs_to_carp(cnf, fncs)

    ek_regs = []
    if p_mode(rmode) == 'ek' or p_mode(rmode) == 're':
        ek_regs += CV_secs_to_carp(cnf, fncs)

    # set up EP
    imp_regs = []
    #if p_mode(rmode) != 'ek':
    imp_regs += ep_secs_to_carp(cnf, fncs)

    cmd  = imp_regs
    cmd += g_regs
    cmd += ek_regs
    cmd += phys_regs

    return cmd


# check configuration
def check_config(cnf, fncs):

    chk_msg = 'Checking configuration and function definitions'
    print('\n{}'.format(chk_msg))
    print('-'*len(chk_msg))

    # get bath labels for marking non active EP
    bl, cnf = get_bath_labels(cnf, pop=True)

    max_len = 0
    for dkey in cnf:
        if len(dkey) > max_len:
            max_len = len(dkey)

    ok = True
    bl_flg = '- [ passive (\'BATH\')] '
    for dkey, domain in cnf.items():

        if dkey in _J2CARP_ART_DOMAINS or domain is None:
            continue

        tags     = domain.get('tags')
        fnc_name = domain.get('func')

        # look up in function definition
        fnc_def = fncs.get(fnc_name)

        if fnc_def:
            active = ''
            if bl and dkey in bl:
                active = bl_flg
            print('Domain {} : tags {} use function {} {}'.format(dkey.ljust(max_len,' '), tags, fnc_name, active))
        else:
            print('Domain {} : tags {} use function {} -- not defined.'.format(dkey.ljust(max_len, ' '), tags, fnc_name))
            ok = False

    if not ok:
        print('\n\n')
        print('Configuration check failed, domains using undefined functions found.\n')

    return ok


# add solver settings
def add_slv_settings(slv):

    cmd = []
    dt = slv.get('dt')
    if dt:
        cmd += [ '-dt', dt ]

    ts = slv.get('ts')
    if ts:
        cmd += [ '-parab_solve', ts ]

    mass_lump = slv.get('lumping')
    if mass_lump:
        cmd += [ '-mass_lumping', 1 ]
    else:
        cmd += [ '-mass_lumping', 0 ]

    opsplit = slv.get('opsplit')
    if opsplit:
        cmd += [ '-operator_splitting', 1 ]
    else:
        cmd += [ '-operator_splitting', 0 ]

    stol = slv.get('stol')
    if stol:
        cmd += [ '-cg_tol_parab', stol ]

    beqm = slv.get('beqm')
    if beqm:
        cmd += ['-bidm_eqv_mono', 1 ]
    else:
        cmd += ['-bidm_eqv_mono', 0 ]

    return cmd


# make sure all specified initial state vectors are found
def check_initial_state_vectors(fncs, verbose):

    max_len = 0
    if verbose:
        print('\n')
        header = 'Checking specified initialization files:'
        print(header)
        print('-'*len(header))

        for key in fncs:
            if len(key) > max_len:
                max_len = len(key)

    missing = 0
    for key,fnc in fncs.items():
        if fnc:
            ep = fnc.get('EP')
            if ep:
                init_file = ep.get('init')
                if len(init_file):
                    if not os.path.exists(init_file):
                        if verbose:
                            print('{} : Initialization uses file {} -- missing.'.format(key.ljust(max_len,' '), init_file))
                        missing += 1
                    else:
                        if verbose:
                            print('{} : Initialization uses file {}.'.format(key.ljust(max_len,' '), init_file))

    if not missing:
        return True
    else:
        return False



# helper function to select stimulus mode
# voltage is more robust, but currently ignored in ek and re runs
# decide on stimulus type as function of propagation mode
def stim_mode(prp):

    # by defaul in rd runs, use voltage clamp to achieve robust capture
    s_type = 9
    s_strength = -30

    # eikonal mediated propagation picks up only transmembrane stimuli
    if p_mode(prp) == 'ek' or p_mode(prp) == 're':
        # eikonal-mediated triggers on transmembrane current stimuli
        s_strength = 250
        s_type     = 0

    return s_type, s_strength


# update experimental description
def update_exp_desc(up_flag, exp_file, exp):

    if up_flag:
        # update experimental settings file
        fname = exp_file
    else:
        # dump to alternative file for review/comparison
        dname = os.path.dirname(exp_file)
        bname = os.path.basename(exp_file)
        base, ext = os.path.splitext(bname)
        fname = os.path.join(dname,'tmp-{}{}'.format(base, ext))

    print('Updating experimental settings to {}'.format(fname))

    with open(fname, 'w') as f:
        dump(exp, f, indent=4)


def get_splitting_op_string(cnf):
    split = cnf.get('SPLIT', None)
    if split is None:
        return None

    # get bath labels
    bl, _ = get_bath_labels(cnf, pop=False)

    # construct the op-string for the meshtool command
    ops = split.get('ops', None)
    op_list = []
    for op in ops:
        # collect lhs and rhs tags,
        # ignore BATH regions
        lhs_tags, rhs_tags = [], []
        for label in op['lhs']:
            if label not in bl:
                lhs_tags.extend(cnf[label]['tags'])
        for label in op['rhs']:
            if label not in bl:
                rhs_tags.extend(cnf[label]['tags'])
        # ignore ops where lhs or rhs is empty
        if len(lhs_tags) == 0 or len(rhs_tags) == 0:
            continue
        lhs_tags = map(str, sorted(lhs_tags))
        rhs_tags = map(str, sorted(rhs_tags))
        op_list.append(','.join(lhs_tags)+':'+','.join(rhs_tags))

    # ignore ops where lhs or rhs is empty
    if len(op_list) == 0:
        return None
    op_str = '/'.join(op_list)

    return op_str


def generate_split_file(job, cnf, mesh_name, force_recreation=False):
    # check if 'SPLIT' entry exists.
    # if not, return None
    split = cnf.get('SPLIT', None)
    if split is None:
        return None, False

    msg = 'Found splitting definition.'
    msg += '\n'+'-'*len(msg)
    print('\n'+msg)

    active = split.get('active', True)
    if not active:
        print('Splitting deactivated!')
        return None, False

    # check if a file name is specified,
    # if not create a default file name
    fname = split.get('fname', None)
    if fname is None:
        fname = mesh_name+'.splt'
    if not fname.endswith('.splt'):
        fname += '.splt'
    # if the file exists and creation is not forced
    # return the file name
    if os.path.exists(fname) and not force_recreation:
        print('Splitting file {} already exists!'.format(fname))
        return fname, True

    op_str = get_splitting_op_string(cnf)
    if op_str is None:
        print('Failed to generate op-string, deactivate splitting')
        return None, False

    meshtool.generate_split(job, mesh_name, op_str, fname)
    print('Splitting file {} created!'.format(fname))

    return fname, True

def plan2carp(job, plan_file, meshname, rmode):

    plan_abs = os.path.abspath(plan_file)
    mesh_abs = os.path.abspath(meshname)
    plan_dir = os.path.dirname(plan_abs)

    # we navigate into the directory of the plan file in order to resolve the relative paths of the plan
    workdir = os.getcwd()
    os.chdir(plan_dir)

    cnf, exps, fncs, elecs, prtcls, slv, plan = load_experiment(None, None, plan_abs, None, None)

    # create split file
    split_file, split_active = generate_split_file(job, cnf, mesh_abs, force_recreation=False)

    # setup mesh
    cmd = setup_mesh(mesh_abs, split_file)

    # set up tissue properties
    cmd += setup_tissue(cnf, fncs, rmode)

    # set up solver settings
    cmd += add_slv_settings(slv)

    # when done, we go back to the actual working director
    os.chdir(workdir)

    return cmd
