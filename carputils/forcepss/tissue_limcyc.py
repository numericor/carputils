#!/usr/bin/env python3

r"""
"""
import os, sys, argparse
from datetime import date
from json import load, dump
#from shutil import copy
import numpy as np

from carputils import settings
from carputils import tools
from carputils import model
from carputils import mesh
from carputils.cep4cms import j2carp as j2c

EXAMPLE_DESCRIPTIVE_NAME = 'Tissue experiment velocity (restitution)/conductivity measuring and tuning'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>'
EXAMPLE_DIR = os.path.dirname(__file__)
GUIinclude = False


def vel2List(CVs, G, cvg):

    # assume rotational isotropy first
    v_list  = [CVs['vf'], CVs['vs']]
    gi_list = [G['gil'], G['git']]
    ge_list = [G['gel'], G['get']]
    axes    = ['f', 't']

    rot_iso = True
    if cvg:
        # we tune Gs to match CVmeas with CVref
        # only tune for sheet normal if reference vn and vs differ
        if CVs['vn'] != CVs['vs']:
            rot_iso = False
    else:
        # we only measure sheet normal if sheet-normal conductivity settings 
        # differ from transverse (sheet) conductivity settings 
        if G.get('git') != G.get('gin') or G.get('get') != G.get('gen'):
            rot_iso = False

    if not rot_iso:
        # append sheet-normal data
        v_list.append(CVs['vn'])
        gi_list.append(G['gin'])
        ge_list.append(G['gen'])
        axes.append('n')

    return v_list, gi_list, ge_list, axes

def converge_CV(job, solver, converge, tol, tmode, dx, restitute_pcls, dkey, ep, cv, trg_dir, np, tbl,
                ID, carp_opts, visual):
    """

    """

    exp_type = 'measuring conduction velocity'
    if converge:
        exp_type = 'tuning conduction velocity'
    if restitute_pcls:
        exp_type = 'measuring conduction velocity restitution'

    print('\n\nInitializing tissue domain {} - {}.'.format(dkey, exp_type))
    print('=' * 88 + '\n\n')

    G  = cv['G']

    length = 2      # length of strand for testing 
    #tol    = 1e-5   # tolerance for velocity convergence
  
    cvsr = []
    cvs  = []
    gis  = []
    ges  = []
    gms  = []
    cvg  = converge

    # reference velocities
    cvref = cv.get('CVref')
    if not cvref:
        return None

    # measured velocities
    cvmeas = cv.get('CVmeas')

    if cvg:
        cvtrg = cvref
    else:
        cvtrg = cvmeas

    cvs, gis, ges, ax = vel2List(cvtrg, G, cvg)

    # iterate over velocities
    tune_cv = {}
    for i, v in enumerate(cvs):

        # velocity must be > 0
        if v == 0: 
            print('Measured velocity CVmeas {} in {} must be > 0!'.format(ax[i], dkey))
            continue

        # create command line for each direction   
        cmd = [settings.execs.TUNECV, 
               '--np',         np,
               '--model',      ep['model'] ]

        # add optional parameters
        mpar   = ep.get('modelpar')
        svinit = ep.get('init')
        plugs  = ep.get('plugs')
        ppar   = ep.get('plugpar')
        if mpar:
            cmd += ['--modelpar', mpar ]
        if plugs:
            cmd += ['--plugs',  plugs ]
        if ppar:
            cmd += ['--plugpar', ppar ]
        if svinit:
            cmd += ['--svinit', svinit ]

        # spatial test resolution
        if dx is None:
            dx = cv.get('dx')
        else:
            cv['dx'] = dx

        cmd += ['--converge',   cvg,
                '--velocity',   v,
                '--resolution', dx,
                '--gi',         gis[i],
                '--ge',         ges[i],
                '--beta',       G.get('surf2vol'),
                '--dt',         solver.get('dt'),
                '--beqm',       solver.get('beqm'),
                '--ts',         solver.get('ts'),
                '--lumping',    solver.get('lumping'),
                '--opsplit',    solver.get('opsplit'),
                '--stol',       solver.get('stol'),
                '--tol',        str(tol),
                '--length',     str(length),
                '--stimS',      500,
                '--stimV',
                '--mode',       tmode,
                '--log',        'tunecv_{}_{}.log'.format(dkey, ax[i]),
                '--silent']

        if tbl is not None:
            cmd += [ '--table', '{}.dat'.format(tbl) ]

        if carp_opts is not None:
            cmd += [ '--CARP-opts', '\"%s\"'%(carp_opts[0])]

        if ID is not None:
            cmd += [ '--ID', '{}_{}'.format(ID, ax[i]) ]

        cvres_file = None
        if restitute_pcls is not None:
            cvres_file = os.path.join(trg_dir, '{}_{}_CV_restitution.dat'.format(dkey, ax[i]))
            #cvres_file = '{}_{}_CV_restitution.dat'.format(dkey, ax[i])
            cmd += [ '--pcl'] + restitute_pcls
            cmd += [ '--cvres', cvres_file ]

        if visual:
            cmd += ['--visualize']
    
        # run tuneCV 
        job.bash(cmd)

        with open('tune.json') as fp:
            tune = load(fp)
            tune_cv['{}'.format(ax[i])] = tune
            # check for rotational isotropy
            if ax[i] == 't' and 'n' not in ax:
                tune_cv['n'] = tune

    # dump combined tune_cv for all directions
    fname = '{}_tune_cv.json'.format(dkey)
    with open(fname, 'w') as fp:
        dump(tune_cv, fp, indent=4)

    return tune_cv


def print_tune_cv_file(tune_file, exp_file):

    with open(tune_file) as fp:
        tune_cv = load(fp)

    with open(exp_file) as fp:
        exps = load(fp)

    domains = exps['functions']
    solver = exps['solver']
    print_tune_cv_results(tune_cv, domains, solver)


def print_tune_cv_results(tune_cv, domains, solver):

    print('\n\nTuning Summary')
    print('='*88)

    n_active = 0
    for dkey, res in tune_cv.items():
        
        # experimental settings in domain
        domain = domains.get(dkey)
        cvs    = domain.get('CV')
        on     = cvs.get('mutable')
        Gs     = cvs.get('G')
        dx     = cvs.get('dx')

        # skip empty results
        if res is None or not on:
            continue
        else:
            n_active = n_active + 1
        
        print('\nGlobal numerical settings')
        print('Grid resolution    = {:.1f} um'.format(dx))
        print('Surface-to-volume  = {:.4f} 1/cm'.format(Gs['surf2vol']))
        print('bidm eqv monodm    = {}'.format(bool(solver.get('beqm'))))
        print('dt                 = {:.1f} um'.format(solver['dt']))
        print('time stepper       = {:d}'.format(solver['ts']))
        print('solver tolerance   = {:g}'.format(solver['stol']))
        print('mass lumping       = {}'.format(solver['lumping']))
        print('operator splitting = {}'.format(solver['opsplit']))
        # source model : monodomain
        # stimulus strength : 250.0

        print('\n\nVelocity and conductivity settings for domain {})'.format(dkey))
        print('='*88)

        # deal with inconsistent (historically) naming of axes
        cv_axs = ['vf', 'vs', 'vn']
        for i, ax in enumerate(['f', 't', 'n']):

            print('\nSettings along axis {}'.format(ax))
            print('-'*50)
            ax_data = res.get(ax)
            if ax_data is not None:
                r_ax = ax_data.get('results')
                print('    v{:s}   = {:.4f} m/s (v_ref = {:.3f} m/s)'.format(ax, r_ax['vel'], cvs['CVref'][cv_axs[i]]))
                print('    g_i{:s} = {:.4f} S/m'.format(ax, r_ax['gi']))
                print('    g_e{:s} = {:.4f} S/m'.format(ax, r_ax['ge']))
                print('    g_m{:s} = {:.4f} S/m'.format(ax, r_ax['gm']))
            else:
                print('Tuning of conduction velocity along axis {} failed'.format(ax))


        print('\n')    

    # inform in case there were no active domains
    if not n_active:
        print('Conduction velocity measurement/tuning not triggered.')
        print('No domains were active, set mutable true for domains of interest.\n')


def print_restitute_results(cv_tune):
    """
    """
    print('\n\nCV Restitution Summary')
    print('='*88)

    #CV_res_files, CV_res_labels = get_cv_res_files(cv_tune)

    # get pcl test vector
    pcls = []
    for dom_key, dom_val in cv_tune.items():
        for dir_key, dir_val in dom_val.items():
            d_pcl = dir_val.get('results').get('pcls')
            pcls.append(d_pcl)

    # assume the same pcl vector was tested in all domains
    pcl_vec = pcls[0]

    # print table header
    print('%15s | %3s | %5s'%('domain','ax', 'PCLs'))
    print('%15s | %3s |'%(' ',' '), end='')
    for pcl in pcl_vec:
        print(f'{pcl:7.1f}', end='')
    print()
    print('-'*88)

    # print CV(PCL) results for all domains
    for dom_key, dom_vals in cv_tune.items():

        # get results of domain
        for dir_key, dir_vals in dom_vals.items():
            # domain
            print('%15s |' % (dom_key), end='')
            # direction
            print(' %3s |'%(dir_key), end='')

            # data
            res  = dir_vals.get('results')
            cvrf = res.get('cv_res')
            pcls = res.get('pcls')

            # read CV restitution data from file
            pcls, cvs = read_cv_res_file(cvrf)
            for i, cv in enumerate(cvs):
                print(f'{cv:7.4f}'%(cv), end='')

            # indicate pcls with failed capture
            if len(cvs) < len(pcl_vec):
                for i in range(len(cvs),len(pcl_vec)):
                    print('%7s' % ('   --  ' ), end='')

            print()

    print('='*88)
    print()


def read_cv_res_file(cvrf):
    """
    Read CV restitution data from file

    :param cvrf:
    :return:
    """

    if os.path.isfile(cvrf):
        cvr_data = np.loadtxt(cvrf)
        pcls = cvr_data[:, 0]
        cvs  = cvr_data[:, 1]

    else:
        print('\n\nCV restitution file {} not found.'%(cvrf))
        pcls = cvs = None

    return pcls, cvs


def get_cv_res_files(cv_tune):
    """
    :param cv_tune:
    :return: list of CV restitution files, list of according labels
    """
    CV_res_files  = []
    CV_res_labels = []
    for dom_key, dom_vals in cv_tune.items():
        for dir_key, dir_vals in dom_vals.items():
            res = dir_vals.get('results')
            cv_res_file = res.get('cv_res')

            CV_res_files  += [ cv_res_file ]
            CV_res_labels += [ '{}-{}'.format(dom_key, dir_key)]

    return CV_res_files, CV_res_labels


def update_exp_CV(exps, tune_cv):

    for dkey in tune_cv:
        
        # tuning results
        res     = tune_cv.get(dkey)

        # experimental settings in domain
        domains = exps.get('functions')
        domain  = domains.get(dkey)
        cvs     = domain.get('CV')
        dx      = cvs.get('dx')
        on      = cvs.get('mutable')
        Gs      = cvs.get('G')
        cvs_m   = cvs.get('CVmeas')

        if res is None:
            continue

        # deal with inconsistent (historically) naming of axes
        cv_axs  = ['vf',  'vs',  'vn']
        gi_axs  = ['gil', 'git', 'gin']
        ge_axs  = ['gel', 'get', 'gen']

        for i, ax in enumerate(['f', 't', 'n']):
            ax_data = res.get(ax)
            if ax_data is not None:
                # tuning along this axes worked, we do have data
                r_ax = ax_data.get('results')
                p_ax = ax_data.get('params')

                # update experiment with tuned values
                Gs[gi_axs[i]] = r_ax['gi']
                Gs[ge_axs[i]] = r_ax['ge']
                cvs_m[cv_axs[i]] = r_ax['vel']
            
                # update also surf2vol
                Gs['surf2vol'] = p_ax.get('beta')

    return exps


def update_exp_CVres(exps, tune_cv):

    # walk over all tuned domains
    for dkey, dval in tune_cv.items():

        # experimental settings in domain
        domains = exps.get('functions')
        domain  = domains.get(dkey)
        cvs     = domain.get('CV')
        cvres   = cvs.get('CVres')
        on      = cvs.get('mutable')

        if not cvres:
            cvs['CVres'] = {}
            cvres = cvs.get('CVres')

        if dval is None:
            continue

        # deal with inconsistent (historically) naming of axes
        cvr_f = ['f_file', 's_file', 'n_file']
        cvr_p = ['f_pcl',  's_pcl',  'n_pcl' ]
        cvr_v = ['f_v',    's_v',    'n_v'   ]

        for i, ax in enumerate(['f', 't', 'n']):
            ax_data = dval.get(ax)

            if ax_data is not None:
                # tuning along this axes worked, we do have data
                r_ax = ax_data.get('results')

                cvres[cvr_f[i]] = r_ax.get('cv_res')

                # copy over pcl and cv data
                pcls = r_ax.get('pcls')
                cvs  = r_ax.get('cvs')

                cvres[cvr_p[i]] = pcls
                cvres[cvr_v[i]] = cvs

    return exps


# update experimental description
def update_exp_desc(up_flag, exp_file, exp):

    if up_flag:
        # update experimental settings file
        fname = exp_file
    else:
        # dump to alternative file for review/comparison
        base = os.path.basename(exp_file)
        fname = 'tmp-{}'.format(base)

    print('Updating experimental settings to {}\n'.format(fname))

    with open(fname, 'w') as f:
        dump(exp, f, indent=4)


def converge_CVs(job, exps, converge, tol, tmode, dx, res_pcls, lim_cyc_dir, domain_lst, tag, np, visual, ID,
                 carp_opts, dry):

    # get domain dictionary
    domains = exps.get('functions', None)

    # get solver options
    solver = exps['solver']

    # compute limit cycle for each region, if mutable flag is set
    cv_tune = {}
    for dkey, domain in domains.items():

        # if domain list provided, skip any domain not in list
        if domain_lst is not None and dkey not in domain_lst:
            continue

        ep = domain.get('EP', None)
        cv = domain.get('CV', None)
        if ep is None or cv is None:
            print('\nNo EP or CV defined for domain {} !\n'.format(dkey))
            continue

        # dealing with mutable flag
        ignore_mutable = False
        mutable = j2c.is_mutable(cv,ignore_mutable)

        if cv.get('mutable'):
            cv_tune[dkey] = converge_CV(job, solver, converge, tol, tmode, dx, res_pcls, dkey, ep, cv, lim_cyc_dir,
                                        np, tag, ID, carp_opts, visual)

            # save tuning results
            if not dry:
                with open('./{}/tune_cv.json'.format(job.ID), 'w') as f:
                    dump(cv_tune, f, indent=4)

    return cv_tune
