#!/usr/bin/env python3

r"""
"""
import os, sys, shutil

from datetime import date
from json import load, dump

from carputils import settings
from carputils import tools
from carputils import model
from carputils import restitute as r
from carputils.cep4cms import j2carp as j2c

EXAMPLE_DESCRIPTIVE_NAME = 'Limit cycle experiment'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>'
EXAMPLE_DIR = os.path.dirname(__file__)
GUIinclude = False


def get_sv_init_file_name(dkey, model, bcl, init_dir):
    f = init_dir+'/{}_{}_bcl_{:.1f}.sv'.format(dkey, model, bcl)
    return f

def get_apdres_file_name(dkey, model, bcl, init_dir):
    f = init_dir+'/{}_{}_bcl_{:.1f}_APD_restitution.dat'.format(dkey, model, bcl)
    return f

def get_apbeats_file_name(dkey, model, bcl, init_dir):
    f = init_dir+'/{}_{}_bcl_{:.1f}_AP_traces.dat'.format(dkey, model, bcl)
    return f

def print_init_summary(domains, cycles):
    
    print('Single cell limit cycle initialization completed.')
    print('='*88+'\n')

    print('{:18s} : {:8s} {:6s} {}'.format('cell', 'bcl [ms]', 'cycles', 'init vector'))
    print('-'*88)



    for dkey, domain in domains.items():

        ep = domain.get('EP')
    
        # skip domains without EP definitions
        if ep is None:
            print('\nNo EP defined for domain {} !\n'.format(dkey))
            continue

            ep['bcl'] = args.bcl
        
        mutable = ep.get('mutable')

        if mutable:
            print('{:18s} : {:5.1f} ms {:6d} {}'.format(dkey, ep.get('bcl'), cycles, ep.get('init')))


# initialize cell, launch limit cycle pacing experiment for a given cell
def init_cell(ep, cycs, job, lim_cyc_dir, dkey, visual):

    model = ep.get('model')
    bcl   = ep.get('bcl')

    # assemble name of file for storing initial state vector 
    init_file  = get_sv_init_file_name(dkey, model, bcl, lim_cyc_dir)
    ep['init'] = init_file

    # configure cellular dynamics
    plg     = ep.get('plugs')
    plg_par = ep.get('plugspar')
    imp_par = ep.get('modelpar')

    cell_dyn = [ '--imp', ep.get('model') ]
    if imp_par and len(imp_par) > 0:
        cell_dyn += ['--imp-par', imp_par]

    if plg and len(plg) > 0:
        cell_dyn += ['--plug-in', plg_par]

    if plg_par and len(plg_par) > 0:
        cell_dyn += ['--plug-par', plg_par]
     

    # configure bench
    duration = cycs * bcl
    cmd = [ settings.execs.BENCH ] + cell_dyn
    cmd+= [ '--bcl',                bcl,
            '--stim-curr',          40,
            '--stim-dur',           1.,
            '--numstim',            cycs,
            '--duration',           duration,
            '--stim-start',         0,
            '--dt',                 20/1000,
            '--fout='               + job.ID + '/{}_limit_cycle'.format(dkey),
            '-S',                   duration,
            '-F',                   init_file,
            '--no-trace',           'on']

    if visual:
        cmd += ['--dt-out', 1.0,
                '--validate']
    else:
        cmd += ['--dt-out', duration]

    job.bash(cmd)

    return ep

# create dictionary entry describing a restitution protocol
def create_restitution_prtcl(prtclID, bcl, CI0, CI1, CIinc, nbeats, prebeats):
    p = {}
    if 'S1S2' in prtclID:
        p['type'] = 'S1S2'

    elif 'dyn' in prtclID:
        p['type'] = 'dynamic'
    else:
        print('Unknown restitution protocol type \'{}\' specified, switching to default S1S2 protocol.'.format(
            p.get('type')))
        p['type'] = 'S1S2'

    p['pp_beats'] = prebeats
    p['bcl'] = bcl
    p['CI0'] = CI0
    p['CI1'] = CI1
    p['pm_beats'] = nbeats
    p['pm_dec'] = CIinc

    prtcl_dict = {}
    prtcl_dict[prtclID] = p

    return prtcl_dict


# measured cell restitute restitution
def restitute_cell(ep, rptcls, job, lim_cyc_dir, dkey, visual, dry):

    model = ep.get('model')
    bcl   = ep.get('bcl')

    # assemble name of file for storing initial state vector 
    init_file  = get_sv_init_file_name(dkey, model, bcl, lim_cyc_dir)
    ep['init'] = init_file

    # configure cellular dynamics
    imp     = ep.get('model')
    plg     = ep.get('plugs')
    plg_par = ep.get('plugspar')
    imp_par = ep.get('modelpar')

    cell_dyn = [ '--imp', imp ]
    if imp_par and len(imp_par) > 0:
        cell_dyn += ['--imp-par', imp_par]

    if plg and len(plg) > 0:
        cell_dyn += ['--plug-in', plg_par]

    if plg_par and len(plg_par) > 0:
        cell_dyn += ['--plug-par', plg_par]

    # carry out restitution experiment
    rp = ep.get('apdresptcl')
    if rp:
        rp_spec = get_apd_restitution_prtcl(rp, rptcls )

        if rp_spec:
            # unpack apd protocol restitution parameters
            print('\nUsing APD restitution protocol {}.\n\n'.format(rp))
            ptcl    = rp_spec.get('type')
            CI0     = rp_spec.get('CI0')
            CI1     = rp_spec.get('CI1')
            CIinc   = rp_spec.get('pm_dec')
            nbeats  = rp_spec.get('pp_beats')
            bcl     = rp_spec.get('bcl')
            pbeats  = rp_spec.get('pm_beats')

    else:
        # use default settings, issue warning
        print('\nAPD restitution protocol {} not found, using default settings.\n\n'.format(rp))
        CI0 = 50
        CI1 = 600
        CIinc = 25
        nbeats = 5
        bcl = 600
        ptcl = 'S1S2'
        pbeats = 20
    
    # read in limit cycle state vector
    sv_init = get_sv_init_file_name(dkey, ep.get('model'), ep.get('bcl'), lim_cyc_dir)

    print('Running restitution experiments to determine APD(DI)')
    r.restitute(job, ep.get('model'), imp_par, plg, plg_par, CI0, CI1, CIinc, nbeats, bcl, ptcl, pbeats, sv_init, dry)

    # move APD restitution directory to avoid overwriting
    src_dir =  os.path.join(job.ID)
    trg_dir = os.path.join('{}_{}'.format(job.ID, dkey))
    trg_apdfile = get_apdres_file_name(dkey, ep.get('model'), ep.get('bcl'), lim_cyc_dir)
    trg_apbeats_file = get_apbeats_file_name(dkey, ep.get('model'), ep.get('bcl'), lim_cyc_dir)

    if not dry:
        # copy output restitution data to target directory
        shutil.copytree(src_dir, trg_dir, dirs_exist_ok=True)
    
        # copy APD restitution output to init directory
        # pass back final apd restitution file for plotting
        src_apdfile = os.path.join(trg_dir, 'restout_APD_restitution.dat')
        shutil.copy(src_apdfile, trg_apdfile)

        # same for AP Vm trace output
        src_apbeats_file = os.path.join(trg_dir, 'restout.txt')
        shutil.copy(src_apbeats_file, trg_apbeats_file)

    ep['apdres'] = trg_apdfile

    return trg_apdfile, trg_apbeats_file


def ep_limit_cycle(job, exps, bcl, cycles, lim_cyc_dir, domain_lst, visual, ignore_mutable):

    # compute limit cycle or restitution for each region, if mutable flag is set
    domains = exps['functions']
    n_acts  = 0
    for dkey, domain in domains.items():


        if domain_lst is not None and dkey not in domain_lst:
            continue

        ep = domain.get('EP')
        # skip domains without EP definitions
        if ep is None:
            print('\nNo EP defined for domain {} !\n'.format(dkey))
            continue

        # select bcl and #cycles for limit cycle experiment
        if bcl is not None:
            #print('Overruling bcl settings given at command line in {:s}.'.format(args.experiment))
            ep['bcl'] = bcl

        if cycles is not None:
            ep['cycles'] = cycles

       
        # dealing with mutable flag
        mutable = j2c.is_mutable(ep,ignore_mutable)

        print('\nDomain {:s}'.format(dkey))
        print('-'*(len(dkey)+7))

        if mutable:

            print('\nLimit cycle at bcl {:.1f} ms for {:d} cycles.\n'.format(ep.get('bcl'), cycles))

            n_acts += 1
            ep = init_cell(ep, cycles, job, lim_cyc_dir, dkey, visual)

        else:

            # no limit cycle pacing, check for valid initial state vector
            init_sv_file = ep.get('init')

            if init_sv_file: 

                print('\nNo limit cycle pacing, use pre-existing initial state vector {:s}.'.format(init_sv_file))
    
                # provided init file not modified, but make sure that name is consistent
                sv_init = get_sv_init_file_name(dkey, ep.get('model'), ep.get('bcl'), lim_cyc_dir)
    
                # if file exist, rename to ensure consistent naming
                if init_sv_file != sv_init and os.path.isfile(init_sv_file):
                    print('Rename init file {:s} to {:s}.'.format(init_sv_file, sv_init))
                    os.rename(init_sv_file, sv_init)
                print('\n\n')

            else:
                # init file string was null or empty
                print('No valid initial state vector provided!')

    return n_acts, exps

def get_apd_restitution_prtcl_sec(exps):

    # check whether we have protocol descriptions
    apd_res_prtcl_sec = None

    # if protocol given as arg, overrule all other settings
    prtcls = exps.get('protocols')
    if prtcls:
        # Are there APD restitution protocols?
        apd_res_prtcl_sec = prtcls.get('APD-restitution')

    return apd_res_prtcl_sec


def get_apd_restitution_prtcl(p, prtcls):

    # retrieve parameters of specific restitution protocol, if given
    apd_res_prtcl = prtcls.get(p)
    if apd_res_prtcl:
        print('Using APD restitution protocol {}.'.format(p))
    else:
        print('Definition of specified protocol {} not found, using default settings.'.format(p))

    return apd_res_prtcl

def ep_apd_restitution(job, exps, bcl, cycles, lim_cyc_dir, domain_lst, visual, ignore_mutable, dry):

    # compute limit cycle or restitution for each region, if mutable flag is set
    domains = exps.get('functions')
    prtcls  = get_apd_restitution_prtcl_sec(exps)

    n_acts  = 0
    apdResFiles = []
    apdLabels   = []
    apResBeatFiles = []
    for dkey, domain in domains.items():

        # if domain list provided, skip any domain not in list
        if domain_lst is not None and dkey not in domain_lst:
            continue

        ep = domain.get('EP')
        # skip domains without EP definitions
        if ep is None:
            print('\nNo EP defined for domain {} !\n'.format(dkey))
            continue

        # select bcl and #cycles for limit cycle experiment
        if bcl is not None:
            #print('Overruling bcl settings given at command line in {:s}.'.format(args.experiment))
            ep['bcl'] = bcl

        if cycles is not None:
            ep['cycles'] = cycles

        # dealing with mutable flag
        mutable = j2c.is_mutable(ep, ignore_mutable)

        print('\nDomain {:s}'.format(dkey))
        print('-'*(len(dkey)+7))

        if mutable:

            n_acts += 1
            apdf, apbeatsf = restitute_cell(ep, prtcls, job, lim_cyc_dir, dkey, visual, dry)
            apdResFiles += [apdf]
            apdLabels += [dkey]
            apResBeatFiles += [apbeatsf]

            # read out apd(n), di(n-1)
            apdTbl = r.readAPDrestitutionFile(apdf, flt='res-curve')

            # add restitution curve to json
            ep['DI' ] = apdTbl['DIp']
            ep['APD'] = apdTbl['APD']

    return n_acts, exps, apdResFiles, apdLabels, apResBeatFiles


