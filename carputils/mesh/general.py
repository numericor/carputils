#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the
# Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Base framework for the mesh generation classes.

This module contains the core framework used by the other mesh generation
modules, and implements shared functionality including the tessellation of
larger elements into tetrahedra, fibre field generation and mesh file output.

For information on the detail of the implementation of this module and other
derived classes, see the meshing section of the carputils documentation.
"""

from __future__ import division, print_function, absolute_import, unicode_literals
from sys import version_info
import numpy as np

# Python2 - Python3 compatibility
try:
    from itertools import izip as zip
except ImportError:
    pass
if version_info.major > 2:
    xrange = range

# To be used internally to ensure elements are always handled in the same order
ELEM_ORDER = ['hexa', 'prism1', 'prism2', 'tetra', 'quad', 'tri', 'line']

ELEM_NUM_NODES = {
    'hexa':   8,
    'prism1': 6,
    'prism2': 6,
    'tetra':  4,
    'quad':   4,
    'tri':    3,
    'line':   2
}

# Local node indicies to subdivide various element types into tetrahedra
TET_SUBDIVISIONS = {
    'hexa':   [[0, 1, 3, 5],
               [0, 3, 4, 5],
               [3, 4, 5, 7],
               [0, 3, 2, 4],
               [2, 4, 3, 7],
               [2, 4, 7, 6]],
    'prism1': [[0, 1, 3, 5],
               [0, 3, 2, 5],
               [0, 5, 2, 4]],
    'prism2': [[0, 1, 3, 5],
               [0, 3, 2, 4],
               [0, 5, 3, 4]],
    'tetra':  [[0, 1, 2, 3]]
}

# Local node indicies to subdivide various element types into triangles
TRI_SUBDIVISIONS = {
    'quad': [[0, 1, 2],
             [1, 3, 2]]
}

# Local node indicies to select faces from different element types
FACE_SELECTIONS = {
    'hexa': {
        'front':  [0, 2, 4, 6],
        'back':   [1, 5, 3, 7],
        'top':    [4, 6, 5, 7],
        'bottom': [0, 1, 2, 3],
        'left':   [2, 3, 6, 7],
        'right':  [1, 0, 5, 4]
    },
}
prism_faces = {
    'front':      [0, 2, 4],
    'back':       [1, 5, 3],
    'base':       [2, 0, 3, 1],
    'upperleft':  [4, 2, 5, 3],
    'upperright': [0, 4, 1, 5]
}
FACE_SELECTIONS['prism1'] = prism_faces
FACE_SELECTIONS['prism2'] = prism_faces

# Local node indicies to select faces of the tetrahedra generated from these
# elements using TET_SUBDIVISIONS above. The first value in any pair refers to
# the index of the tetrahedron in the corresponding TET_SUBDIVISIONS entry
# that this face corresponds to.
TET_FACE_SELECTIONS = {
    'hexa': {
        'front':  [(3, [0, 4, 2]), (5, [2, 4, 6])],
        'back':   [(0, [1, 3, 5]), (2, [5, 3, 7])],
        'top':    [(5, [4, 7, 6]), (2, [4, 5, 7])],
        'bottom': [(3, [0, 2, 3]), (0, [0, 3, 1])],
        'left':   [(5, [2, 6, 7]), (4, [2, 7, 3])],
        'right':  [(0, [1, 5, 0]), (1, [0, 5, 4])]
    },
    'prism1': {
        'front':      [(2, [0, 4, 2])],
        'back':       [(0, [1, 3, 5])],
        'base':       [(0, [0, 3, 1]), (1, [0, 2, 3])],
        'upperleft':  [(1, [2, 5, 3]), (2, [2, 4, 5])],
        'upperright': [(0, [0, 1, 5]), (2, [0, 5, 4])]
    },
    'prism2': {
        'front':      [(1, [0, 4, 2])],
        'back':       [(0, [1, 3, 5])],
        'base':       [(0, [0, 3, 1]), (1, [0, 2, 3])],
        'upperleft':  [(1, [2, 4, 3]), (2, [3, 4, 5])],
        'upperright': [(0, [0, 1, 5]), (2, [0, 5, 4])]
    }
}

# How to format an element in openCARP
CARP_ELEM_FORMAT = {
    'hexa':   'Hx {0[5]} {0[4]} {0[6]} {0[7]} {0[1]} {0[3]} {0[2]} {0[0]}',
    'prism1': 'Pr {0[1]} {0[3]} {0[5]} {0[0]} {0[4]} {0[2]}',
    'prism2': 'Pr {0[1]} {0[3]} {0[5]} {0[0]} {0[4]} {0[2]}',
    'tetra':  'Tt {0[0]} {0[1]} {0[2]} {0[3]}',
    'quad':   'Qd {0[0]} {0[1]} {0[3]} {0[2]}',
    'tri':    'Tr {0[0]} {0[2]} {0[1]}',
    'line':   'Ln {0[0]} {0[1]}'
}

# How to reorder internal node ordering to VTK node ordering
VTK_HEX_REORDER = [0, 1, 3, 2, 4, 5, 7, 6]
VTK_PRISM_REORDER = [0, 2, 4, 1, 3, 5]
VTK_QUAD_REORDER = [0, 1, 3, 2]

# VTK cell type indicies for element types used here.
VTK_CELL_TYPES = {
    'hexa':   12,
    'prism1': 13,
    'prism2': 13,
    'tetra':  10,
    'quad':    9,
    'tri':     5,
    'line':    3
}

class Mesh(object):
    """
    A generic mesh class the provides the core functionality in 1D and 3D.
    """

    def __init__(self):

        self._ptsarray = None
        self._mixedelem = None
        self._lonarray = None
        self._element_centres_cache = None

        # ID definitions
        # IDs default to 1, unless the element centroids lie inside one of
        # the regions defined in self._ID_regions
        self._tag_regions = {}

    def add_region(self, tag, func):
        """
        Add a new tag region.

        Tags are calculated by testing the centroid of each element with func.

        Parameters
        ----------
        tag : int
            Tag value of the region
        func : callable
            Function returning True when a passed point is inside the region,
            or an instance of one of :class:`carputils.mesh.BoxRegion`,
            :class:`carputils.mesh.SphereRegion` or
            :class:`carputils.mesh.CylinderRegion.`
        """
        self._tag_regions[int(tag)] = func

    def _generate_points(self):
        """
        Reimplement in subclasses to generate points.

        The implemented version of this method should generate a numpy array of
        coordinates and store it to self._ptsarray.
        """
        raise NotImplementedError

    def points(self):
        """
        Generate and return numpy array of point coordinates.

        Returns
        -------
        array
            Mesh point coordinates, shape (npoint, 3)
        """

        # Generate the points if not done already
        if self._ptsarray is None:
            self._generate_points()

        return self._ptsarray

    def n_pts(self):
        """
        Return the number of points/nodes in the mesh.
        """
        return self.points().shape[0]

    def _generate_elements(self):
        """
        Reimplement in subclasses to generate elements and faces.

        The implemented version of this method should generate numpy arrays of
        element node lists for each element type, and store it to
        self._mixedelem as a dict whose keys are one of ELEM_ORDER above and
        values are these arrays.

        Each element that borders on a face to be defined (which should exist
        in SELECTIONS above) should be recorded in self._mixedelem_selections,
        which should be a dictionary whose keys are the same element type from
        ELEM_ORDER as above and whose values are numpy arrays containing a list
        of per-element-type element index, meaning the zero-indexed row that
        the element appears in in its self._mixedelem[elemtype] array.
        """
        raise NotImplementedError

    def elements(self):
        """
        Generate and return list of numpy arrays of element node indices.

        Returns
        -------
        list of arrays
            List of arrays of shape (nelem, nlocalnode) for each hybrid element
            type in the generated mesh
        """

        # Genertate the elements if not done already
        if self._mixedelem is None:
            self._generate_elements()
            for etype in self._mixedelem:
                self._mixedelem[etype] = np.array(self._mixedelem[etype])

        # Build a list of element arrays according to the order set out in
        # ELEM_ORDER
        data = []
        for etype in ELEM_ORDER:
            if etype in self._mixedelem:
                elem = self._mixedelem[etype]
                if elem.size > 0:
                    data.append((etype, elem))

        return data

    def n_elem(self):
        """
        Count and return the total number of elements in the mesh.

        Returns
        -------
        int
            The number of elements.
        """
        count = 0
        # Iterate over all element types
        for _, elems in self.elements():
            count += elems.shape[0]
        return count

    def element_centres(self):
        """
        Calculate the centroids of the mesh elements.

        Returns
        -------
        array
            Centroids of elements, shape (nelem, 3)
        """

        if self._element_centres_cache is not None:
            return self._element_centres_cache

        pts = self.points()
        elem_parts = self.elements()
        part_centres = [pts[elem].mean(axis=1) for _, elem in elem_parts]
        self._element_centres_cache = np.concatenate(part_centres, axis=0)
        return self._element_centres_cache

    def element_tags(self):
        """
        Determine element tags based on rules passed to :meth:`add_region`.

        Elements without an associated region default to tag 1.

        Returns
        -------
        array
            tags of elements, shape (nelem,)
        """

        # Get centroids of all elements
        centres = self.element_centres()

        # Default all tags to 1
        tags = np.ones(centres.shape[0], dtype=int)

        # Loop over all defined regions
        for tag in sorted(self._tag_regions.keys()):

            # Get the function determining if it's in the region
            in_region = self._tag_regions[tag]

            # Loop over all elements and assign the region tag to appropriate
            # elements
            for i, coord in enumerate(centres):
                if in_region(coord):
                    tags[i] = tag

        return tags

    def _calc_fibres(self):
        """
        Reimplement in subclasses to generate element-wise fibre vectors.

        Fibres should be stored as a numpy array to self._lonarray
        """
        raise NotImplementedError

    def fibres(self):
        """
        Generate numpy array of fibre vectors and return.

        Returns
        -------
        array
            Fibre direction vectors
        """

        # Generate fibres if not done already
        if self._lonarray is None:
            self._calc_fibres()

        return self._lonarray

    def _generate_carp_pts(self, filename):
        """
        Generate the openCARP points file.

        Parameters
        ----------
        filename : str
            The pts file name
        """

        # Get points and convert from mm to um
        pts = self.points() * 1000.0

        with open(filename, 'w') as f_p:
            # Header
            f_p.write('{}\n'.format(pts.shape[0]))
            # Content
            for coord in pts:
                f_p.write('{0[0]} {0[1]} {0[2]}\n'.format(coord))

    def _generate_carp_elem(self, filename):
        """
        Generate the openCARP element file.

        Parameters
        ----------
        filename : str
            The elem file name
        """

        # Get element tags
        elem_tags = self.element_tags()

        with open(filename, 'w') as f_p:

            # Header
            f_p.write('{}\n'.format(self.n_elem()))

            # Content
            i_elem = 0 # Count as we go

            # Loop over element types
            for etype, part in self.elements():

                # Get the element type's format string
                fmt = CARP_ELEM_FORMAT[etype] + ' {1}\n'

                # Write the elements of this type
                for plist in part:
                    tag = elem_tags[i_elem]
                    i_elem += 1
                    f_p.write(fmt.format(plist, tag))

    def _generate_carp_lon(self, filename, lon):
        """
        Generate the openCARP fibrefile.

        Parameters
        ----------
        filename : str
            The lon file name
        """

        # Get templates
        dat = {3: ('1\n', '{0[0]} {0[1]} {0[2]}\n'),
               6: ('2\n', '{0[0]} {0[1]} {0[2]} {0[3]} {0[4]} {0[5]}\n'),
               9: ('3\n', '{0[0]} {0[1]} {0[2]} {0[3]} {0[4]} {0[5]} {0[6]} {0[7]} {0[8]}\n')}
        hdr, line = dat[lon.shape[1]]

        with open(filename, 'w') as f_p:
            # Header
            f_p.write(hdr)
            # Content
            for vec in lon:
                f_p.write(line.format(vec))

    def generate_carp(self, meshname):
        """
        Generate the mesh and store it to disk in openCARP format.

        Simply pass the basename to store the mesh to:

        >>> geom.generate_carp('mesh/new')

        to save the mesh to mesh/new.pts, mesh/new.elem and mesh/new.lon.

        Parameters
        ----------
        meshname : str
            The base name (without extension) of the mesh to be generated.
        """

        self._generate_carp_pts('{}.pts'.format(meshname))
        self._generate_carp_elem('{}.elem'.format(meshname))
        self._generate_carp_lon('{}.lon'.format(meshname), self.fibres())

    def generate_vtk(self, filename):
        """
        Generate VTK file of mesh.

        Parameters
        ----------
        filename : str
            Filename to write out to
        """

        f_p = open(filename, 'w')

        # Header
        f_p.write('# vtk DataFile Version 3.0\n')
        f_p.write('Auto generated ring model\n')
        f_p.write('ASCII\n')
        f_p.write('DATASET UNSTRUCTURED_GRID\n')

        # Write points
        pts = self.points()
        f_p.write('POINTS {} double\n'.format(pts.shape[0]))
        for point in pts:
            f_p.write('{0[0]} {0[1]} {0[2]}\n'.format(point))

        # Write elements
        elem_groups = self.elements()
        n_elem = 0
        n_vals = 0
        for _, elems in elem_groups:
            n_elem += elems.shape[0]
            n_vals += elems.shape[0] * (elems.shape[1] + 1)

        f_p.write('CELLS {} {}\n'.format(n_elem, n_vals))
        for etype, elems in elem_groups:
            for elem in elems:
                f_p.write(str(len(elem)) + ' ')
                if etype == 'hexa':
                    elem = elem[VTK_HEX_REORDER]
                elif etype.startswith('prism'):
                    elem = elem[VTK_PRISM_REORDER]
                f_p.write(' '.join([str(i) for i in elem]) + '\n')

        # Write cell types
        f_p.write('CELL_TYPES {}\n'.format(n_elem))
        for etype, elems in elem_groups:
            celltype = '{}\n'.format(VTK_CELL_TYPES[etype])
            for i in xrange(elems.shape[0]):
                f_p.write(celltype)

        # Write out some data for visualisation
        f_p.write('CELL_DATA {}\n'.format(n_elem))

        # Write IDs
        elem_tags = self.element_tags()
        f_p.write('SCALARS IDs int 1\n')
        f_p.write('LOOKUP_TABLE default\n')
        for tag in elem_tags:
            f_p.write('{0}\n'.format(tag))

        # Write fibres
        lon = self.fibres()
        f_p.write('VECTORS fibres float\n')
        for vec in lon:
            f_p.write('{0[0]} {0[1]} {0[2]}\n'.format(vec))

        # Write sheets
        if lon.shape[1] >= 6:
            f_p.write('VECTORS sheets float\n')
            for vec in lon:
                f_p.write('{0[3]} {0[4]} {0[5]}\n'.format(vec))

        f_p.close()

class Mesh2D(Mesh):
    """
    A generic mesh class adding the core functionality for 2D meshes.

    Parameters
    ----------
    triangulate : bool, optional
        Whether to divide the hybrid mesh into tetrahedra
    """

    def __init__(self, triangulate=False):

        # Initialise core mesh attributes
        Mesh.__init__(self)

        # Store constructor arguments
        self._triangulate = bool(triangulate)

        # Initialise some 2D-specific properties
        self._triarray = None

    def _generate_tris(self):
        """
        Subdivide the mesh into triangles.
        """

        # Ensure the hybrid data structures have been generated
        if self._mixedelem is None:
            self._generate_elements()
            for etype in self._mixedelem:
                self._mixedelem[etype] = np.array(self._mixedelem[etype])

        tris = []

        # Loop over all defined element types
        for etype in ELEM_ORDER:

            # Do nothing if no elements of this type defined
            if etype not in self._mixedelem:
                continue

            # Loop over all elements and subdivide into triangles
            for _, elem in enumerate(self._mixedelem[etype]):
                for subdivisions in TRI_SUBDIVISIONS[etype]:
                    # Get the local nodes for this tri and store
                    tris.append(elem[subdivisions])

        # Convert to numpy arrays for convenient slicing
        self._triarray = np.array(tris)

    def elements(self):
        """
        Generate and return array of element point indices.

        Returns
        -------
        list of arrays
            List of arrays of shape (nelem, nlocalnode) for each hybrid element
            type in the generated mesh
        """

        # Return triangles, if that option was set
        if self._triangulate:

            # Generate tets if not done so already
            if self._triarray is None:
                self._generate_tris()
            return [('tri', self._triarray)]

        else:
            # Otherwise, generate and return hybrid mesh
            return Mesh.elements(self)

class Mesh3D(Mesh):
    """
    A generic mesh class adding the core functionality for 3D meshes.

    Parameters
    ----------
    fibre_rule : callable, optional
        Function describing the transmural variation of fibre angle, takes a
        single normalised transmural distance on [0,1] and returns a helix
        angle in radians, defaults to circumferentially oriented fibres
    fibre_rule_2: callable, optional
        Function describing a second fiber rule
    tetrahedrise: bool, optional
        Whether to divide the hybrid mesh into tetrahedra
    """

    def __init__(self, fibre_rule=lambda t: 0.0, fibre_rule_2=None,
                 tetrahedrise=True):

        # Initialise core mesh attributes
        Mesh.__init__(self)

        # Store constructor arguments
        self._tetrahedrise = bool(tetrahedrise)
        self._fibre_rule_1 = fibre_rule
        self._fibre_rule_2 = fibre_rule_2

        # Initialise some 3D-specific properties
        self._tetarray = None
        self._mixedelem_selections = None
        self._mixedfaces = None
        self._tetfaces = None

    def _clear_mixedelem_data(self):
        """
        Convenience method clearing and initialising the hybrid data structure.
        """

        # Initialise element lists
        self._mixedelem = {k: [] for k in TET_SUBDIVISIONS.keys()}

        # Initialise element selections
        self._mixedelem_selections = {}

    def _generate_tets(self):
        """
        Subdivide the hybrid mesh into tetrahedrons.
        """

        # Ensure the hybrid data structures have been generated
        if self._mixedelem is None:
            self._generate_elements()
            for etype in self._mixedelem:
                self._mixedelem[etype] = np.array(self._mixedelem[etype])

        # Initialise the tetrahedral element list and face selections
        tets = []
        #faces = {sel: {'parents': [], 'nodes': []} for sel in SELECTIONS}
        faces = {}

        # Loop over all defined element types
        for etype in ELEM_ORDER:

            try:
                elems = self._mixedelem[etype]
            except KeyError:
                # No elements of this type
                continue

            # Prefetch selections
            try:
                selections = self._mixedelem_selections[etype]
            except KeyError:
                # No selections defined for this element type
                selections = {}

            # Loop over all elements, subdivide into tets and store
            # corresponding tet faces
            for i, elem in enumerate(elems):

                # Loop over all selection types
                for sel_name, sel in selections.items():

                    for side, elemlist in sel.items():

                        # Is this element in this selection?
                        if i in elemlist:

                            try:
                                facesel = faces[sel_name]
                            except KeyError:
                                facesel = {'parents': [], 'nodes': []}
                                faces[sel_name] = facesel

                            # Then store all corresponding faces
                            face_list = TET_FACE_SELECTIONS[etype][side]

                            for local_tet, local_nodes in face_list:

                                # Get the index that the corresponding tet will
                                # have
                                tri_elem = len(tets) + local_tet

                                # Get the nodes of this face
                                tri_nodes = elem[local_nodes]

                                # Store the parent element and node list
                                facesel['parents'].append(tri_elem)
                                facesel['nodes'].append(tri_nodes)

                # Handle elements
                for subdivisions in TET_SUBDIVISIONS[etype]:
                    # Get the local nodes for this tet and store
                    tets.append(elem[subdivisions])

        # Convert everything to numpy arrays for convenient slicing
        self._tetarray = np.array(tets)
        self._tetfaces = {sel: {k: np.array(a) for k, a in dat.items()}
                          for sel, dat in faces.items()}

    def elements(self):
        """
        Generate and return array of element point indices.

        Returns
        -------
        list of arrays
            List of arrays of shape (nelem, nlocalnode) for each hybrid element
            type in the generated mesh
        """

        # Return tetrahedra, if that option was set
        if self._tetrahedrise:

            # Generate tets if not done so already
            if self._tetarray is None:
                self._generate_tets()
            return [('tetra', self._tetarray)]

        else:
            # Otherwise, generate and return hybrid mesh
            return Mesh.elements(self)

    def _register_face(self, etype, side, selection=None, eindex=None):
        """
        Register the face of an element.

        Parameters
        ----------
        etype : str
            The parent element type
        side : str
            The side of the element to extract the face from
        selection : str, optional
            The selection to assign the face to - by default the side
        eindex : int, optional
            The index of the parent element within its element type - by default
            the last element of the specified type is assumed
        """

        assert etype in TET_FACE_SELECTIONS.keys()

        if selection is None:
            selection = side

        if eindex is None:
            eindex = len(self._mixedelem[etype]) - 1

        try:
            selections = self._mixedelem_selections[etype]
        except KeyError:
            selections = {}
            self._mixedelem_selections[etype] = selections

        try:
            sel = selections[selection]
        except KeyError:
            sel = {}
            selections[selection] = sel

        try:
            sidesel = sel[side]
        except KeyError:
            sidesel = []
            sel[side] = sidesel

        sidesel.append(eindex)

    def _generate_faces(self):
        """
        Generate and store the faces of the hybrid mesh.
        """

        # Generate the hybrid mesh if not done so already
        if self._mixedelem is None:
            self._generate_elements()
            for etype in self._mixedelem:
                self._mixedelem[etype] = np.array(self._mixedelem[etype])

        # 'Face' data structure is:
        # First index is selection key
        # Second index is face type
        # Third index is for either the 'parents' list, which notes the global
        # element index of the tet this face belongs to, or the 'nodes' list,
        # which contains the local-to-global node map for each face
        faces = {}

        # Count elements as we go
        eindex_start = 0

        # Loop over elements in consistent order
        for etype in ELEM_ORDER:

            try:
                elems = self._mixedelem[etype]
            except KeyError:
                # No elements of this type
                continue

            try:
                selections = self._mixedelem_selections[etype]
            except KeyError:
                # No selections defined for this element type
                continue

            # Loop over all selection types
            for sel_name, sel in selections.items():

                for side, elemlist in sel.items():

                    # i is element index within the local block
                    for i in elemlist:

                        # Get the local nodes that define this face
                        face_nodes = FACE_SELECTIONS[etype][side]

                        # None means no relevant face here!
                        if face_nodes is None:
                            continue

                        try:
                            facesel = faces[sel_name]
                        except KeyError:
                            facesel = {}
                            faces[sel_name] = facesel

                        # Determine face type from number of nodes
                        ftype = {3: 'tri', 4: 'quad'}[len(face_nodes)]

                        try:
                            faceseltype = facesel[ftype]
                        except KeyError:
                            faceseltype = {'parents': [], 'nodes': []}
                            facesel[ftype] = faceseltype

                        # Store face in correct location
                        faceseltype['nodes'].append(elems[i][face_nodes])
                        # Store the parent element of this face
                        faceseltype['parents'].append(eindex_start + i)

            eindex_start += self._mixedelem[etype].shape[0]

        # Convert data to numpy arrays for convenient slicing
        self._mixedfaces = {}
        for sel, sel_dat in faces.items():
            self._mixedfaces[sel] = {}
            for ftype, dat in sel_dat.items():
                self._mixedfaces[sel][ftype] = {}
                for key, val in dat.items():
                    self._mixedfaces[sel][ftype][key] = np.array(val)

    def faces(self, selection, return_elem=False):
        """
        Generate an array of faces on the specified surface.

        Parameters
        ----------
        selection : str
            One of ['top', 'bottom', 'inside', 'outside']
        return_elem : bool, optional
            True to return an array of faces' parent element along with the
            face node indicies array

        Returns
        -------
        face : array
            Indices of each selected face
        elem : int, when return_elem == True
            The parent to which this face blongs
        """

        # Return tet faces instead of hybrid element faces, when set
        if self._tetrahedrise:
            # Generate tetrahedral faces if not done so already
            if self._tetfaces is None:
                self._generate_tets()
            dat = {'tri': self._tetfaces[selection]}

        else:
            # Generate hybrid element faces if not done so already
            if self._mixedfaces is None:
                self._generate_faces()
            dat = self._mixedfaces[selection]

        # Return data in requested format
        if return_elem:
            return [(ftype, d['nodes'], d['parents'])
                    for ftype, d in dat.items() if len(d['parents']) > 0]
        # else
        return [(ftype, d['nodes'])
                for ftype, d in dat.items() if len(d['parents']) > 0]

    def n_face(self, surface):
        """
        Return the number of faces in a given surface.

        Returns
        -------
        int
            The number of faces
        """
        count = 0
        for _, face_array in self.faces(surface):
            count += face_array.shape[0]
        return count

    def face_centres(self, selection):
        """
        Calculate the centroids of faces on the specified surface.

        Parameters
        ----------
        selection : str
            One of ['top', 'bottom', 'inside', 'outside']

        Returns
        -------
        array
            Centres of the surface faces
        """
        pts = self.points()
        face_parts = self.faces(selection)
        part_centres = [pts[faces].mean(axis=1) for _, faces in face_parts]
        return np.concatenate(part_centres, axis=0)

    def face_normals(self, selection, unit=True):
        """
        Calculate the normals to faces on the specified surface.

        Parameters
        ----------
        selection : str
            One of ['top', 'bottom', 'inside', 'outside']
        unit : bool
            True to normalise vectors before returning, default is True

        Returns
        -------
        array
            Normals to faces on the specified surface
        """

        pts = self.points()
        cross = []

        # For each face type
        for _, faces in self.faces(selection):

            # Get the coordinate of the nodes on this face
            facept_coords = pts[faces]

            # Get two vectors on this face
            vec1 = facept_coords[:, 1, :] - facept_coords[:, 0, :]
            vec2 = facept_coords[:, 2, :] - facept_coords[:, 0, :]

            # Get a normal vector to this face
            # Scaled by 0.5 such that it correspond to the face area, at least
            # when a triangle
            cross.append(0.5 * np.cross(vec1, vec2))

        # Convert to one big array
        cross = np.concatenate(cross, axis=0)

        # Normalise
        if unit:
            mag = np.sqrt((cross**2).sum(axis=1))
            cross /= mag[:, np.newaxis]

        return cross

    def face_areas(self, selection):
        """
        Calculate the areas of faces on the specified surface.

        Parameters
        ----------
        selection : str
            One of ['top', 'bottom', 'inside', 'outside']

        Returns
        -------
        array
            Areas of faces on the specified surface
        """
        cross = self.face_normals(selection, unit=False)
        return np.sqrt((cross**2).sum(axis=1))

    def _element_transmural_distance(self):
        """
        Reimplement to return normalised transmural depth for fibre generation.

        Should vary from 0 on the endocardium to 1 on the epicardium
        """
        raise NotImplementedError

    def _element_circum_direction(self):
        """
        Reimplement to return circumferential direction vec for each element.
        """
        raise NotImplementedError

    def _element_apical_basal_direction(self):
        """
        Reimplement to return apical to basal direction vec for each element.
        """
        raise NotImplementedError

    def _calc_fibre_direction(self, fibre_rule):
        """
        Calculate one element-wise fibre direction.

        Generate fibre orientation for element centres, using the rule
        provided as the fibre_rule argument to the class constructor. The rule
        argument expects a function providing an angle from the circumferential
        direction, in radians.

        Args
            fibre_rule: rule to generate fiber orientation
        Returns
            Fibre direction vector
        """

        # Evaluate the depth and basis for fibre vector calculation
        depth = self._element_transmural_distance()
        circum = self._element_circum_direction()
        apicbasal = self._element_apical_basal_direction()

        # Make sure basis is orthogonal
        normal = np.cross(circum, apicbasal)
        apicbasal = np.cross(normal, circum)

        # Make sure basis is orthonormal
        circum_norm = np.sqrt((circum**2).sum(axis=1))
        circum /= circum_norm[:, np.newaxis]
        apicbasal_norm = np.sqrt((apicbasal**2).sum(axis=1))
        apicbasal /= apicbasal_norm[:, np.newaxis]

        # Get fibre angle
        angle = np.vectorize(fibre_rule)(depth)

        # Get components of fibre vetor
        circ_comp = circum * np.cos(angle)[:, np.newaxis]
        apba_comp = apicbasal * np.sin(angle)[:, np.newaxis]

        # Combine components to get fibre vector
        fibre = circ_comp + apba_comp

        return fibre

    def _calc_fibres(self):
        """
        Calculate element-wise fibre directions.

        Generate fibre orientations for element centres, using the rule
        provided as the fibre_rule argument to the class constructor. The rule
        argument expects a function providing an angle from the circumferential
        direction, in radians.

        Returns
        -------
        array
            Fibre direction vectors
        """
        # Get first fibre vector
        fibre = self._calc_fibre_direction(self._fibre_rule_1)

        if self._fibre_rule_2:   # compute sheet direction with second rule
            sheet = self._calc_fibre_direction(self._fibre_rule_2)
        else:                    # compute sheet direction orthogonal to fiber
            # Cross product with transmural to get sheet direction
            circum = self._element_circum_direction()
            apicbasal = self._element_apical_basal_direction()

            # Make sure basis is orthogonal
            normal = np.cross(circum, apicbasal)
            sheet = np.cross(normal, fibre)
            # Normalise
            sheet_norm = np.sqrt((sheet**2).sum(axis=1))
            sheet /= sheet_norm[:, np.newaxis]

        # Combine components and store to internal array
        self._lonarray = np.column_stack((fibre, sheet))

    def _bc_nodes(self):
        """
        Determine nodes at 90 degree intervals on bottom face for BCs.

        To be reimplemented in subclasses.

        Returns
        -------
        list
            Indices of nodes at 0, 90, 180 and 270 degrees
        """
        raise NotImplementedError

    def _generate_carp_surf(self, filename, surface):
        """
        Generate a single openCARP .surf file for a certain surface.

        Parameters
        ----------
        filename : str
            The file to save the .surf file to
        surface : str
            The surface to generate
        """

        # Write surface file
        with open(filename, 'w') as f_p:
            # Header
            f_p.write('{}\n'.format(self.n_face(surface)))

            # Get local nodes and parent elements of faces in selection
            for ftype, facepts in self.faces(surface):

                # Get the format string for this face type
                fmt = CARP_ELEM_FORMAT[ftype] + '\n'

                # Write content
                for fce in facepts:
                    f_p.write(fmt.format(fce))

    def _generate_carp_vtx(self, filename, surface):
        """
        Generate a single openCARP .vtx file for a certain surface.

        Parameters
        ----------
        filename : str
            The file to save the .vtx file to
        surface : str
            The surface to generate
        """

        # Get list of nodes on this surface
        vtx = np.concatenate([pts.flatten() for _, pts in self.faces(surface)])
        vtx = np.unique(vtx)

        with open(filename, 'w') as f_p:

            # Header
            f_p.write('{}\n'.format(vtx.shape[0]))
            f_p.write('extra\n')

            # Content
            for vertex in vtx:
                f_p.write('{}\n'.format(vertex))

    def _generate_carp_neubc(self, filename, surface):

        assert self._tetrahedrise, 'Only usable for tet meshes'

        # Since tetrahedrized, should only be one element type
        elem_parts = self.elements()
        assert len(elem_parts) == 1
        elem = elem_parts[0][1]

        # Get data
        normals = self.face_normals(surface)
        centres = self.face_centres(surface) * 1e3
        areas = self.face_areas(surface) * 1e6
        elem_tags = self.element_tags()

        with open(filename, 'w') as f_p:

            # Header
            f_p.write('{} # mesh elast {} {} :\n'.format(self.n_face(surface),
                                                         self.n_elem(),
                                                         self.n_pts()))

            # Initialise format string
            fmt = ('{0[0]} {0[1]} {0[2]} {1} {2} {3} '
                   '{4[0]} {4[1]} {4[2]} {5[0]} {5[1]} {5[2]} {6}\n')

            # Initialise face counter
            i_face = 0

            for _, facepts, felem in self.faces(surface, return_elem=True):

                # Determine extra node to make up tet
                felempts = elem[felem]

                # Do it by construction of a masking array
                not_in_face = (felempts != facepts[:, 0:1]) \
                            * (felempts != facepts[:, 1:2]) \
                            * (felempts != facepts[:, 2:3])

                # Check only one match per row
                assert all(not_in_face.sum(axis=1) == 1)

                # Apply mask and collapse using sum operation
                extra_pt = (not_in_face * felempts).sum(axis=1)

                # Get associated element IDs
                face_tags = elem_tags[felem]

                # Write out faces of this type
                for row in zip(facepts, face_tags, extra_pt, felem):
                    row += (normals[i_face], centres[i_face], areas[i_face])
                    i_face += 1
                    f_p.write(fmt.format(*row))

    def generate_cyl_coords(self, filename):
        """
        Generate cylindrical coordinate field for the mesh.

        Returns:
            transmural, longitudinal and circumferential fiber field

        """
        circum = self._element_circum_direction()
        axial = self._element_apical_basal_direction()
        radial = np.cross(circum, axial)
        axial = np.cross(radial, circum)

        # Combine components and store in array
        lonarray = np.column_stack((radial, circum, axial))

        self._generate_carp_lon(filename, lonarray)


    def generate_carp(self, meshname, faces=[]):
        """
        Generate the mesh and store it to disk in openCARP format.

        Simply pass the basename to store the mesh to:

        >>> geom.generate_carp('mesh/new')

        to save the mesh to mesh/new.pts, mesh/new.elem and mesh/new.lon. To
        also work out and store .vtx, .surf and .neubc files for certain faces,
        specify these as a list as the 'faces' argument:

        >>> geom.generate_carp('mesh/new', faces['inside'])

        to additionally generate the files mesh/new_inside.vtx,
        mesh/new_inside.surf and mesh/new_inside.neubc.

        Note that neubc files are only generated for tetrahedral meshes.

        Parameters
        ----------
        meshname : str
            The base name (without extension) of the mesh to be generated.
        faces : list, optional
            List of faces to generate, entries must be one of
            ['top', 'bottom', 'inside', 'outside']
        """

        # Generate the pts, elem and lon files
        Mesh.generate_carp(self, meshname)

        # Generate faces files
        for selection in faces:

            # Generate .surf file
            filename = '{}_{}.surf'.format(meshname, selection)
            self._generate_carp_surf(filename, selection)

            # Generate .vtx file
            filename = '{}_{}.vtx'.format(meshname, selection)
            self._generate_carp_vtx(filename, selection)

            # Generate .neubc file (tet meshes only)
            if self._tetrahedrise:
                filename = '{}_{}.neubc'.format(meshname, selection)
                self._generate_carp_neubc(filename, selection)

    def generate_carp_rigid_dbc(self, basename):
        """
        Generate files for DBC definiton preventing free motion.

        Generate vertex files for the imposition of Dirichlet boundary
        conditions preventing the free rotation and translation of the mesh.
        See :ref:`dbc_free_body_motion` for an explanation.

        Parameters
        ----------
        basename : str
            Mesh base name (without extension)
        """

        nodes = self._bc_nodes()

        # BC file for two nodes along the x axis
        fname = '{}_xaxis.vtx'.format(basename)

        with open(fname, 'w') as f_p:
            # Header
            f_p.write('2\nextra\n')
            # Content
            f_p.write('{}\n'.format(nodes[0]))
            f_p.write('{}\n'.format(nodes[2]))

        # BC file for one node on y axis
        fname = '{}_yaxis.vtx'.format(basename)

        with open(fname, 'w') as f_p:
            # Header
            f_p.write('1\nextra\n')
            # Content
            f_p.write('{}\n'.format(nodes[1]))

    def generate_vtk_face(self, filename, selection):
        """
        Generate VTK file of selected face.

        Parameters
        ----------
        filename : str
            Filename of VTK file to generate
        selection : str
            One of ['top', 'bottom', 'inside', 'outside']
        """

        f_p = open(filename, 'w')

        # Header
        f_p.write('# vtk DataFile Version 3.0\n')
        f_p.write('Auto generated ring model - face "{}"\n'.format(selection))
        f_p.write('ASCII\n')
        f_p.write('DATASET UNSTRUCTURED_GRID\n')

        # Write points
        pts = self.points()
        f_p.write('POINTS {} double\n'.format(pts.shape[0]))
        for point in pts:
            f_p.write('{0[0]} {0[1]} {0[2]}\n'.format(point))

        # Write faces
        face_parts = self.faces(selection)
        n_vals = 0
        for _, faces in face_parts:
            n_vals += faces.shape[0] * (faces.shape[1] + 1)

        f_p.write('CELLS {} {}\n'.format(self.n_face(selection), n_vals))
        for ftype, faces in face_parts:
            for face in faces:
                if ftype == 'quad':
                    face = face[VTK_QUAD_REORDER]
                f_p.write(str(len(face)) + ' ')
                f_p.write(' '.join([str(i) for i in face]) + '\n')

        # Write cell types
        f_p.write('CELL_TYPES {}\n'.format(self.n_face(selection)))
        for ftype, faces in face_parts:
            celltype = '{}\n'.format(VTK_CELL_TYPES[ftype])
            for i in xrange(faces.shape[0]):
                f_p.write(celltype)

        # Write out some data for visualisation
        f_p.write('CELL_DATA {}\n'.format(self.n_face(selection)))

        # Write normals
        f_p.write('NORMALS normal float\n')
        for n in self.face_normals(selection):
            f_p.write('{0[0]} {0[1]} {0[2]}\n'.format(n))

        # Write areas
        f_p.write('SCALARS area float 1\n')
        f_p.write('LOOKUP_TABLE default\n')
        for a in self.face_areas(selection):
            f_p.write('{}\n'.format(a))

        f_p.close()

def closest_to_rangles(angles):
    """
    Return the indicies of the angles closest to a series of right angles.
    """

    indices = []

    for target in [0.0, np.pi/2, np.pi, 3*np.pi/2]:

        # Get the difference from the angle, and also wrapped forwards and
        # backwards by 2pi
        errs = np.column_stack((angles - target,
                                angles + 2*np.pi - target,
                                angles - 2*np.pi - target))
        # Get the minimum absolute error
        err = np.abs(errs).min(axis=1)

        # Save the index of the lowest
        indices.append(err.argmin())

    return np.array(indices)

def distribute_elements(n_type1, n_type2, type1, type2):
    """
    Distribute two element types evenly throughout a layer.
    """

    if n_type1 < n_type2:
        small = type1
        large = type2
        n_small = n_type1
        n_large = n_type2
    else:
        small = type2
        large = type1
        n_small = n_type2
        n_large = n_type1

    # Create 'chunks' equal to number of 'small' elements, with first and last
    # of half size
    large_per_chunk = n_large // n_small # Rounds down
    large_chunks = np.ones(n_small+1, dtype=int) * large_per_chunk
    # Make first and last chunk half size
    large_chunks[0] //= 2
    large_chunks[-1] //= 2
    # Add remainder from splitting of said chunk into two parts
    large_chunks[0] += large_per_chunk % 2

    # Share remaining elements among chunks
    left = n_large - large_chunks.sum()

    if left != 0:
        # Evenly distribute remaining elements
        # Generate list of values on [0,1] at even spacing
        remain = np.linspace(0, 1, left + 1)
        # Offset by half of spacing to avoid every level having prisms in
        # same place
        remain += remain[1] / 2.
        # Drop repeated val
        remain = remain[:-1]

        # Scale to range equivalent to block indices
        remain *= n_small
        # Round down and convert to int
        remain = np.array(np.floor(remain), dtype=int)

        for i in remain:
            large_chunks[i] += 1

    assert large_chunks.sum() == n_large

    elems = []
    for csize in large_chunks:
        elems += [large] * csize
        elems += [small]

    # Drop extra small on end
    elems = elems[:-1]

    return elems
