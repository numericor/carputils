#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Generate a simple pipe geometry, providing a few parameters.
"""

import sys
import numpy as np
from carputils.mesh.general import Mesh3D, closest_to_rangles

# provide Python2 to Python3 compatibility
isPy2 = True
if sys.version_info.major > 2:
    isPy2 = False
    xrange = range


class Pipe(Mesh3D):
    """
    Generate a pipe mesh or a pipe-segment mesh

    This class describes a pipe, and generates the described mesh. For
    example, to define a pipe with a 5mm internal radius and 5mm rim:

    >>> geom = Pipe(5)

    By default, the thickness of the pipe is 1/10 of the radius, and the rim
    length is the same as the thickness and the pipe length is 100 times the
    thickmess. These can be controlled exactly with the corresponding paramters:

    >>> geom = Pipe(5, thickness=0.5, pipe_legnth=10.0, rim_legnth=0.6)

    Discretisation of the pipe is done into hexahedra, with one hex across both
    the thickness and length. The circumferential discretisation is chosen to
    ensure as regular aspect hexahedra as possible. If a tetrahedral mesh is
    generated, these hexahedra are further subdivided into tets. Discretisation
    can be controlled with the division ``div_`` parameters:

    >>> geom = Pipe(5, 2, 0.7, div_walll=3)

    The pipe's fibre directions are, by default, oriented in the
    circumferential direction. To control the fibre direction, pass a function
    as the ``fibre_rule`` argument that itself takes a normalised transmural
    distance and returns a helix angle in radians:

    >>> rule = lambda t: (1 - t)/2
    >>> geom = Pipe(5, 2, 0.7, fibre_rule=rule)

    :func:`carputils.mesh.pipe.linear_fibre_rule` provides a convenient method
    to generate such rules. For example, to generate a +60/-60 degrees linear
    fibre rule:

    >>> rule = linear_fibre_rule(60, -60)
    >>> geom = Pipe(5, 2, 7, fibre_rule=rule)

    To generate the actual mesh, use the :meth:`generate_carp` and
    :meth:`generate_vtk` methods.

    Args:
        radius : float
            Inner radius of the pipe, in mm
        thickness : float, optional
            Radial thickness of the pipe, in mm, defaults to 1/10 inner radius
        length : float, optional
            Length of the pipe, in mm, by default 100 times the thickness
        div_wall : int, optional
            Number of elements across the radial thickness, defaults to 1
        div_circum : int, optional
            Number of elements around the full circle of the pipe, defaults to a
            number regularising the hexahedron aspects
        div_legnth : int, optional
            Number of elements across the length of the pipe, defaults to 100
        angle : float, optional
            Central angle of the segment in (0, 2*PI)
        tetrahedrise : bool, optional
            True to subdivide the mesh into tetrahedra
        fibre_rule : callable, optional
            Function describing the transmural variation of fibre angle, takes a
            single normalised transmural distance on [0,1] and returns a helix
            angle in radians, defaults to circumferentially oriented fibres
    """

    def __init__(self, radius, thickness=None, length=None,
                 div_wall=1, div_circum=None, div_length=100,
                 angle=None, *args, **kwargs):

        Mesh3D.__init__(self, *args, **kwargs)

        # Default thickness is radius/5
        if thickness is None:
            thickness = radius/10.0

        # If height not specified, again go for ideal aspect ratio
        if length is None:
            depth_per_div = float(thickness)/div_wall
            length = depth_per_div*div_length

        # If not specified, estimate div_circum for ideal aspect ratio
        if div_circum is None:
            depth_per_div = float(thickness)/div_wall
            # Assuming arc length for a division is ideally equal to its depth
            div_circum = int(np.round(2.0*np.pi*radius/depth_per_div))

        # Set angle to -1 (out of range) if no angle is given
        if angle is None:
            angle = -1.0

        self._radius = float(radius)
        self._thickness = float(thickness)
        self._length = float(length)
        self._div_wall = int(div_wall)
        self._div_circ = int(div_circum)
        self._div_length = int(div_length)
        self._angle = float(angle)


    @classmethod
    def with_resolution(cls, radius, resolution, length=None,
                        thickness=None, angle=None, **kwargs):
        """
        Simplified interface to generate pipe with target resolution.

        Args:
            radius : float
                Inner radius of ellipsoid cavity
            resolution : float
                Target mesh edge length
            length : float, optional
                Length of the pipe (default: 100 radius/10)
            thickness : float, optional
                Thickness of pipe wall (default: radius/5)
            angle : float, optional
                Central angle of the segment (default: None)
        """

        # Determine wall thickness and height, when not set
        if length is None:
            length = radius*10.0
        if thickness is None:
            thickness = radius/10.0

        if angle is None:
            angle = -1.0

        # Determine number of divisions in each direction
        tot_theta = angle if 0.0 < angle < 2.0 * np.pi else 2.0*np.pi
        # Circumferential
        div_circum = int(round(tot_theta*radius/resolution))
        # Transmural
        div_wall = int(round(thickness/resolution))
        # Vertical
        div_length = int(round(length/resolution))

        # Generate object and return
        geom = cls(radius, thickness, length, div_wall,
                   div_circum, div_length, angle, **kwargs)

        return geom

    def is_segment(self):
        return 0.0 < self._angle < 2.0*np.pi

    def cavity_volume(self):
        """
        Calculate the volume of the cavity analytically.

        The actual volume may be less due to discretisation effects.

        Returns:
            float
                The cavity volume
        """
        return np.pi*self._radius**2*self._length

    def _generate_points(self):
        """
        Generate the points of the mesh.
        """
        is_segment = 0.0 < self._angle < 2.0*np.pi
        r_per_div = self._thickness/self._div_wall
        tot_theta = self._angle if is_segment else 2.0*np.pi
        theta_per_div = tot_theta/self._div_circ
        z_per_div = self._length/self._div_length

        num_pts_circ = self._div_circ + (1 if is_segment else 0)

        coords = []

        for i_circ in xrange(num_pts_circ):
            theta = i_circ*theta_per_div

            z = 0.0
            for i_length in xrange(self._div_length+1):

                for i_wall in xrange(self._div_wall+1):
                    r = self._radius+i_wall*r_per_div

                    # Calc Cartesian coordinates
                    x = r*np.cos(theta)
                    y = r*np.sin(theta)

                    coords.append([x, y, z])

                z += z_per_div

        self._ptsarray = np.array(coords)

    def _generate_elements(self):
        """
        Generate the mesh connectivity, tesselated into hexahedra.
        """

        self._clear_mixedelem_data()

        is_segment = 0.0 < self._angle < 2.0*np.pi
        # Number of points per slice in the circumferential direction
        pts_per_htdiv = self._div_wall+1
        pts_per_circdiv = (self._div_wall+1)*(self._div_length+1)

        for i_circ in xrange(self._div_circ):

            circ_div = [i_circ*pts_per_circdiv,
                        (i_circ+1)*pts_per_circdiv]

            # Wrap back to start of circle
            if not is_segment and i_circ+1 == self._div_circ:
                circ_div[1] = 0

            cutsurf0 = False
            cutsurf1 = False
            if is_segment:
                cutsurf0 = i_circ == 0
                cutsurf1 = i_circ == self._div_circ-1

            for i_length in xrange(self._div_length):
                ht_offset = [i_length*pts_per_htdiv,
                             (i_length+1)*pts_per_htdiv]

                bottom = (i_length == 0)
                top = (i_length == self._div_length-1)

                for i_wall in xrange(self._div_wall):
                    trans_offset = [i_wall, i_wall+1]

                    inside = (i_wall == 0)
                    outside = (i_wall == self._div_wall-1)

                    points = []
                    for h in ht_offset:
                        for c in circ_div:
                            for t in trans_offset:
                                points.append(c+h+t)

                    self._mixedelem['hexa'].append(points)
                    if cutsurf0:
                        self._register_face('hexa', 'right', 'cutsurf0')
                    if cutsurf1:
                        self._register_face('hexa', 'left', 'cutsurf1')
                    if bottom:
                        self._register_face('hexa', 'bottom')
                    if top:
                        self._register_face('hexa', 'top')
                    if inside:
                        self._register_face('hexa', 'front', 'inside')
                    if outside:
                        self._register_face('hexa', 'back', 'outside')

    def _bc_nodes(self):
        """
        Determine nodes at 90 degree intervals on bottom face for BCs.

        Returns:
            list
                Indices of nodes at 0, 90, 180 and 270 degrees
        """

        # Get the points on the endocardium
        pts_endo = []
        for _, facepts in self.faces('inside'):
            pts_endo.append(facepts.flatten())
        pts_endo = np.concatenate(pts_endo)

        # Get the points on the base
        pts_bot = []
        for _, facepts in self.faces('bottom'):
            pts_bot.append(facepts.flatten())
        pts_bot = np.concatenate(pts_bot)

        # Get points in both
        candidates = np.intersect1d(pts_endo, pts_bot)

        # Get candidate coordinates
        coords = self.points()[candidates]

        # Get polar coordinate
        theta = np.arctan2(coords[:, 1], coords[:, 0])

        # Find BC nodes
        return candidates[closest_to_rangles(theta)]

    def _element_transmural_distance(self):
        centres = self.element_centres()
        r = np.sqrt((centres[:, :-1]**2).sum(axis=1))
        depth = (r-self._radius)/self._thickness
        return depth

    def _element_circum_direction(self):
        centres = self.element_centres()
        # Determine the circumferential direction vector
        circum = np.column_stack((-centres[:, 1], centres[:, 0], np.zeros(centres.shape[0])))
        # As unit vector
        circum /= np.sqrt((circum**2).sum(axis=1))[:, np.newaxis]
        return circum

    def _element_apical_basal_direction(self):
        return np.array([0., 0., 1.0])


if __name__ == '__main__':

    import argparse
    from carputils.mesh.fibres import linear_fibre_rule

    parser = argparse.ArgumentParser()

    parser.add_argument('meshname',
                        help='Basename for mesh output')
    parser.add_argument('--radius',
                        type=float, default=10.0,
                        help='Internal radius of pipe')
    parser.add_argument('--thickness',
                        type=float, default=1.0,
                        help='Thickness of pipe wall')
    parser.add_argument('--length',
                        type=float, default=100.0,
                        help='Length of pipe')
    parser.add_argument('--angle',
                        type=float, default=None,
                        help='Central angle of the segment in degree')
    parser.add_argument('--hexahedra',
                        action='store_true',
                        help='Generate pipe with hexes rather than tets')

    args = parser.parse_args()

    DEGREE2RAD = 2.0*np.pi/360.0

    # Generate a plus 60 - minus 60 fibre rule
    p60m60 = linear_fibre_rule(60, -60)

    faces = ['top', 'bottom', 'inside', 'outside']
    # Create pipe object

    pipe = Pipe(args.radius, args.thickness, args.length,
                div_wall=3, div_circum=50, div_length=50,
                angle=None if args.angle is None else args.angle*DEGREE2RAD,
                tetrahedrise=not args.hexahedra,
                fibre_rule=p60m60)

    if pipe.is_segment():
        faces += ['cutsurf0', 'cutsurf1']

    # Generate CARP mesh
    pipe.generate_carp(args.meshname, faces)

    # Generate VTK mesh
    pipe.generate_vtk('{}.vtk'.format(args.meshname))
    for f in faces:
        pipe.generate_vtk_face('{}_{}.vtk'.format(args.meshname, f), f)
