#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Classes and routines for the generation of geometric meshes.

For a detailed description of how to use the mesh generation functionality,
see :ref:`carputils-mesh`.
"""

__all__ = ['Mesh', 'Block', 'Cable', 'Grid', 'Ring', 'Ellipsoid', 'Pipe',
           'BoxRegion', 'SphereRegion', 'CylinderRegion',
           'generate', 'linear_fibre_rule', 'block_boundary_condition']

# Import all the geometry classes
from carputils.mesh.base import Mesh
from carputils.mesh.block import *
from carputils.mesh.cable import Cable
from carputils.mesh.grid import Grid
from carputils.mesh.ring import Ring
from carputils.mesh.ellipsoid import Ellipsoid
from carputils.mesh.pipe import Pipe

# Import the utility functions
from carputils.mesh.generate import generate
from carputils.mesh.region import *
from carputils.mesh.fibres import linear_fibre_rule
