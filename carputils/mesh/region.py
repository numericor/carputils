#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Define some region defining objects for use with geometry classes.
"""
import numpy as np

__all__ = ['BoxRegion',
           'CylinderRegion',
           'SphereRegion']

class Region(object):
    """
    A general region definition.

    Do not use directly. Use :class:`BoxRegion`, :class:`SphereRegion` or
    :class:`CylinderRegion` instead.

    Args:
        tag : int, optional
            Tag ID to assign to this region, defaults to auto assign by mesher
        bath : int, optional
            0 - no bath, 1 - isotropic bath, 2 - anisotropic bath, default is 0
        p0 : array-like, optional
            Relevant point 0, in mm
        p1 : array-like, optional
            Relevant point 1, in mm
        radius : float
            Radius, where relevant, in mm
        rtype : int
            region type identifier for mesher
        ctype : int
            region type identifier for carp
    """

    def __init__(self, tag=None, bath=0, rtype=0,
                 p0=(0., 0., 0.), p1=(0., 0., 0.), radius=0., ctype=None):

        self.tag = tag                # None - auto set, int - manual set
        self.bath = int(bath)         # 0-no bath, 1-isotropic, 2-anisotropic
        self.rtype = int(rtype)       # 0-block, 1-sphere, 2-cylinder
        self.radius = float(radius)   # radius in mm
        self.p0 = np.array(p0, dtype=float) # in mm
        self.p1 = np.array(p1, dtype=float) # in mm
        if ctype is not None:
            self.ctype = int(ctype)

    def mesher_opts(self, i):
        """
        Generate the mesher option list for this region.

        Args:
            i : int
                Index of mesh region.

        Returns:
            list
                Mesher command line options for this region.
        """

        # Convert mm to cm
        radius = self.radius / 10.
        p0 = self.p0 / 10.
        p1 = self.p1 / 10.

        # Mesher takes tags < 0 as 'auto assign'
        tag = -1 if self.tag is None else self.tag

        opts = ['-regdef[{}].tag'.format(i), tag,
                '-regdef[{}].type'.format(i), self.rtype,
                '-regdef[{}].bath'.format(i), self.bath,
                '-regdef[{}].rad'.format(i), radius,
                '-regdef[{}].p0[0]'.format(i), p0[0],
                '-regdef[{}].p0[1]'.format(i), p0[1],
                '-regdef[{}].p0[2]'.format(i), p0[2],
                '-regdef[{}].p1[0]'.format(i), p1[0],
                '-regdef[{}].p1[1]'.format(i), p1[1],
                '-regdef[{}].p1[2]'.format(i), p1[2]]

        return opts

    def carp_opts(self, i):
        """
        Generate the carp option list for this tag region.

        Args:
            i : int
                Index of mesh region.

        Returns:
            list
                Mesher command line options for this region.
        """

        # Convert mm to cm
        radius = self.radius / 10.
        p0 = self.p0 / 10.
        p1 = self.p1 / 10.

        # Mesher takes tags < 0 as 'auto assign'
        tag = -1 if self.tag is None else self.tag

        # convert dimensions to micrometers
        opts = ['-tagreg[{}].tag'.format(i), tag,
                '-tagreg[{}].type'.format(i), self.ctype,
                '-tagreg[{}].radius'.format(i), radius*10000,
                '-tagreg[{}].p0[0]'.format(i), p0[0]*10000,
                '-tagreg[{}].p0[1]'.format(i), p0[1]*10000,
                '-tagreg[{}].p0[2]'.format(i), p0[2]*10000,
                '-tagreg[{}].p1[0]'.format(i), p1[0]*10000,
                '-tagreg[{}].p1[1]'.format(i), p1[1]*10000,
                '-tagreg[{}].p1[2]'.format(i), p1[2]*10000]

        return opts

    def __call__(self, coord):
        """
        Reimplement with logic determining if coord (mm) is in the region.
        """
        raise NotImplementedError

class BoxRegion(Region):
    """
    Describe an axis-aligned cuboid for mesh tag assignment.

    Note: !!!mesher assumes the boxes to be relative to the center of the mesh!!!

    Args:
        lowerleft : array-like, optional
            Lower left corner of the box region, defaults to (0,0,0)
        upperright : array-like, optional
            Upper right corner of the box region, defaults to (0,0,0)
        tag : int, optional
            Tag ID to assign to this region, defaults to auto assign by mesher
        bath : int, optional
            0 - no bath, 1 - isotropic bath, 2 - anisotropic bath, default is 0
    """

    def __init__(self, lowerleft=(0., 0., 0), upperright=(0., 0., 0.),
                 tag=None, bath=0):
        Region.__init__(self, tag, bath, 0, lowerleft, upperright, ctype=2)

    def __call__(self, coord):
        coord = np.array(coord)
        return (coord >= self.p0).all() and (coord <= self.p1).all()

class SphereRegion(Region):
    """
    Describe a sphere for mesh tag assignment.

    Args:
        radius : float, optional
            Radius of the sphere, defaults to 0
        centre : array-like, optional
            Centre of the sphere, defaults to (0,0,0)
        tag : int, optional
            Tag ID to assign to this region, defaults to auto assign by mesher
        bath : int, optional
            0 - no bath, 1 - isotropic bath, 2 - anisotropic bath, default is 0
    """

    def __init__(self, radius=0., centre=(0., 0., 0.), tag=None, bath=0):
        Region.__init__(self, tag, bath, 1, centre, radius=radius, ctype=1)

    def __call__(self, coord):
        coord = np.array(coord)
        r = ((coord - self.p0)**2).sum()
        return r <= self.radius**2

class CylinderRegion(Region):
    """
    Describe a cylinder for mesh tag assignment.

    Args:
        start  : array-like
            Starting point
        axis : array-like
            axis of cylinder with magnitude being the length of the cylinder
        radius : float, optional
            Radius of the sphere, defaults to 0
        centre : array-like, optional
            Centre of the sphere, defaults to (0,0,0)
        tag : int, optional
            Tag ID to assign to this region, defaults to auto assign by mesher
        bath : int, optional
            0 - no bath, 1 - isotropic bath, 2 - anisotropic bath, default is 0
    """

    def __init__(self, radius=0., start=(0., 0., 0.), axis=(0., 0., 0.),
                 tag=None, bath=0):
        Region.__init__(self, tag, bath, 2, start, axis, radius, ctype=3)
        axis = np.array(axis)
        self.axis_mag = np.sqrt((axis**2).sum())
        self.normalised_axis = axis / self.axis_mag

    def __call__(self, coord):
        coord = np.array(coord)
        relative_to_p0 = coord - self.p0
        dist_along_axis = np.dot(relative_to_p0, self.normalised_axis)
        if dist_along_axis < 0 or dist_along_axis > self.axis_mag:
            return False
        # Using Pythagoras'
        dist_from_axis = (relative_to_p0**2).sum() - dist_along_axis**2
        return dist_from_axis <= self.radius**2

    def mesher_opts(self, i):
        opts = super(CylinderRegion, self).mesher_opts(i)
        opts += ['-regdef[{}].cyllen'.format(i), self.axis_mag/10.]
        return opts

    def carp_opts(self, i):
        # Convert mm to um
        radius = self.radius * 1000.
        p0 = self.p0 * 1000.
        p1 = p0 + (self.normalised_axis*self.axis_mag)*1000.

        print(p1)

        # convert dimensions to micrometers
        opts = ['-tagreg[{}].tag'.format(i), self.tag,
                '-tagreg[{}].type'.format(i), self.ctype,
                '-tagreg[{}].radius'.format(i), radius,
                '-tagreg[{}].p0[0]'.format(i), p0[0],
                '-tagreg[{}].p0[1]'.format(i), p0[1],
                '-tagreg[{}].p0[2]'.format(i), p0[2],
                '-tagreg[{}].p1[0]'.format(i), p1[0],
                '-tagreg[{}].p1[1]'.format(i), p1[1],
                '-tagreg[{}].p1[2]'.format(i), p1[2]]

        return opts
