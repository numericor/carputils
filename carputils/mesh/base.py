#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the
# Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
from sys import version_info
import math
import numpy as np

# provide Python2 to Python3 compatibility
isPy2 = True
if version_info.major > 2:
    isPy2 = False
    xrange = range

# Prepare some openCARP constants
CARP_ELEM_ID = {'Tt': 0, 'Hx': 1, 'Oc': 2, 'Py': 3, 'Pr': 4,
                'Qd': 5, 'Tr': 6, 'Ln': 7, 'Tt2': 8, 'Tr2': 9}

CARP_ELEM_LABEL = {val: key for key, val in CARP_ELEM_ID.items()}

CARP_ELEM_SIZE = {'Tt': 4, 'Hx': 8, 'Oc': 6, 'Py': 5, 'Pr': 6,
                  'Qd': 4, 'Tr': 3, 'Ln': 2, 'Tt2': 4, 'Tr2': 3}

for key, val in CARP_ELEM_ID.items():
    CARP_ELEM_SIZE[val] = CARP_ELEM_SIZE[key]

class Element(object):
    """
    Describes a mesh element.

    Parameters
    ----------
    etype : int or str
        The openCARP element type
    nodes : array of int
        The node numbers of this element
    """

    def __init__(self, etype, nodes):

        # Make sure etype is int type
        if isinstance(etype, str):
            self.etype = CARP_ELEM_ID[etype]
        else:
            assert (isinstance(etype, int) or isinstance(etype, np.int64))
            self.etype = int(etype)

        # Check number of nodes is correct
        assert len(nodes) == CARP_ELEM_SIZE[self.etype]
        self.nodes = nodes

class ElementMixinBase(object):
    """
    Base class for element mixins, to be subclassed with element storage code.
    """

    def __init__(self, *args, **kwargs):
        super(ElementMixinBase, self).__init__(*args, **kwargs)
        self.num_elems = None
        self._elem_type = None
        self._elem_ID = None

    def elements(self):
        """
        Iterate over the elements of the mesh.

        Yields
        ------
        Element
            The mesh elements
        """
        for i in xrange(self.num_elems):
            yield self.element(i)

class ElementMaskedArrayMixin(ElementMixinBase):
    """
    Element mixin implementing numpy masked array storage.
    """

    def __init__(self, *args, **kwargs):
        super(ElementMaskedArrayMixin, self).__init__(*args, **kwargs)
        self._masked_elem = None
        self._elem_size = None

    def read_elements(self, filename):
        """
        Method to read elements from an openCARP .elem file.

        Parameters
        ----------
        filename : str
            The full filename of the openCARP elem file
        """

        with open(filename) as f_p:

            # Read header
            self.num_elems = int(f_p.readline())

            # Initialise arrays
            max_elem_size = max(CARP_ELEM_SIZE.values())
            elems = np.zeros((self.num_elems, max_elem_size), dtype=int)
            mask = np.ones((self.num_elems, max_elem_size), dtype=bool)

            self._elem_size = np.empty(self.num_elems, dtype=int)
            self._elem_type = np.empty(self.num_elems, dtype=int)
            self._elem_ID = np.ones(self.num_elems, dtype=int)

            # Parse elements
            for i_elem in xrange(self.num_elems):

                line = f_p.readline().split()
                if line == '':
                    raise Exception('File ended prematurely')

                # Store element size and type
                size = CARP_ELEM_SIZE[line[0]]
                self._elem_size[i_elem] = size
                self._elem_type[i_elem] = CARP_ELEM_ID[line[0]]

                # Convert rest of line to ints
                data = [int(v) for v in line[1:]]

                # Store element indices
                #self._masked_elem[i_elem,:size] = data[:size]
                elems[i_elem, :size] = data[:size]
                mask[i_elem, :size] = False

                # Add material ID if available
                if len(data) > size:
                    self._elem_ID[i_elem] = data[size]

            self._masked_elem = np.ma.array(elems, mask=mask)

    def element(self, i_elem):
        """
        Return the requested element.

        Parameters
        ----------
        i_elem : int
            The index of the requested element

        Returns
        -------
        Element
            The element
        """
        return Element(self._elem_type[i_elem],
                       np.ma.compressed(self._masked_elem[i_elem]))

    def element_centroids(self):
        """
        Compute and return the centroids of all elements in the mesh.

        Returns
        -------
        array
            Shape (num_elems, 3), the coordinate of the centroid of each elem
        """

        # Construct a 3D mask for the element coordinates array
        mask3 = np.concatenate((self._masked_elem.mask[:, :, np.newaxis],)*3,
                               axis=2)

        # Use the element index array to slice the points and remask it
        element_coords = np.ma.array(self._points[self._masked_elem],
                                     mask=mask3)

        # Sum and divide by num elem points to compute the element centres
        return np.sum(element_coords, axis=1) / self._elem_size[:, np.newaxis]

    def element_IDs(self):
        """
        Return the element ids of all elements in the mesh.

        Returns
        -------
        array
            Shape (num_elems, 1)
        """
        return self._elem_ID

class ElementCompressedRowMixin(ElementMixinBase):
    """
    Element mixing implementing compressed row storage similar to openCARP storage.
    """

    def __init__(self, *args, **kwargs):
        super(ElementCompressedRowMixin, self).__init__(*args, **kwargs)
        self._crs_elem = None
        self._crs_starts = None
        self._elem_size = None

    def read_elements(self, filename):
        """
        Method to read elementsfrom an openCARP .elem file.

        Parameters
        ----------
        filename : str
            The full filename of the openCARP elem file
        """

        with open(filename) as f_p:

            # Read header
            self.num_elems = int(f_p.readline())

            # Initialise arrays
            max_size = max(CARP_ELEM_SIZE.values()) * self.num_elems
            self._crs_elem = np.empty(max_size, dtype=int)
            self._crs_starts = np.empty(self.num_elems, dtype=int)
            self._elem_size = np.empty(self.num_elems, dtype=int)
            self._elem_type = np.empty(self.num_elems, dtype=int)
            self._elem_ID = np.ones(self.num_elems, dtype=int)

            # Parse elements
            elem_start = 0
            for i_elem in xrange(self.num_elems):

                line = f_p.readline().split()
                if line == '':
                    raise Exception('File ended prematurely')

                # Store element start, size and type
                size = CARP_ELEM_SIZE[line[0]]
                self._crs_starts[i_elem] = elem_start
                self._elem_size[i_elem] = size
                self._elem_type[i_elem] = CARP_ELEM_ID[line[0]]

                # Convert rest of line to ints
                data = [int(v) for v in line[1:]]

                # Store element indices
                self._crs_elem[elem_start:elem_start+size] = data[:size]

                # Add material ID if available
                if len(data) > size:
                    self._elem_ID[i_elem] = data[size]

                # Increment element start
                elem_start += size

            # Throw away unused space
            self._crs_elem = self._crs_elem[:elem_start]

    def element(self, i_elem):
        """
        Return the requested element.

        Parameters
        ----------
        i_elem : int
            The index of the requested element

        Returns
        -------
        Element
            The element
        """

        start = self._crs_starts[i_elem]
        size = self._elem_size[i_elem]
        indices = self._crs_elem[start:start+size]

        return Element(self._elem_type[i_elem], indices)

    def element_centroids(self):
        """
        Compute and return the centroids of all elements in the mesh.

        Returns
        -------
        array
            Shape (num_elems, 3), the coordinate of the centroid of each elem
        """

        # Prepare array
        centroids = np.empty((self.num_elems, 3), dtype=float)

        for i_elem in xrange(self.num_elems):

            # Get element indices
            start = self._crs_starts[i_elem]
            size = self._elem_size[i_elem]
            ind = self._crs_elem[start:start+size]

            # Compute centroid for this element
            centroids[i_elem, :] = self._points[ind].sum(axis=0) / self._elem_size[i_elem]

        return centroids

class Mesh(ElementMaskedArrayMixin):
    """
    Base mesh class with read functionality.

    To load a mesh from files, use the :meth:`from_files` class method.
    """

    def __init__(self, *args, **kwargs):

        # Call parent class constuctors
        super(Mesh, self).__init__(*args, **kwargs)

        # Initialise some values
        self.num_pts = None
        self.num_axes = None

        self._points = None
        self._fibres = None
        self._sheets = None

    @classmethod
    def from_files(cls, basename, lonfile=None, um2mm=True):
        """
        Load a mesh from openCARP mesh files.

        Parameters
        ----------
        basename : str
            The openCARP mesh base name
        lonfile : str, optional
            The .lon file name, by default the basename is used
        """

        # Instantiate object
        obj = cls()

        # Load points
        obj.read_points(basename + '.pts', um2mm=um2mm)

        # Load elements
        obj.read_elements(basename + '.elem')

        # Load fibres
        if lonfile is None:
            lonfile = basename
        if not lonfile.endswith('.lon'):
            lonfile += '.lon'
        obj.read_fibres(lonfile)

        return obj

    def points(self):
        return self._points

    def read_points(self, filename, um2mm=True):
        """
        Method to read point coordinates from an openCARP .pts file.

        Parameters
        ----------
        filename : str
            The full filename of the openCARP pts file
        um2mm : bool
            Convert units to mm.
        """

        # Need to convert to mm?
        scaler = 1000. if um2mm is True else 1.

        with open(filename) as f_p:
            # Load header
            self.num_pts = int(f_p.readline())
            # Load data
            from pandas import read_csv
            self._points = read_csv(filename, delimiter=' ',
                                    header=None, skiprows=1).values.squeeze()
            self._points = self._points / scaler

        # Sanity check
        assert self._points.shape[0] == self.num_pts

    def fibres(self):
        return self._fibres

    def sheets(self):
        if self.num_axes < 2:
            raise Exception('Sheets not read')
        return self._sheets

    def read_fibres(self, filename):
        """
        Method to read fibres from a openCARP .lon file.

        Parameters
        ----------
        filename : str
            The full filename of the openCARP lon file
        """

        with open(filename) as f_p:
            # Read header
            self.num_axes = int(f_p.readline())
            # Read data
            self._fibres = np.loadtxt(f_p)

        if self.num_axes == 2:
            # Split array into two
            self._sheets = self._fibres[:, 3:]
            self._fibres = self._fibres[:, :3]

        # Sanity check
        if self.num_elems is not None:
            assert self._fibres.shape[0] == self.num_elems

    def rotation_matrix(self, axis, theta):
        """
        Return the rotation matrix associated with positive rotation about
        the given axis by theta radians.
        based on Euler-Rodrigues formula
        """
        axis = np.asarray(axis)
        axis = axis / math.sqrt(np.dot(axis, axis))
        a = math.cos(theta / 2.0)
        b, c, d = -axis * math.sin(theta / 2.0)
        aa, bb, cc, dd = a * a, b * b, c * c, d * d
        bc, ad, ac, ab, bd, cd = b * c, a * d, a * c, a * b, b * d, c * d
        return np.array([[aa + bb - cc - dd, 2 * (bc + ad), 2 * (bd - ac)],
                         [2 * (bc - ad), aa + cc - bb - dd, 2 * (cd + ab)],
                         [2 * (bd + ac), 2 * (cd - ab), aa + dd - bb - cc]])


    def rotate(self, geom, filename, axis, theta, rotate_fibres=True):
        """
        Rotate points of mesh and overwrite points file
        Args:
            x_theta: Rotation of theta about the x-axes of the coord. system
            y_theta: Rotation of theta about the y-axes of the coord. system
            z_theta: Rotation of theta about the z-axes of the coord. system
        """

        points_file = filename + '.pts'
        self.read_points(points_file, um2mm=False)
        if rotate_fibres:
            lon_file = filename + '.lon'
            self.read_fibres(lon_file)

        theta_rad = np.radians(theta)
        R = self.rotation_matrix(axis, theta_rad)

        for x in self._points:
            x[...] = R.dot(x)
        for x in self._fibres:
            x[...] = R.dot(x)

        # Adjust sizes
        geom._size[0] = round((max(self._points[:,0])- min(self._points[:,0]))/1000)
        geom._size[1] = round((max(self._points[:,1])- min(self._points[:,1]))/1000)
        geom._size[2] = round((max(self._points[:,2])- min(self._points[:,2]))/1000)

        with open(points_file, 'w') as f_p:
            # Header
            f_p.write('{}\n'.format(len(self._points)))
            # Content
            np.savetxt(f_p, self._points)
        if rotate_fibres:
            with open(lon_file, 'w') as f_p:
                # Header
                f_p.write('{}\n'.format(self.num_axes))
                # Content
                np.savetxt(f_p, np.c_[self._fibres, self._sheets])


