#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
import numpy

from carputils.cml.cmltree import CMLNode
from carputils.cml.cmltree import CMLLeaf
from carputils.cml.cmltree import CMLListBranch
from carputils.cml.cmltree import CMLDictBranch
from carputils.cml.cmltree import CMLNamespace

from carputils.cml.cmlconverter import CMLConverter
from carputils.cml.cmlconverter import CMLConverterYAML


class CMLParser:
    """
    Class defining a basic parser interface
    """

    def __init__(self):
        pass

    def parsef(self, fp):
        """
        Parse a file

        Note:
            This is just a method interface, a `NotImplementedError`
            will be raised when the method is called.

        Args:
            fp: File pointer to the file to be parsed

        Raise:
            NotImplementedError
        """
        raise NotImplementedError('error, `parsef` method not implemented!')

    def parses(self, string):
        """
        Parse a string

        Note:
            This is just a method interface, a `NotImplementedError`
            will be raised when the method is called.

        Args:
            srtring: string to be parsed

        Raise:
            NotImplementedError
        """
        raise NotImplementedError('error, `parses` method not implemented!')

    def parsel(self, lines):
        """
        Parse lines

        Note:
            This is just a method interface, a `NotImplementedError`
            will be raised when the method is called.

        Args:
            lines: lines to be parsed

        Raise:
            NotImplementedError
        """
        raise NotImplementedError('error, `parsel` method not implemented!')


class CMLParserYAML(CMLParser):
    """
    Class to parse YAML strings
    """

    def __init__(self, emptyValue=None):
        """
        Method to initialize a CMLParserYAML object

        Kwargs:
            empty_value: default value for empty nodes
        """
        CMLParser.__init__(self)
        # default value for empty nodes
        self.__emptyval = emptyValue

        # comment character
        self.__comment = '#'
        # key-value separator character
        self.__keyvalsep = ':'
        # item separator character
        self.__itemsep = ','
        # list item character
        self.__listitem = '-'
        # quote character
        self.__quote = '"'
        # list-environment characters
        self.__listenv = ['[', ']']
        # dictionary-environment characters
        self.__dictenv = ['{', '}']

    def __parse_list(self, string, lineno, node, key):

        branch = CMLListBranch(node, key)
        if len(string) == 0:
            return branch

        # split string
        lst = list(map(lambda s: s.strip(), string.split(self.__itemsep)))
        # remove empty strings
        lst = list(filter(lambda s: len(s) > 0, lst))

        for item in lst:
            branch.add_child(CMLLeaf(branch, None, CMLConverterYAML.convert(item)))

        return branch

    def __parse_dict(self, string, lineno, node, key):

        branch = CMLDictBranch(node, key)
        if len(string) == 0:
            return branch

        # split string
        lst = list(map(lambda s: s.strip(), string.split(self.__itemsep)))
        # remove empty strings
        lst = list(filter(lambda s: len(s) > 0, lst))

        for item in lst:
            # find separator
            index = item.find(self.__keyvalsep)

            # assign key
            itemkey = (item if index == -1 else item[:index]).strip()
            if len(itemkey) == 0:
                raise AttributeError('error in line {}, empty key!'.format(lineno+1))

            valuestr = None if index == -1 else item[index+1:].strip()
            if valuestr is not None and len(valuestr) == 0:
                raise AttributeError('error in line {}, empty value!'.format(lineno+1))
            branch.add_child(CMLLeaf(branch, itemkey, CMLConverterYAML.convert(valuestr)))

        return branch

    def __find_comment_index(self, line):
        string_index_pairs = []
        string_indices = []
        # find all strings ( = text within quotation marks)
        index = line.find(self.__quote)
        while index != -1:
            if len(string_indices) < 2:
                string_indices.append(index)
            else:
                string_index_pairs.append(string_indices)
                string_indices = [index]
            index = line.find(self.__quote, index+1)
        else:
            if len(string_indices) == 2:
                string_index_pairs.append(string_indices)

        comment_indices = []
        # find all hashtags not in strings
        index = line.find(self.__comment)
        while index != -1:
            instring = False
            for string_indices in string_index_pairs:
                if len(string_indices) == 2 and \
                      string_indices[0] < index < string_indices[1]:
                    instring = True
                    break
            if not instring:
                comment_indices.append(index)
            index = line.find(self.__comment, index+1)

        return -1 if len(comment_indices) == 0 else min(comment_indices)

    def parsef(self, fp):
        """
        Parse a YAML file

        Args:
            fp: File pointer to the YAML file to be parsed

        Raises:
            AttributeError

        Returns:
            A cmltree.CMLDictBranch representing the root node of
            the parsed code
        """
        lines = fp.readlines()
        return self.parsel(lines)

    def parses(self, string):
        """
        Parse a YAML string

        Args:
            string: YAML string to be parsed

        Raises:
            AttributeError

        Returns:
            A cmltree.CMLDictBranch representing the root node of
            the parsed code
        """
        lines = string.split('\n')
        return self.parsel(lines)

    def parsel(self, lines):
        """
        Parse a YAML strings

        Args:
            lines: YAML lines to be parsed

        Raises:
            AttributeError

        Returns:
            A cmltree.CMLDictBranch representing the root node of
            the parsed code
        """
        # create root node
        root = CMLDictBranch(None, 'root')
        node = root
        indent = 0

        # iterate over lines
        for i, line in enumerate(lines):

            # delete comment and tailing whitespaces in current line
            #index = line.find('#')
            comment_index = self.__find_comment_index(line)
            line = (line if comment_index == -1 else line[:comment_index]).rstrip()

            # if line is empty, continue with next line
            if len(line) == 0:
                continue

            # get indention of current line
            lineindent = len(line)-len(line.lstrip())

            # set reference indention if not set yet
            if lineindent > 0 and indent == 0:
                indent = lineindent

            # get indention-level of current line
            level = 0
            if lineindent >= 0 and indent > 0:
                level = float(lineindent*1.0/indent)
                if not level.is_integer():
                    raise AttributeError('error in line {}, indention!'.format(i+1))
                level = int(level)

            # raise exception if current indention-level is greater than
            # indention-level of parent node plus one
            if level > node.level+1:
                raise AttributeError('error in line {}, indention!'.format(i+1))

            # remove leading whitespace
            line = line.lstrip()

            # if line is empty, continue with next line
            if len(line) == 0:
                continue

            # find separator
            colon_index = line.find(self.__keyvalsep)
            is_list_item = line.startswith(self.__listitem)

            # raise exception if no separator or list-specifier is found
            if colon_index == -1 and not is_list_item:
                raise AttributeError('error in line {}, item specifier not found!'.format(i+1))

            # check wheter item has a key or not
            haskey = not is_list_item

            if haskey:
                # if indention has changed, find correct node on the stack
                while level <= node.level:
                    # all previous nodes of undefined type on a higher level
                    # are converted to a leaf-node with no value
                    if type(node) is CMLNode:
                        node = CMLLeaf.from_node(node, self.__emptyval)
                        node.parent.children[node.index] = node
                    # pop last node on stack
                    node = node.parent

                # if the node-type is undefined, set node type.
                # throw error if wrong node type
                if type(node) is CMLNode:
                    node = CMLDictBranch.from_node(node)
                    node.parent.children[node.index] = node
                elif type(node) is not CMLDictBranch:
                    raise AttributeError('error in line {}, wrong type, CMLDictBranch expected!'.format(i+1))

                # split into key and valuestr
                key, valuestr = line[:colon_index].strip(), line[colon_index+1:].strip()
                # check if key is not empty
                if len(key) == 0:
                    raise AttributeError('error in line {}, empty key!'.format(i+1))

                if len(valuestr) > 0:
                    # assign value to object
                    if valuestr.startswith(self.__listenv[0]) and valuestr.endswith(self.__listenv[1]):
                        childnode = self.__parse_list(valuestr[1:-1], i, node, key)
                        node.add_child(childnode)
                        node = childnode
                    elif valuestr.startswith(self.__dictenv[0]) and valuestr.endswith(self.__dictenv[1]):
                        childnode = self.__parse_dict(valuestr[1:-1], i, node, key)
                        node.add_child(childnode)
                        node = childnode
                    else:
                        childnode = CMLLeaf(node, key, CMLConverterYAML.convert(valuestr))
                        node.add_child(childnode)
                else:
                    # if no value is given, add a new node
                    childnode = CMLNode(node, key)
                    node.add_child(childnode)
                    node = childnode

            else:
                # the level of list items can only be the parent-level+1
                if level != node.level+1:
                    raise AttributeError('error in line {}, indention!'.format(i+1))

                # if the node-type is undefined, set node type.
                # throw error if wrong node type
                if type(node) is CMLNode:
                    node = CMLListBranch.from_node(node)
                    node.parent.children[node.index] = node
                elif type(node) is not CMLListBranch:
                    raise AttributeError('error in line {}, wrong type, CMLListBranch expected!'.format(i+1))

                valuestr = line[1:].strip()
                # check if valuestr is not empty
                if len(valuestr) == 0:
                    raise AttributeError('error in line {}, empty value!'.format(i+1))

                # assign value to object
                if valuestr.startswith(self.__listenv[0]) and valuestr.endswith(self.__listenv[1]):
                    childnode = self.__parse_list(valuestr[1:-1], i, node, None)
                    node.add_child(childnode)
                    node = childnode
                elif valuestr.startswith(self.__dictenv[0]) and valuestr.endswith(self.__dictenv[1]):
                    childnode = self.__parse_dict(valuestr[1:-1], i, node, None)
                    node.add_child(childnode)
                    node = childnode
                else:
                    childnode = CMLLeaf(node, None, CMLConverterYAML.convert(valuestr))
                    node.add_child(childnode)

        return root
