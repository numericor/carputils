#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
import sys
from collections import OrderedDict
from carputils.cml.cmlconverter import CMLConverter

isPy2 = not sys.version_info.major > 2

DictType = OrderedDict


class CMLDictOperations:
    """
    Class defining dictionary operations
    """

    def __init__(self):
        """
        Method to initialize a CMLDictOperations object
        """
        pass

    @staticmethod
    def __flatten_child(obj, prefix):
        """
        Flatten child dictionaries recursively

        This method flattens child dictionaries of the type
        OrderedDict recursively.

        Args:
            obj:    child dictionary
            prefix: string used as key prefix

        Returns:
            A list of key-value pairs
        """
        it = obj.iteritems if isPy2 else obj.items
        childlist = []
        for key, value in it():
            globalkey = '{}.{}'.format(prefix, key)
            if isinstance(value, DictType):
                childlist += CMLDictOperations.__flatten_child(value, globalkey)
            else:
                childlist.append([globalkey, value])
        return childlist

    @staticmethod
    def flatten(obj):
        """
        Flatten dictionaries recursively

        This method flattens dictionaries of the type OrderedDict
        recursively.

        Args:
            obj: root dictionary

        Returns:
            A flattened dictionary of the type OrderedDict
        """
        it = obj.iteritems if isPy2 else obj.items
        childlist = []
        for key, value in it():
            if isinstance(value, DictType):
                childlist += CMLDictOperations.__flatten_child(value, key)
            else:
                childlist.append([key, value])
        return DictType(childlist)


class CMLNamespace(object):
    """
    Class defining a namespace
    """

    def __init__(self, make_identifier=True, substitute='_', **kwargs):
        """
        Method to initialize a CMLNamespace object

        Kwargs:
            make_identifier: If True, the keys of `kwargs` are converted
                             to identifiers
            substitute:      A valid identifier string used to replace
                             invalid characters in the key strings
            kwargs:          Key-value pairs where the keys are added as
                             attributes to the namespace with the corresponding
                             values assigned to them
        """
        it = kwargs.iteritems if isPy2 else kwargs.items
        for key, value in it():
            self.add_attribute(key, value, make_identifier, substitute)

    def add_attribute(self, key, value, make_identifier=True, substitute='_'):
        """
        Add attribute to the namespace

        Args:
            key:   Attribute name
            value: Attribute value

        Kwargs:
            make_identifier: If True, the attribute name `key` is converted
                             to an identifier
            substitute:      A valid identifier string used to replace
                             invalid characters in the `key` strings

        Raises:
            AttributeError: If `key` is not a valid identifier string and
                            make_identifier is False.
        """
        if not CMLConverter.is_identifier(key):
            if not make_identifier:
                raise AttributeError('error, `{}` is not an identifier!'.format(key))
            else:
                key = CMLConverter.make_identifier(key, substitute=substitute)
        setattr(self, key, value)

    def to_string(self, level=0, indent=1):
        """
        Create a string representing the namespace

        Kwargs:
            level:  indention level
            indent: number of spaces per level

        Returns:
            A string representing the namespace
        """
        it = self.__dict__.iteritems if isPy2 else self.__dict__.items
        string = ''
        for key, value in it():
            if key[0].isalpha():
                string += ' '*level*indent + '{}:'.format(key)
                if isinstance(value, CMLNamespace):
                    string += '\n'+value.to_string(level+1, indent)
                else:
                    string += ' {}\n'.format(value)
        return string

    def __str__(self):
        """
        Return a string representing the namespace

        Returns:
            A string representing the namespace
        """
        return self.to_string()


class CMLNode(object):
    """
    Class defining a simple node object

    CMLNodes are just placeholders and
    should never occur in the final
    tree.

    Attributes:
        parent: parent node
        level:  level of the node
        index:  index of the node in the parent's child collection
        key:    key of the node (string or None)
    """

    def __init__(self, parent, key):
        """
        Method to initialize a CMLNode object

        Args:
            parent: Parent node
            key:    Key string or None
        """
        # parent node
        self.parent = parent
        # node level
        self.level = -1 if parent is None else parent.level+1
        # index of node in parent's child list
        self.index = -1
        # key of node
        self.key = None if key is None else str(key)

    @property
    def has_key(self):
        """
        Check if the CML-Node has a valid key

        Returns:
            True is key is valid, False otherwise
        """
        return CMLConverter.is_valid_string(self.key)

    def add_child(self, node):
        """
        Add child node

        Note:
            This is just a method interface, a `NotImplementedError`
            will be raised when the method is called.

        Args:
            node: child node

        Raises:
            NotImplementedError
        """
        raise NotImplementedError('error, `add_child` method not implemented!')

    def serialize(self):
        """
        Serialize node and all child nodes

        Note:
            This is just a method interface, a `NotImplementedError`
            will be raised when the method is called.
        """
        raise NotImplementedError('error, `serialize` method not implemented!')

    def objectify(self):
        """
        Objectify node and all child nodes

        Note:
            This is just a method interface, a `NotImplementedError`
            will be raised when the method is called.
        """
        raise NotImplementedError('error, `objectify` method not implemented!')


class CMLLeaf(CMLNode):
    """
    Class defining a leaf object

    CMLLeafs are nodes to which a
    value is assigned to. CMLLeafs
    can not have child CMLNodes.
    """

    def __init__(self, parent, key, value):
        """
        Method to initialize a CMLLeaf object

        Args:
            parent: Parent node
            key:    Key string or None
            value:  Value assigned to the leaf
        """
        super(CMLLeaf, self).__init__(parent, key)
        self.value = value

    @staticmethod
    def from_node(node, value):
        """
        Method to create a CMLLeaf object

        A CMLLeaf object is created from a
        CMLNode object.

        Args:
            node:  Placeholder node
            value: Value assigned to the leaf

        Raises:
            AttributeError: if node is not an instance of CMLNode

        Returns:
            The CMLLeaf object created
        """
        if not isinstance(node, CMLNode):
            raise AttributeError('error, CMLNode expected but got {}!'.format(type(node)))
        leafnode = CMLLeaf(node.parent, node.key, value)
        leafnode.index = node.index
        return leafnode

    def add_child(self, node):
        """
        Add child node

        Note:
            This is just a method interface, a `NotImplementedError`
            will be raised when the method is called.

        Args:
            node: child node

        Raises:
            NotImplementedError
        """
        raise NotImplementedError('error, `add_child` method not implemented!')

    def serialize(self):
        """
        Serialize node and all child nodes

        Returns:
            If the key of the node is None, the value assigned to
            the node is returned, else a list containing the key
            and the value is returned
        """
        return [self.key, self.value] if self.has_key else self.value

    def objectify(self):
        """
        Objectify node and all child nodes

        Returns:
            If the key of the node is None, the value assigned to
            the node is returned, else a list containing the key
            and the value is returned
        """
        return [self.key, self.value] if self.has_key else self.value


class CMLListBranch(CMLNode):
    """
    Class defining a list-branch object

    CMLListBranch are nodes which can hold
    several CMLNodes as children.
    """

    def __init__(self, parent, key):
        """
        Method to initialize a CMLListBranch object

        Args:
            parent: Parent node
            key:    Key string or None
        """
        super(CMLListBranch, self).__init__(parent, key)
        ## list to hold the children
        self.children = []

    @staticmethod
    def from_node(node):
        """
        Method to create a CMLListBranch object

        A CMLListBranch object is created from a
        CMLNode object.

        Args:
            node: Placeholder node

        Raises:
            AttributeError: if node is not an instance of CMLNode

        Returns:
            The CMLListBranch object created
        """
        if not isinstance(node, CMLNode):
            raise AttributeError('error, CMLNode expected but got {}!'.format(type(node)))
        branchnode = CMLListBranch(node.parent, node.key)
        branchnode.index = node.index
        return branchnode

    def add_child(self, node):
        """
        Add child node

        Args:
            node: child node

        Raises:
            AttributeError: if node is not an instance of CMLNode

        Raises:
            AttributeError
        """
        if not isinstance(node, CMLNode):
            raise AttributeError('error, CMLNode expected but got {}!'.format(type(node)))
        node.index = len(self.children)
        self.children.append(node)

    def serialize(self):
        """
        Serialize node and all child nodes

        Returns:
            If the key of the node is None, a list of
            all serialized child nodes is returned, else
            a list containing the key and the list of
            all serialized child nodes is returned
        """
        childseries = [child.serialize() for child in self.children]
        return [self.key, childseries] if self.has_key else childseries

    def objectify(self):
        """
        Objectify node and all child nodes

        Returns:
            If the key of the node is None, a list of
            all objectified child nodes is returned, else
            a list containing the key and the list of
            all objectified child nodes is returned
        """
        childobjects = [child.objectify() for child in self.children]
        return [self.key, childobjects] if self.has_key else childobjects


class CMLDictBranch(CMLNode):
    """
    Class defining a dict-branch object

    CMLDictBranch are nodes which can hold
    several CMLNodes as children.
    """

    def __init__(self, parent, key):
        """
        Method to initialize a CMLDictBranch object

        Args:
            parent: Parent node
            key:    Key string or None
        """
        super(CMLDictBranch, self).__init__(parent, key)
        ## list to hold the children
        self.children = []

    @staticmethod
    def from_node(node):
        """
        Method to create a CMLDictBranch object

        A CMLDictBranch object is created from a
        CMLNode object.

        Args:
            node: Placeholder node

        Raises:
            AttributeError: if node is not an instance of CMLNode

        Returns:
            The CMLDictBranch object created
        """
        if not isinstance(node, CMLNode):
            raise AttributeError('error, CMLNode expected but got {}!'.format(type(node)))
        branchnode = CMLDictBranch(node.parent, node.key)
        branchnode.index = node.index
        return branchnode

    def add_child(self, node):
        """
        Add child node

        Args:
            node: child node

        Raises:
            AttributeError: if node is not an instance of CMLNode

        Raises:
            AttributeError
        """
        if not isinstance(node, CMLNode) and not node.has_key:
            raise AttributeError('CMLNode with key expected but got {}!'.format(type(node)))
        node.index = len(self.children)
        self.children.append(node)

    def serialize(self):
        """
        Serialize node and all child nodes

        Returns:
            If the key of the node is None, a dictionary of
            all serialized child nodes as value is returned,
            else a list containing the key and the dictionary
            of all serialized child nodes as value is returned
        """
        childseries = DictType([child.serialize() for child in self.children])
        return [self.key, childseries] if self.has_key else childseries

    def objectify(self):
        """
        Objectify node and all child nodes

        Returns:
            If the key of the node is None, a CMLNamespace with
            all child nodes as attributes is returned, else a
            list containing the key and the CMLNamespace with
            all child nodes as attributes is returned
        """
        childobjects = DictType([child.objectify() for child in self.children])
        return [self.key, CMLNamespace(**childobjects)] if self.has_key else CMLNamespace(**childobjects)

