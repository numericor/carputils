# carputils

carputils is a Python framework for generating and running openCARP
experiments. carputils also provides functions for automatically comparing
simulation results against known solutions (regression testing).

## Installing

See INSTALL.md

## License
carputils is released under the [Apache Software License](LICENSE.md),
Version 2.0 ("Apache 2.0").
