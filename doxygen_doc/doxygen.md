# carputils Python Framework code documention

## Introduction

carputils is a Python package containing several openCARP-related functionalities and a framework for running electrophysiology simulations and testing their outputs en masse. Provided functionality includes:

* Scripting environment for designing complex in silico experiments
* Generation of openCARP command line inputs with:
	* Automatic adding of MPI launcher
	* Debugger and profiler attachment
	* Automatic batch script generation and submission with platform-specific hardware profiles
* Compact model setup using Python classes
* Efficient IO of standard openCARP file formats to NumPy arrays
* Failure-compliant mass running of tests
* Automatic comparison of test results against reference solutions

To start navigating through the documentation, please click on "Namespaces", "Classes", or "Files" above to start walking through the documentation.

For examples on how to use the carputils Python Framework look in our [Examples](https://opencarp.org/documentation/examples) section.

[Homepage](https://www.openCARP.org)